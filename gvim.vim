edit g:/tmp/c/a.c
"i insert text just before the cursor
I insert text at the start of the line
"a append text just after the cursor
A append text at the end of the line
"o open a new line below
"O open a new line above
s substitute the current character
"S substitute the current line
"r replace the current character
"R replace continuous characters

"查看临时文件
:swapname

" To save, ctrl-s.
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

:cd ../tmp
:pwd

"删除一个字符
d l

"复制一个段落
y ap

"两个小技巧
• x → delete one character at current cursor position
• p → paste after current cursor position

• d → delete
• w → one word
• w → move to the next word
• P → paste before the current cursor position

""
d$ p

""""""""""""""""""""
"时间机器
:earlier 4m

:later 45s

:undo 5
:undolist
"""""""""""""""""""""""""""

"搜索忽略大小写
set ignorecase
"搜索忽略大小写
set smartcase

""""""""""""""""""""""
""关于折叠
set foldmethod=indent
"创建
zc 
"打开
zo
"两者切换
za
:nnoremap <space> za

""""""""""""""""""""""""
"""关于缓冲区
"跳到第一个
:b 1

"列出当前缓冲区
:buffers
:ls

"删除第一个缓冲区
:bd 1

"""""""""""""""""""""""""
"""""table

:tabc
" Shortcuts for moving between tabs.
" Alt-j to move to the tab to the left
noremap <A-j> gT
" Alt-k to move to the tab to the right
noremap <A-k> gt

"""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""""脚本基础
"显示当前行有多少字符
:echo strlen(getline("."))

"变量声明
:let len = strlen(getline("."))
:echo "We have" len "characters in this line."

"显示家目录
:echo $HOME
"显示文件类型
:echo &filetype
"显示宏
:echo @a

""""""""""""""""""""""函数声明
:function CurrentLineLength()
: let len = strlen(getline("."))
: return len
:endfunction
"
"If you want to simply "call" a function to run but not display the contents, you can use :call CurrentLineLength()e
:call CurrentLineLength()
"
""""""""""""""""调用函数
:echo CurrentLineLength()

"""""""""""""""""""""判断
:if has("gui_running")
: colorscheme desert
:else
: colorscheme darkblue
:endif

":help feature-list to see what kind of features are available in Vim.
"elseif也可以用
"
:let i = 0
:while i < 5
: echo i
: let i += 1
:endwhile
"
:for i in range(5)
: echo i
:endfor
""""""""""""""""""""""""数据结构
:let fruits = ['apple', 'mango', 'coconut']
:echo fruits[0]
" apple
:echo len(fruits)
" 3
:call remove(fruits, 0)
:echo fruits
" ['mango', 'coconut']
:call sort(fruits)
:echo fruits
" ['coconut', 'mango']
:for fruit in fruits
: echo "I like" fruit
:endfor
" I like coconut
" I like mango
"

:r !date
ZZ :wq

:map ^P I#<ESC>
:map ^B 0x

:n1,n2s/^/#/g
:n1,n2s/^#//g
:n1,n2s/^/\/\//g

ab sammail samlee@lampbrother.net
