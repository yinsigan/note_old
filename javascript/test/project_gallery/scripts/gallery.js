function prepareGallery(){
    //if(!document.getElementById || !document.getElementsByTagName) return false;
    var nav = document.getElementById("gallerNav");
    var link = nav.getElementsByTagName("a");
    //var defaultImg = document.getElementById("galleryDefaultImg");
    //var galleryDescription = document.getElementById("galleryDescription");
    for(var i=0; i<link.length; i++){
        link[i].onclick = function(){
            //defaultImg.src = this.href;
            //galleryDescription.innerHTML = this.title;
            //galleryDescription.firstChild.nodeValue = this.title;
            //return false;
            return showPic(this);
        };
    }
}
function showPic(whichpic){
    var nav = document.getElementById("gallerNav");
    var link = nav.getElementsByTagName("a");
    var defaultImg = document.getElementById("galleryDefaultImg");
    var description = document.getElementById("galleryDescription");
    //defaultImg.getAttribute("src") = whichpic.href;
    var source = whichpic.getAttribute("href");
    defaultImg.setAttribute("src",source);
    description.firstChild.nodeValue = whichpic.title;
    for(var i=0; i<link.length; i++){
        link[i].setAttribute("id","");
    }
    whichpic.setAttribute("id","focus");
    return false;
}
addLoadEvent(prepareGallery);
