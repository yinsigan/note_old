window.onload = function(){
    document.nine.onsubmit = function(evt){
        if(evt)
            evt.preventDefault();
        else
            window.event.returnValue = false;

        var startNum = this.startNum;
        var finishNum = this.finishNum;
        var start = parseInt(startNum.value);
        var finish = parseInt(finishNum.value);
        if(start && finish){
            drawTable(start,finish);
        }else{
            alert("请输入正确的数学!");
        }

    };

    function drawTable(start,finish){
        var oldTable = document.getElementById("dynamicTable");
        if(oldTable){
            document.body.removeChild(oldTable);
        }
        var table = document.createElement("table");
        for(var i=start; i<=finish; i++){
            row = table.insertRow(table.rows.length);
            for(var j=start,cell; j<=i; j++){
                cell = row.insertCell(row.cells.length);
                cell.innerHTML = j + "*" + i + "=" + (i*j);
            }
            for(var k=j; k<=finish; k++){
                cell = row.insertCell(row.cells.length);
                cell.innerHTML = "&nbsp";
            }
        }
        table.border = "1";
        table.id = "dynamicTable";
        document.body.appendChild(table);
    }
    /*var table = document.getElementById("table");
    var row = table.insertRow(0);
    var cell = row.insertCell(0);
    cell.innerHTML = "new cell";
    console.log(table.rows.length);*/
};
