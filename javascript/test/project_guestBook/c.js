window.onload  = function(){
    var contentList = $("contentList");
    var li = contentList.getElementsByTagName("li")[0];
    //li.style.display = "none";
    contentList.removeChild(li);//移除
    var fm = $("fm1");
    var msgTitle = $("msgTitle");
    var userName = $("userName");
    var msgContent = $("msgContent");
    fm.onsubmit = function(evt){
        if(evt){//W3C
            evt.preventDefault();
        }else{//IE
            window.event.returnValue = false;
        }
        var title = msgTitle.value;
        var user = userName.value;
        var content = msgContent.value;
        if(title && user && content){
            var newLi = li.cloneNode(true);
            var msgTitleSpan = getByClass('msgTitle',newLi)[0];
            var userNameSpan = getByClass('userName',newLi)[0];
            var msgDateSpan = getByClass('msgDate',newLi)[0];
            var msgContentDiv = getByClass('msgContent',newLi)[0];
            msgTitleSpan.innerHTML = title;
            userNameSpan.innerHTML = user;
            msgDateSpan.innerHTML = date("Y年m月d日 H:i:s");
            //msgDateSpan.innerHTML = new Date();
            //TODO: 实现date("Y-m-d H:i:s")这样的函数
            //msgContentDiv.innerHTML = content;
            msgContentDiv.innerHTML = htmlEncode(content);
            //TODO: 实现htmlEncode("<html>") &lt; &gt;
            contentList.appendChild(newLi);
            //newLi.style.display = "block";
        }else{
            alert("请将表单填写完整!!");
        }
    };

    $("sortASC").onclick = function(){
        sortBBS(1);
    };

    $("sortDESC").onclick = function(){
        sortBBS(-1);
    };

    function sortBBS(dir){
        //将贴子排序f
        //dir 1表示正序 -1表示倒序
        var list = contentList.getElementsByTagName("li");
        //console.log(list.length);
        var a = [];
        var dateSpan;
        var tuple;
        var re = /(\d{4})年(\d{1,2})月(\d{1,2})日 (\d{1,2}):(\d{1,2}):(\d{1,2})/;
        var d;
        for(var i=0; i<list.length; i++){
            dateSpan = getByClass("msgDate",list[i])[0];
            //console.log(dateSpan.innerHTML);
            //2010年5月10日 16:1:51 解析出其中的Y m d H i s
            tuple = re.exec(dateSpan.innerHTML);
            d = new Date();
            d.setFullYear(tuple[1]);
            d.setMonth(tuple[2]-1);
            d.setDate(tuple[3]);
            d.setHours(tuple[4]);
            d.setMinutes(tuple[5]);
            d.setSeconds(tuple[6]);
            //console.log(d.getTime());
            a.push({
                node:list[i], //贴子对应的DOM元素
                date:d.getTime() //时间
            });
        }
        a.sort(function($1,$2){
            //console.log(a.date + "\n" + b.date);
            if($1.date > $2.date) return dir;
            else if($1.date < $2.date) return -dir;
            else return 0;
        });
        contentList.innerHTML = "";
        var frag =  document.createDocumentFragment();
        //alert(frag.nodeType);
        for(var j=0; j<a.length; j++){
            //console.log(a[j].date);
            //contentList.appendChild(a[j].node);
            frag.appendChild(a[j].node);
            //alert(j);
        }
        contentList.appendChild(frag);
    }
    sortBBS();

};
function $(id){
    return document.getElementById(id);
}
function getByClass(className,context){
    var context = context || document;
    if(context.getElementsByClassName){
        return context.getElementsByClassName(className);
    }
    var nodes = context.getElementsByTagName('*');
    var ret = [];
    for(var i=0; i<nodes.length; i++){
        if(hasClass(nodes[i],className))
            ret.push(nodes[i]);
    }
    return ret;
}
function hasClass(node,className) {
    var names=node.className.split(/\s+/);
    for (var i=0;i<names.length;i++) {
        if (names[i]==className) return true;
    }
    return false;
}
function date(s,t){
    //s Y-m-d H:i:s
    //t new Date().getTime()
    t = t || new Date();
    var re = /Y|m|d|H|i|s/g;
    /*s.replace(re,function($1,$2,$3){
        alert($1 + "\n" + $2 + "\n" + $3);
        return "";
    });*/
    return s=s.replace(re,function($1){
        /*if($1 == "Y") return t.getFullYear();
        else if($1 == "m") return t.getMonth()+1;
        else if($1 == "d") return t.getDate();
        return $1;*/
        switch($1){
            case "Y":return t.getFullYear();
            case "m":return t.getMonth();
            case "d":return t.getDate();
            case "H":return t.getHours();
            case "i":return t.getMinutes();
            case "s":return t.getSeconds();
        }
        return $1;
    });

    
}

//console.log(date("Y-m-d"));

//避免每次都创建
/*var div = document.createElement("div");
var t = document.createTextNode("");
div.appendChild(t);*/
function htmlEncode(html){
    //return html.replace("<","&lt;").replace(">","&gt;");
    /*var div = document.createElement("div");
    var t = document.createTextNode(html);
    div.appendChild(t);*/
    /*t.nodeValue = html;
    return div.innerHTML;*/
    arguments.callee.textNode.nodeValue = html;
    return arguments.callee.div.innerHTML;
};
htmlEncode.div = document.createElement("div");
htmlEncode.textNode = document.createTextNode("");
htmlEncode.div.appendChild(htmlEncode.textNode);

//变量只执行一次 执行到才可以使用
/*(function(){
    var div = document.createElement("div");
    var t = document.createTextNode("");
    div.appendChild(t);
    function htmlEncode(html){
        t.nodeValue = html;
        return div.innerHTML;
    }
    window.htmlEncode = htmlEncode;
})();*/

var htmlEncode = (function(){
    var div = document.createElement("div");
    var t = document.createTextNode("");
    div.appendChild(t);
    return function htmlEncode(html){
        t.nodeValue = html;
        return div.innerHTML;
    }
})();

//console.log(htmlEncode("<span>ssss</span>"));

/*var a = [21,3,54,5,22,5325];
a.sort(function(a,b){
    console.log(a + "\n" + b);
    return a>b?1:-1;
});
console.log(a);*/

/*var a = [234,42,5,52,6];
a.sort(function(a,b){
    if(a>b)
        return 1;
    else if(a<b)
        return -1;
    else
        return 0;
});
console.log(a);*/
