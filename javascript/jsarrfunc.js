<script type="text/javascript">
    /*
     *author : gan
     *time : 2011/10/16
     *content : 模拟ECMAScript 5 的数组函数
     *
     *
     * */


Array.prototype.join = function (s) {
    var s = s || ',';
    var str = "";
    for( var i=0; i<this.length; i++ ) {
        //// Skip undefined + nonexistent elements
        if (a[i] === undefined) continue;
        //if (!a[i]) continue; // Skip null, undefined, and nonexistent elements
        //if (!(i in a)) continue ; // Skip nonexistent elements
        if ( i  == this.length-1 ) {
            str += this[i];
        } else {
            str += this[i] + s;
        }
        
    }
    return str;
    
};

Array.prototype.reverse = function () {
    var arr = [];
    for ( var i=this.length; i>=0; i--) {
        if ( this[i] == undefined ) {
            continue;
        } else {
            arr.push( this[i] );
        }
    }
    return arr;
};

Array.prototype.push = function () {
    for ( var i=0; i<arguments.length; i++ ) {
        this[this.length + i] = arguments[i];
    }
};

Array.prototype.toString = function () {
    return this.join(",");
};

Array.prototype.sort = function (f) {
    if ( f == undefined ) {
        for ( var i=0; i<this.length; i++ ) {
            this[i] = String(this[i]);
        }

        for ( var j=0; j<this.length-1; j++ ) {
            for ( var k=0; k<this.length-j-1; k++ ) {
                if ( this[k] > this[k+1] ) {
                    var t = this[k];
                    this[k] = this[k+1];
                    this[k+1] = t;
                }
            }
        }
    } else if ( typeof f == 'function' ) {

        for ( var j=0; j<this.length-1; j++ ) {
            for ( var k=0; k<this.length-j-1; k++ ) {
                if ( f( this[k], this[k+1] ) > 0 ) {
                    var t = this[k];
                    this[k] = this[k+1];
                    this[k+1] = t;
                }
            }
        }

    }
    
};


Array.prototype.concat = function () {
    for ( var i=0; i<arguments.length; i++ ) {
        this.push( arguments[i] );
    }
    return this;
};

var isArray = Function.isArray || function(o) {
    return typeof o === "object" && Object.prototype.toString.call(o) === "[object Array]";
}

Array.prototype.pop = function () {
    var s = this[this.length - 1];
    delete this[this.length-1];
    this.length = this.length - 1;
    return s;
};

Array.prototype.shift = function () {
    var s = this[0];
    delete this[0];
    return s;
};

Array.prototype.map = function (f) {
    for ( var i=0; i<this.length; i++ ) {
        this[i] = f( this[i] );
    }
    return this;
};

Array.prototype.filter = function (f) {
    var arr = [];
    for ( var i=0; i<this.length; i++ ) {
        if ( !!f(this[i]) ) {
            arr.push( this[i] );
        }        
    }
    return arr;
};

Array.prototype.every = function (f) {
    var flag = true;
    for ( var i=0; i<this.length; i++ ) {
        if ( !f(this[i]) ) {
            flag = false;
        }
    }
    return flag;
};

Array.prototype.some = function (f) {
    var flag = false;
    for ( var i=0; i<this.length; i++ ) {
        if ( !!f(this[i]) ) {
            flag = true;
            break;
        }
    }
    return flag;
};



Array.prototype.forEach = function (f) {
    for ( var i=0; i<this.length; i++ ) {
        f(this[i], i, this);
    }
};

Array.prototype.indexOf = function (element,start) {
    var start = start || 0;
    for ( var i=start; i<this.length; i++ ) {
        if ( this[i] === element ) {
            return i;
        }
    }
    return -1;
};

Array.prototype.length = ( function() {
    var num = 0;
    for ( var i=0; i<this.length; i++ ) {
        num++;
    }
    return num;
} )();



Array.prototype.slice = function (start,end) {
    var arr = [];
    var end = end || this.length;
    var start = start || this.length;
    if ( end <= this.length) {
        if ( end < 0 ) {
            end = end + this.length;
        }
        if ( start < 0) {
            start = start + this.length;
        }
        if ( start <= end ) {
            for ( var i=start; i<end; i++ ) {
                arr.push(this[i]);
            }
        }
    }
    return arr;
};


</script>
