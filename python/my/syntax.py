#####################################################################################
Assignment Creating references a, *b = 'good', 'bad', 'ugly'
Calls and other expressions Running functions log.write("spam, ham")
print calls Printing objects print('The Killer', joke)
if/elif/else Selecting actions if "python" in text:
print(text)
for/else Sequence iteration for x in mylist:
print(x)
while/else General loops while X > Y:
print('hello')
pass Empty placeholder while True:
pass
break Loop exit while True:
if exittest(): break
continue Loop continue while True:
if skiptest(): continue
def Functions and methods def f(a, b, c=1, *d):
print(a+b+c+d[0])
return Functions results def f(a, b, c=1, *d):
return a+b+c+d[0]
yield Generator functions def gen(n):
for i in n: yield i*2
global Namespaces x = 'old'
def function():
global x, y; x = 'new'
nonlocal Namespaces (3.0+) def outer():
x = 'old'
def function():
nonlocal x; x = 'new'
import Module access import sys
from Attribute access from sys import stdin
class Building objects class Subclass(Superclass):
staticData = []
def method(self): pass
try/except/ finally Catching exceptions try:
action()
except:
print('action error')
raise Triggering exceptions raise EndSearch(location)
assert Debugging checks assert X > Y, 'X too small'
with/as Context managers (2.6+) with open('data') as myfile:
process(myfile)
del Deleting references del data[k]
del data[i:j]
del obj.attr
del variable
#####################################################################################
    ////////////////////////分配

    spam = 'Spam' Basic form
    spam, ham = 'yum', 'YUM' Tuple assignment (positional)
    [spam, ham] = ['yum', 'YUM'] List assignment (positional)
    a, b, c, d = 'spam' Sequence assignment, generalized
    a, *b = 'spam' Extended sequence unpacking (Python 3.0)
    spam = ham = 'lunch' Multiple-target assignment
    spams += 42 Augmented assignment (equivalent to spams = spams + 42)
    
    % python
    >>> nudge = 1
    >>> wink = 2
    >>> A, B = nudge, wink # Tuple assignment
    >>> A, B # Like A = nudge; B = wink
    (1, 2)
    >>> [C, D] = [nudge, wink] # List assignment
    >>> C, D
    (1, 2)

    >>> nudge = 1
    >>> wink = 2
    >>> nudge, wink = wink, nudge # Tuples: swaps values
    >>> nudge, wink # Like T = nudge; nudge = wink; wink = T
    (2, 1)

    >>> [a, b, c] = (1, 2, 3) # Assign tuple of values to list of names
    >>> a, c
    (1, 3)
    >>> (a, b, c) = "ABC" # Assign string of characters to tuple
    >>> a, c
    ('A', 'C')

    >>> string = 'SPAM'
    >>> a, b, c, d = string # Same number on both sides
    >>> a, d
    ('S', 'M')
    >>> a, b, c = string # Error if not
    ...error text omitted...
    ValueError: too many values to unpack
    
    >>> a, b, c = string[0], string[1], string[2:] # Index and slice
    >>> a, b, c
    ('S', 'P', 'AM')
    >>> a, b, c = list(string[:2]) + [string[2:]] # Slice and concatenate
    >>> a, b, c
    ('S', 'P', 'AM')
    >>> a, b = string[:2] # Same, but simpler
    >>> c = string[2:]
    >>> a, b, c
    ('S', 'P', 'AM')
    >>> (a, b), c = string[:2], string[2:] # Nested sequences
    >>> a, b, c
    ('S', 'P', 'AM')
    
    >>> ((a, b), c) = ('SP', 'AM') # Paired by shape and position
    >>> a, b, c
    ('S', 'P', 'AM')
    
    for (a, b, c) in [(1, 2, 3), (4, 5, 6)]: ... # Simple tuple assignment
    for ((a, b), c) in [((1, 2), 3), ((4, 5), 6)]: ... # Nested tuple assignment

    def f(((a, b), c)): # For arguments too in Python 2.6, but not 3.0
    f(((1, 2), 3))

    >>> red, green, blue = range(3)
    >>> red, blue
    (0, 2)

    >>> seq
    [1, 2, 3, 4]
    >>> a, b, c, *d = seq
    >>> print(a, b, c, d)
    1 2 3 [4]
    
    >>> a, b, c, d, *e = seq
    >>> print(a, b, c, d, e)
    1 2 3 4 []
    >>> a, b, *e, c, d = seq
    >>> print(a, b, c, d, e)
    1 2 3 4 []
    
    >>> a, *b, c, *d = seq
    SyntaxError: two starred expressions in assignment
    >>> a, b = seq
    ValueError: too many values to unpack
    >>> *a = seq
    SyntaxError: starred assignment target must be in a list or tuple
    >>> *a, = seq
    >>> a
    [1, 2, 3, 4]
    
    >>> seq
    [1, 2, 3, 4]
    >>> a, *b = seq # First, rest
    >>> a, b
    (1, [2, 3, 4])
    >>> a, b = seq[0], seq[1:] # First, rest: traditional
    >>> a, b
    (1, [2, 3, 4])
    
    >>> *a, b = seq # Rest, last
    >>> a, b
    ([1, 2, 3], 4)
    >>> a, b = seq[:-1], seq[-1] # Rest, last: traditional
    >>> a, b
    ([1, 2, 3], 4)
    
    for (a, *b, c) in [(1, 2, 3, 4), (5, 6, 7, 8)]:
    ...
    
    a, *b, c = (1, 2, 3, 4) # b gets [2, 3]
    for (a, b, c) in [(1, 2, 3), (4, 5, 6)]: # a, b, c = (1, 2, 3), ...

    for all in [(1, 2, 3, 4), (5, 6, 7, 8)]:
    a, b, c = all[0], all[1:3], all[3]

    >>> a = b = c = 'spam'
    >>> a, b, c
    ('spam', 'spam', 'spam')

    >>> c = 'spam'
    >>> b = c
    >>> a = b

    >>> a = b = 0
    >>> b = b + 1
    >>> a, b
    (0, 1)
    
    >>> a = b = []
    >>> b.append(42)
    >>> a, b
    ([42], [42])

    >>> a = []
    >>> b = []
    >>> b.append(42)
    >>> a, b
    ([], [42])


X += Y X &= Y X -= Y X |= Y
X *= Y X ^= Y X /= Y X >>= Y
X %= Y X <<= Y X **= Y X //= Y
    
    >>> x = 1
    >>> x = x + 1 # Traditional
    >>> x
    2
    >>> x += 1 # Augmented
    >>> x
    3

    >>> S = "spam"
    >>> S += "SPAM" # Implied concatenation
    >>> S
    'spamSPAM'

    >>> L = [1, 2]
    >>> L = L + [3] # Concatenate: slower
    >>> L
    [1, 2, 3]
    >>> L.append(4) # Faster, but in-place
    >>> L
    [1, 2, 3, 4]
    
    >>> L = L + [5, 6] # Concatenate: slower
    >>> L
    [1, 2, 3, 4, 5, 6]
    >>> L.extend([7, 8]) # Faster, but in-place
    >>> L
    [1, 2, 3, 4, 5, 6, 7, 8]

    >>> L += [9, 10] # Mapped to L.extend([9, 10])
    >>> L
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    >>> L = [1, 2]
    >>> M = L # L and M reference the same object
    >>> L = L + [3, 4] # Concatenation makes a new object
    >>> L, M # Changes L but not M
    ([1, 2, 3, 4], [1, 2])
    >>> L = [1, 2]
    >>> M = L
    >>> L += [3, 4] # But += really means extend
    >>> L, M # M sees the in-place change too!
    ([1, 2, 3, 4], [1, 2, 3, 4])

######################################print子句

    C:\misc> c:\python30\python
    >>>
    >>> print() # Display a blank line
    >>> x = 'spam'
    >>> y = 99
    >>> z = ['eggs']
    >>>
    >>> print(x, y, z) # Print 3 objects per defaults
    spam 99 ['eggs']

    >>> print(x, y, z, sep='') # Suppress separator
    spam99['eggs']
    >>>
    >>> print(x, y, z, sep=', ') # Custom separator
    spam, 99, ['eggs']

    >>> print(x, y, z, end='') # Suppress line break
    spam 99 ['eggs']>>>
    >>>
    >>> print(x, y, z, end=''); print(x, y, z) # Two prints, same output line
    spam 99 ['eggs']spam 99 ['eggs']
    >>> print(x, y, z, end='...\n') # Custom line end
    spam 99 ['eggs']...
    >>>

    >>> print(x, y, z, sep='...', end='!\n') # Multiple keywords
    spam...99...['eggs']!
    >>> print(x, y, z, end='!\n', sep='...') # Order doesn't matter
    spam...99...['eggs']!

    >>> print(x, y, z, sep='...', file=open('data.txt', 'w')) # Print to a file
    >>> print(x, y, z) # Back to stdout
    spam 99 ['eggs']
    >>> print(open('data.txt').read()) # Display file text
    spam...99...['eggs']

    >>> text = '%s: %-.4f, %05d' % ('Result', 3.14159, 42)
    >>> print(text)
    Result: 3.1416, 00042
    >>> print('%s: %-.4f, %05d' % ('Result', 3.14159, 42))
    Result: 3.1416, 00042

    C:\misc> c:\python26\python
    >>>
    >>> x = 'a'
    >>> y = 'b'
    >>> print x, y
    a b

    >>> print x, y,; print x, y
    a b a b

    >>> print x + y
    ab
    >>> print '%s...%s' % (x, y)
    a...b

###########重定向

    >>> print('hello world') # Print a string object in 3.0
    hello world
    >>> print 'hello world' # Print a string object in 2.6
    hello world

    >>> 'hello world' # Interactive echoes
    'hello world'

    >>> import sys # Printing the hard way
    >>> sys.stdout.write('hello world\n')
    hello world

    import sys
    sys.stdout.write(str(X) + ' ' + str(Y) + '\n')

    import sys
    sys.stdout = open('log.txt', 'a') # Redirects prints to a file
    ...
    print(x, y, x) # Shows up in log.txt

    C:\misc> c:\python30\python
    >>> import sys
    >>> temp = sys.stdout # Save for restoring later
    >>> sys.stdout = open('log.txt', 'a') # Redirect prints to a file
    >>> print('spam') # Prints go to file, not here
    >>> print(1, 2, 3)
    >>> sys.stdout.close() # Flush output to disk
    >>> sys.stdout = temp # Restore original stream
    >>> print('back here') # Prints show up here again
    back here
    >>> print(open('log.txt').read()) # Result of earlier prints
    spam
    1 2 3

    log = open('log.txt', 'a') # 3.0
    print(x, y, z, file=log) # Print to a file-like object
    print(a, b, c) # Print to original stdout
    log = open('log.txt', 'a') # 2.6
    print >> log, x, y, z # Print to a file-like object
    print a, b, c # Print to original stdout

    C:\misc> c:\python30\python
    >>> log = open('log.txt', 'w')
    >>> print(1, 2, 3, file=log) # 2.6: print >> log, 1, 2, 3
    >>> print(4, 5, 6, file=log)
    >>> log.close()
    >>> print(7, 8, 9) # 2.6: print 7, 8, 9
    7 8 9
    >>> print(open('log.txt').read())
    1 2 3
    4 5 6

    >>> import sys
    >>> sys.stderr.write(('Bad!' * 8) + '\n')
    Bad!Bad!Bad!Bad!Bad!Bad!Bad!Bad!
    >>> print('Bad!' * 8, file=sys.stderr) # 2.6: print >> sys.stderr, 'Bad' * 8
    Bad!Bad!Bad!Bad!Bad!Bad!Bad!Bad!

    >>> X = 1; Y = 2
    >>> print(X, Y) # Print: the easy way
    1 2
    >>> import sys # Print: the hard way
    >>> sys.stdout.write(str(X) + ' ' + str(Y) + '\n')
    1 2
    4
    >>> print(X, Y, file=open('temp1', 'w')) # Redirect text to file
    >>> open('temp2', 'w').write(str(X) + ' ' + str(Y) + '\n') # Send to file manually
    4
    >>> print(open('temp1', 'rb').read()) # Binary mode for bytes
    b'1 2\r\n'
    >>> print(open('temp2', 'rb').read())
    b'1 2\r\n'

*********************************************************************************
*                               if
*********************************************************************************

    >>> if 1:
    ... print('true')
    ...
    true

    >>> if not 1:
    ... print('true')
    ... else:
    ... print('false')
    ...
    false

    >>> x = 'killer rabbit'
    >>> if x == 'roger':
    ...     print("how's jessica?")
    ... elif x == 'bugs':
    ...     print("what's up doc?")
    ... else:
    ...     print('Run away! Run away!')
    ...
    Run away! Run away!

    >>> choice = 'ham'
    >>> print({'spam': 1.25, # A dictionary-based 'switch'
    ...        'ham': 1.99, # Use has_key or get for default
    ...        'eggs': 0.99,
    ...        'bacon': 1.10}[choice])
    1.99

    >>> if choice == 'spam':
    ...     print(1.25)
    ... elif choice == 'ham':
    ...     print(1.99)
    ... elif choice == 'eggs':
    ...     print(0.99)
    ... elif choice == 'bacon':
    ...     print(1.10)
    ... else:
    ...     print('Bad choice')
    ...
    1.99

    >>> branch = {'spam': 1.25,
    ...           'ham': 1.99,
    ...           'eggs': 0.99}
    >>> print(branch.get('spam', 'Bad choice'))
    1.25
    >>> print(branch.get('bacon', 'Bad choice'))
    Bad choice

    >>> choice = 'bacon'
    >>> if choice in branch:
    ...     print(branch[choice])
    ... else:
    ...     print('Bad choice')
    ...
    Bad choice

################缩进规则

    x = 1
    if x:
        y = 2
        if y:
            print('block2')
        print('block1')
    print('block0')

    X and Y
        Is true if both X and Y are true
    X or Y
        Is true if either X or Y is true
    not X
        Is true if X is false (the expression returns True or False)

    >>> 2 < 3, 3 < 2 # Less-than: return True or False (1 or 0)
    (True, False)

    >>> 2 or 3, 3 or 2 # Return left operand if true
    (2, 3) # Else, return right operand (true or false)
    >>> [] or 3
    3
    >>> [] or {}
    {}

    >>> 2 and 3, 3 and 2 # Return left operand if false
    (3, 2) # Else, return right operand (true or false)
    >>> [] and {}
    []
    >>> 3 and []
    []

    if X:
        A = Y
    else:
        A = Z

    A = Y if X else Z

    >>> A = 't' if 'spam' else 'f' # Nonempty is true
    >>> A
    't'
    >>> A = 't' if '' else 'f'
    >>> A
    'f'

    A = ((X and Y) or Z)

    A = [Z, Y][bool(X)]

    >>> ['f', 't'][bool('')]
    'f'
    >>> ['f', 't'][bool('spam')]
    't'
    
############################################################################
#                            while                                         #
############################################################################
    while <test>: # Loop test
        <statements1> # Loop body
    else: # Optional else
        <statements2> # Run if didn't exit loop with break
        

    >>> while True:
    ...     print('Type Ctrl-C to stop me!')
    
    >>> x = 'spam'
    >>> while x: # While x is not empty
    ...     print(x, end=' ')
    ...     x = x[1:] # Strip first character off x
    ...
    spam pam am m
    
    >>> a=0; b=10
    >>> while a < b: # One way to code counter loops
    ...     print(a, end=' ')
    ...     a += 1 # Or, a = a + 1
    ...
    0 1 2 3 4 5 6 7 8 9
        
    while True:
        ...loop body...
        if exitTest(): break
            
    break
        Jumps out of the closest enclosing loop (past the entire loop statement)
    continue
        Jumps to the top of the closest enclosing loop (to the loop’s header line)
    pass
        Does nothing at all: it’s an empty statement placeholder
    Loop else block
        Runs if and only if the loop is exited normally (i.e., without hitting a break)

    while <test1>:
        <statements1>
        if <test2>: break # Exit loop now, skip else
        if <test3>: continue # Go to top of loop now, to test1
    else:
        <statements2> # Run if we didn't hit a 'break'
        
    while True: pass # Type Ctrl-C to stop me!

    def func1():
        pass # Add real code here later
    def func2():
        pass
--------------------------continue
    x = 10
    while x:
        x = x−1 # Or, x -= 1
        if x % 2 != 0: continue # Odd? -- skip print
        print(x, end=' ')
    
    x = 10
    while x:
        x = x−1
        if x % 2 == 0: # Even? -- print
            print(x, end=' ')

---------------------------break

    >>> while True:
    ...     name = input('Enter name:')
    ...     if name == 'stop': break
    ...     age = input('Enter age: ')
    ...     print('Hello', name, '=>', int(age) ** 2)
    ...
    Enter name:mel
    Enter age: 40
    Hello mel => 1600
    Enter name:bob
    Enter age: 30
    Hello bob => 900
    Enter name:stop
        
----------------------Loop else
    x = y // 2 # For some y > 1
    while x > 1:
        if y % x == 0: # Remainder
            print(y, 'has factor', x)
            break # Skip else
        x -= 1
    else: # Normal exit
        print(y, 'is prime')

    found = False
    while x and not found:
        if match(x[0]): # Value at front?
            print('Ni')
            found = True
        else:
            x = x[1:] # Slice off front and repeat
    if not found:
        print('not found')

    while x: # Exit when x empty
        if match(x[0]):
            print('Ni')
            break # Exit, go around else
        x = x[1:]
    else:
        print('Not found') # Only here if exhausted x

----------------------------------for Loops
    //基本格式
    for <target> in <object>: # Assign object items to target
        <statements> # Repeated loop body: use target
    else:
        <statements> # If we didn't hit a 'break'

    for <target> in <object>: # Assign object items to target
        <statements>
        if <test>: break # Exit loop now, skip else
        if <test>: continue # Go to top of loop now
    else:
        <statements> # If we didn't hit a 'break'

    //事例
    >>> for x in ["spam", "eggs", "ham"]:
    ...     print(x, end=' ')
    ...
    spam eggs ham
    
    >>> sum = 0
    >>> for x in [1, 2, 3, 4]:
    ...     sum = sum + x
    ...
    >>> sum
    10
    >>> prod = 1
    >>> for item in [1, 2, 3, 4]: prod *= item
    ...
    >>> prod
    24
    
    >>> S = "lumberjack"
    >>> T = ("and", "I'm", "okay")
    >>> for x in S: print(x, end=' ') # Iterate over a string
    ...
    l u m b e r j a c k
    >>> for x in T: print(x, end=' ') # Iterate over a tuple
    ...
    and I'm okay #'
    
    >>> T = [(1, 2), (3, 4), (5, 6)]
    >>> for (a, b) in T: # Tuple assignment at work
    ...     print(a, b)
    ...
    1 2
    3 4
    5 6
    
    >>> D = {'a': 1, 'b': 2, 'c': 3}
    >>> for key in D:
    ...     print(key, '=>', D[key]) # Use dict keys iterator and index
    ...
    a => 1
    c => 3
    b => 2
    >>> list(D.items())
    [('a', 1), ('c', 3), ('b', 2)]
    >>> for (key, value) in D.items():
    ...     print(key, '=>', value) # Iterate over both keys and values
    ...
    a => 1
    c => 3
    b => 2

    >>> T
    [(1, 2), (3, 4), (5, 6)]
    >>> for both in T:
    ...     a, b = both # Manual assignment equivalent
    ...     print(a, b)
    ...
    1 2
    3 4
    5 6

    >>> ((a, b), c) = ((1, 2), 3) # Nested sequences work too
    >>> a, b, c
    (1, 2, 3)
    >>> for ((a, b), c) in [((1, 2), 3), ((4, 5), 6)]: print(a, b, c)
    ...
    1 2 3
    4 5 6

    >>> for ((a, b), c) in [([1, 2], 3), ['XY', 6]]: print(a, b, c)
    ...
    1 2 3
    X Y 6

    //Python 3.0 extended sequence assignment in for loops

    >>> a, b, c = (1, 2, 3) # Tuple assignment
    >>> a, b, c
    (1, 2, 3)
    >>> for (a, b, c) in [(1, 2, 3), (4, 5, 6)]: # Used in for loop
    ...     print(a, b, c)
    ...
    1 2 3
    4 5 6

    >>> a, *b, c = (1, 2, 3, 4) # Extended seq assignment
    >>> a, b, c
    (1, [2, 3], 4)
    >>> for (a, *b, c) in [(1, 2, 3, 4), (5, 6, 7, 8)]:
    ...     print(a, b, c)
    ...
    1 [2, 3] 4
    5 [6, 7] 8

    >>> list(range(5)), list(range(2, 5)), list(range(0, 10, 2))
    ([0, 1, 2, 3, 4], [2, 3, 4], [0, 2, 4, 6, 8])

    >>> list(range(−5, 5))
    [−5, −4, −3, −2, −1, 0, 1, 2, 3, 4]
    >>> list(range(5, −5, −1))
    [5, 4, 3, 2, 1, 0, −1, −2, −3, −4]

    >>> for i in range(3):
    ... print(i, 'Pythons')
    ...
    0 Pythons
    1 Pythons
    2 Pythons

    >>> X = 'spam'
    >>> for item in X: print(item, end=' ') # Simple iteration
    ...
    s p a m

    >>> i = 0
    >>> while i < len(X): # while loop iteration
    ...     print(X[i], end=' ')
    ...     i += 1
    ...
    s p a m

    >>> X
    'spam'
    >>> len(X) # Length of string
    4
    >>> list(range(len(X))) # All legal offsets into X
    [0, 1, 2, 3]
    >>>
    >>> for i in range(len(X)): print(X[i], end=' ') # Manual for indexing
    ...
    s p a m

    >>> for item in X: print(item) # Simple iteration
    ...

    >>> S = 'abcdefghijk'
    >>> list(range(0, len(S), 2))
    [0, 2, 4, 6, 8, 10]
    >>> for i in range(0, len(S), 2): print(S[i], end=' ')
    ...
    a c e g i k
    
    >>> S = 'abcdefghijk'
    >>> for c in S[::2]: print(c, end=' ')
    ...
    a c e g i k
        
    >>> L = [1, 2, 3, 4, 5]
    >>> for x in L:
    ... x += 1
    ...
    >>> L
    [1, 2, 3, 4, 5]
    >>> x
    6
        
    >>> L = [1, 2, 3, 4, 5]
    >>> for i in range(len(L)): # Add one to each item in L
    ...     L[i] += 1 # Or L[i] = L[i] + 1
    ...
    >>> L
    [2, 3, 4, 5, 6]

    >>> i = 0
    >>> while i < len(L):
    ...     L[i] += 1
    ...     i += 1
    ...
    >>> L
    [3, 4, 5, 6, 7]

    [x+1 for x in L]

    ////////////////////Parallel Traversals: zip and map
    >>> L1 = [1,2,3,4]
    >>> L2 = [5,6,7,8]
        
    >>> zip(L1, L2)
    <zip object at 0x026523C8>
    >>> list(zip(L1, L2)) # list() required in 3.0, not 2.6
    [(1, 5), (2, 6), (3, 7), (4, 8)]
    
    >>> for (x, y) in zip(L1, L2):
    ...     print(x, y, '--', x+y)
    ...
    1 5 -- 6
    2 6 -- 8
    3 7 -- 10
    4 8 -- 12
        
    >>> T1, T2, T3 = (1,2,3), (4,5,6), (7,8,9)
    >>> T3
    (7, 8, 9)
    >>> list(zip(T1, T2, T3))
    [(1, 4, 7), (2, 5, 8), (3, 6, 9)]
    
    >>> S1 = 'abc'
    >>> S2 = 'xyz123'
    >>>
    >>> list(zip(S1, S2))
    [('a', 'x'), ('b', 'y'), ('c', 'z')]
        
    /////map equivalence in Python 2.6
    >>> S1 = 'abc'
    >>> S2 = 'xyz123'
    >>> map(None, S1, S2) # 2.X only
    [('a', 'x'), ('b', 'y'), ('c', 'z'), (None, '1'), (None, '2'), (None,'3')]

    >>> list(map(ord, 'spam'))
    [115, 112, 97, 109]
    
    >>> res = []
    >>> for c in 'spam': res.append(ord(c))
    >>> res
    [115, 112, 97, 109]
    
    >>> D1 = {'spam':1, 'eggs':3, 'toast':5}
    >>> D1
    {'toast': 5, 'eggs': 3, 'spam': 1}
    >>> D1 = {}
    >>> D1['spam'] = 1
    >>> D1['eggs'] = 3
    >>> D1['toast'] = 5

    >>> keys = ['spam', 'eggs', 'toast']
    >>> vals = [1, 3, 5]
        
    >>> list(zip(keys, vals))
    [('spam', 1), ('eggs', 3), ('toast', 5)]
    >>> D2 = {}
    >>> for (k, v) in zip(keys, vals): D2[k] = v
    ...
    >>> D2
    {'toast': 5, 'eggs': 3, 'spam': 1}
    
    >>> keys = ['spam', 'eggs', 'toast']
    >>> vals = [1, 3, 5]
    >>> D3 = dict(zip(keys, vals))
    >>> D3
    {'toast': 5, 'eggs': 3, 'spam': 1}

    >> S = 'spam'
    >>> offset = 0
    >>> for item in S:
    ...     print(item, 'appears at offset', offset)
    ...     offset += 1
    ...
    s appears at offset 0
    p appears at offset 1
    a appears at offset 2
    m appears at offset 3

    >>> S = 'spam'
    >>> for (offset, item) in enumerate(S):
    ...     print(item, 'appears at offset', offset)
    ...
    s appears at offset 0
    p appears at offset 1
    a appears at offset 2
    m appears at offset 3

    >>> E = enumerate(S)
    >>> E
    <enumerate object at 0x02765AA8>
    >>> next(E)
    (0, 's')
    >>> next(E)
    (1, 'p')
    >>> next(E)
    (2, 'a')

    >>> [c * i for (i, c) in enumerate(S)]
    ['', 'p', 'aa', 'mmm']

