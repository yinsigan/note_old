##############################################################
    D = {} Empty dictionary
    D = {'spam': 2, 'eggs': 3} Two-item dictionary
    D = {'food': {'ham': 1, 'egg': 2}} Nesting
    D = dict(name='Bob', age=40)
    D = dict(zip(keyslist, valslist))
    D = dict.fromkeys(['a', 'b'])
    Alternative construction techniques:
    keywords, zipped pairs, key lists
    D['eggs']
    D['food']['ham']
    Indexing by key
    'eggs' in D Membership: key present test
    D.keys()
    D.values()
    D.items()
    D.copy()
    D.get(key, default)
    D.update(D2)
    D.pop(key)
    Methods: keys,
    values,
    keys+values,
    copies,
    defaults,
    merge,
    delete, etc.
    len(D) Length: number of stored entries
    D[key] = 42 Adding/changing keys
    del D[key] Deleting entries by key
    list(D.keys())
    D1.keys() & D2.keys()
    Dictionary views (Python 3.0)
    D = {x: x*2 for x in range(10)} Dictionary comprehensions (Python 3.0)
##############################################################
    
    ***********字典基本操作

    % python
    >>> D = {'spam': 2, 'ham': 1, 'eggs': 3} # Make a dictionary
    >>> D['spam'] # Fetch a value by key
    2
    >>> D # Order is scrambled
    {'eggs': 3, 'ham': 1, 'spam': 2}
    
    >>> len(D) # Number of entries in dictionary
    3
    >>> 'ham' in D # Key membership test alternative
    True
    >>> list(D.keys()) # Create a new list of my keys
    ['eggs', 'ham', 'spam']
    
    >>> D
    {'eggs': 3, 'ham': 1, 'spam': 2}
    >>> D['ham'] = ['grill', 'bake', 'fry'] # Change entry
    >>> D
    {'eggs': 3, 'ham': ['grill', 'bake', 'fry'], 'spam': 2}
    >>> del D['eggs'] # Delete entry
    >>> D
    {'ham': ['grill', 'bake', 'fry'], 'spam': 2}
    >>> D['brunch'] = 'Bacon' # Add new entry
    >>> D
    {'brunch': 'Bacon', 'ham': ['grill', 'bake', 'fry'], 'spam': 2}
    
    *****************字典方法***************
    ////////////////values() items()
    >>> D = {'spam': 2, 'ham': 1, 'eggs': 3}
    >>> list(D.values())
    [3, 1, 2]
    >>> list(D.items())
    [('eggs', 3), ('ham', 1), ('spam', 2)]
    
    >>> D.get('spam') # A key that is there
    2
    >>> print(D.get('toast')) # A key that is missing
    None
    >>> D.get('toast', 88)
    88
    
    >>> D
    {'eggs': 3, 'ham': 1, 'spam': 2}
    >>> D2 = {'toast':4, 'muffin':5}
    >>> D.update(D2)
    >>> D
    {'toast': 4, 'muffin': 5, 'eggs': 3, 'ham': 1, 'spam': 2}
    
    # pop a dictionary by key
    >>> D
    {'toast': 4, 'muffin': 5, 'eggs': 3, 'ham': 1, 'spam': 2}
    >>> D.pop('muffin')
    5
    >>> D.pop('toast') # Delete and return from a key
    4
    >>> D
    {'eggs': 3, 'ham': 1, 'spam': 2}
# pop a list by position
    >>> L = ['aa', 'bb', 'cc', 'dd']
    >>> L.pop() # Delete and return from the end
    'dd'
    >>> L
    ['aa', 'bb', 'cc']
    >>> L.pop(1) # Delete from a specific position
    'bb'
    >>> L
    ['aa', 'cc']
    
    ////////////////A Languages Table
    >>> table = {'Python': 'Guido van Rossum',
    ... 'Perl': 'Larry Wall',
    ... 'Tcl': 'John Ousterhout' }
    >>>
    >>> language = 'Python'
    >>> creator = table[language]
    >>> creator
    'Guido van Rossum'
    >>> for lang in table: # Same as: for lang in table.keys()
    ... print(lang, '\t', table[lang])
    ...
    Tcl John Ousterhout
    Python Guido van Rossum
    Perl Larry Wall

    $$$$$$$$$$$$$$$$$$$$$$$字典用法
    //Sequence operations don’t work
    //Assigning to new indexes adds entries.
    //Keys need not always be strings
    
    >>> L = []
    >>> L[99] = 'spam'
    Traceback (most recent call last):
    File "<stdin>", line 1, in ?
    IndexError: list assignment index out of range
    
    >>> D = {}
    >>> D[99] = 'spam'
    >>> D[99]
    'spam'
    >>> D
    {99: 'spam'}
    
    $$$$$$$$$$$$$$$$$$$$$$$$$$

    >>> Matrix = {}
    >>> Matrix[(2, 3, 4)] = 88
    >>> Matrix[(7, 8, 9)] = 99
    >>>
    >>> X = 2; Y = 3; Z = 4 # ; separates statements
    >>> Matrix[(X, Y, Z)]
    88
    >>> Matrix
    {(2, 3, 4): 88, (7, 8, 9): 99}

    >>> Matrix[(2,3,6)]
    Traceback (most recent call last):
    File "<stdin>", line 1, in ?
    KeyError: (2, 3, 6)
    
    >>> if (2,3,6) in Matrix: # Check for key before fetch
    ...     print(Matrix[(2,3,6)]) # See Chapter 12 for if/else
    ... else:
    ...     print(0)
    ...
    0
    >>> try:
    ...      print(Matrix[(2,3,6)]) # Try to index
    ... except KeyError: # Catch and recover
    ...      print(0) # See Chapter 33 for try/except
    ...
    0
    >>> Matrix.get((2,3,4), 0) # Exists; fetch and return
    88
    >>> Matrix.get((2,3,6), 0) # Doesn't exist; use default arg
    0

    /////Using dictionaries as “records”
    >>> rec = {}
    >>> rec['name'] = 'mel'
    >>> rec['age'] = 45
    >>> rec['job'] = 'trainer/writer'
    >>>
    >>> print(rec['name'])
    mel

    >>> mel = {'name': 'Mark',
    ...        'jobs': ['trainer', 'writer'],
    ...        'web': 'www.rmi.net/˜lutz',
    ...        'home': {'state': 'CO', 'zip':80513}}
    
    >>> mel['name']
    'Mark'
    >>> mel['jobs']
    ['trainer', 'writer']
    >>> mel['jobs'][1]
    'writer'
    >>> mel['home']['zip']
    80513
    
    ///////////////Other Ways to Make Dictionaries    
    {'name': 'mel', 'age': 45} # Traditional literal expression
    D = {} # Assign by keys dynamically
    D['name'] = 'mel'
    D['age'] = 45
    dict(name='mel', age=45) # dict keyword argument form
    dict([('name', 'mel'), ('age', 45)]) # dict key/value tuples form

    >>> dict.fromkeys(['a', 'b'], 0)
    {'a': 0, 'b': 0}
        
