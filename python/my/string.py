• Single quotes: 'spa"m'
• Double quotes: "spa'm"
• Triple quotes: '''... spam ...''', """... spam ..."""
• Escape sequences: "s\tp\na\0m"
• Raw strings: r"C:\new\test.spm"
• Byte strings in 3.0 (see Chapter 36): b'sp\x01am'
• Unicode strings in 2.6 only (see Chapter 36): u'eggs\u0020spam'

---------单引号和双引号是相同的-------------------
-----------字符串有一种特殊情况就是会隐式进行+------------
例如:
    title = "Meaning " 'of' " Life"//在其他语言是不行的
myfile = open(r'C:\\new\\text.dat', 'w')//不会转义

重复一个字符串n次用*
    例如:
        "-" * 8

print()语句总会在后面输出"\n",如果要改,可以用end
    例如:
        for c in myjob: print(c, end=' ')

判断一个字符,或一个子串在一个字符串中可以简单的用in操作符
>>>'k' in 'hacker'
True
>>>'z' in 'hacker'
False
>>>'spam' in 'abcspamdef'
True


-----------------字符串的切片和索引------------------
>>>S = 'spam'
>>>S[0], S[-2]
('s', 'a')
>>>S[1:3], S[1], S[:-1]
('pa', 'pam', 'spa')

--------------切片扩展----------------------
//头,尾,步长
>>> S = 'abcdefghijklmnop'
>>> S[1:10:2]
'bdfhj'
>>> S[::2]
'acegikmo'

//反过来了
>>> S = 'hello'
>>> S[::−1]
'olleh'

>>> S = 'abcedfg'
>>> S[5:1:−1]
'fdec'
----------------------切片语法-------------------------
>>> 'spam'[1:3] # Slicing syntax
'pa'
>>> 'spam'[slice(1, 3)] # Slice objects
'pa'
>>> 'spam'[::-1]
'maps'
>>> 'spam'[slice(None, None, −1)]
'maps'

-----------把一个其他类型的对象转成字符串用str()-------------
----------repr()返回对象的字符串表达形式-------------
    >>> print(repr('spam')) # Convert to as-code string
    'spam'
    >>> print('spam')
    spam

-------!!注意print()函数,第二个参数在输出时在前面有个空格-----------------
-------在连接string和int 时应用int() str() -------

/////////////////字符代码转化

    >>> ord('s')
    115
    >>> chr(115)
    's'

    ////////////可以执行字符运算
    >>> S = '5'
    >>> S = chr(ord(S) + 1)
    >>> S
    '6'
    >>> S = chr(ord(S) + 1)
    >>> S
    '7'

    ///////////////字符自动转化为数字
    >>> int('5')
    5
    >>> ord('5') - ord('0')
    5
        
    >>> B = '1101' # Convert binary digits to integer with ord
    >>> I = 0
    >>> while B != '':
    ... I = I * 2 + (ord(B[0]) - ord('0'))
    ... B = B[1:]
    ...
    >>> I
    13

    >>> int('1101', 2) # Convert binary to integer: built-in
    13
    >>> bin(13) # Convert integer to binary
    '0b1101'

    /////////////////改变字符串
    >>> S = 'splot'
    >>> S = S.replace('pl', 'pamal')
    >>> S
    'spamalot'

    /////////////////格式化字符串
    >>> 'That is %d %s bird!' % (1, 'dead') # Format expression
    That is 1 dead bird!
    >>> 'That is {0} {1} bird!'.format(1, 'dead') # Format method in 2.6 and 3.0
    'That is 1 dead bird!'
    

------------------------------------字符串方法-----------------------------------------
S.capitalize() S.ljust(width [, fill])
S.center(width [, fill]) S.lower()
S.count(sub [, start [, end]]) S.lstrip([chars])
S.encode([encoding [,errors]]) S.maketrans(x[, y[, z]])
S.endswith(suffix [, start [, end]]) S.partition(sep)
S.expandtabs([tabsize]) S.replace(old, new [, count])
S.find(sub [, start [, end]]) S.rfind(sub [,start [,end]])
S.format(fmtstr, *args, **kwargs) S.rindex(sub [, start [, end]])
S.index(sub [, start [, end]]) S.rjust(width [, fill])
S.isalnum() S.rpartition(sep)
S.isalpha() S.rsplit([sep[, maxsplit]])
S.isdecimal() S.rstrip([chars])
S.isdigit() S.split([sep [,maxsplit]])
S.isidentifier() S.splitlines([keepends])
S.islower() S.startswith(prefix [, start [, end]])
S.isnumeric() S.strip([chars])
S.isprintable() S.swapcase()
S.isspace() S.title()
S.istitle() S.translate(map)
S.isupper() S.upper()
S.join(iterable) S.zfill(width)
-------------------------------------------------------------------------------------------

***********字符串使用事例
    
    ****************find()
    >>> S = 'xxxxSPAMxxxxSPAMxxxx'
    >>> where = S.find('SPAM') # Search for position
    >>> where # Occurs at offset 4
    4
    >>> S = S[:where] + 'EGGS' + S[(where+4):]
    >>> S
    'xxxxEGGSxxxxSPAMxxxx'

    ************************replace()
    >>> S = 'xxxxSPAMxxxxSPAMxxxx'
    >>> S.replace('SPAM', 'EGGS') # Replace all
    'xxxxEGGSxxxxEGGSxxxx'
    >>> S.replace('SPAM', 'EGGS', 1) # Replace one
    'xxxxEGGSxxxxSPAMxxxx'
    
    ***************将字符串分开来就是单个字符成一个列表*************
    >>> S = 'spammy'
    >>> L = list(S)
    >>> L
    ['s', 'p', 'a', 'm', 'm', 'y']

    ******************join()
    >>> 'SPAM'.join(['eggs', 'sausage', 'ham', 'toast'])
    'eggsSPAMsausageSPAMhamSPAMtoast'

    /////////////////////解析字符串

    >>> line = 'aaa bbb ccc'
    >>> cols = line.split()
    >>> cols
    ['aaa', 'bbb', 'ccc']
    
    >>> line = 'bob,hacker,40'
    >>> line.split(',')
    ['bob', 'hacker', '40']
    
    **********************rstrip() upper() isalpha() endswith() startswith()
    >>> line = "The knights who say Ni!\n"
    >>> line.rstrip()
    'The knights who say Ni!'
    >>> line.upper()
    'THE KNIGHTS WHO SAY NI!\n'
    >>> line.isalpha()
    False
    >>> line.endswith('Ni!\n')
    True
    >>> line.startswith('The')
    True
    
    >>> line
    'The knights who say Ni!\n'
    >>> line.find('Ni') != −1 # Search via method call or expression
    True
    >>> 'Ni' in line
    True
    >>> sub = 'Ni!\n'
    >>> line.endswith(sub) # End test via method call or slice
    True
    >>> line[-len(sub):] == sub
    True
    
*******************格式化字符串***********************************

    >>> 'That is %d %s bird!' % (1, 'dead') # Format expression
    That is 1 dead bird!

    >>> exclamation = "Ni"
    >>> "The knights who say %s!" % exclamation
    'The knights who say Ni!'
    >>> "%d %s %d you" % (1, 'spam', 4)
    '1 spam 4 you'
    >>> "%s -- %s -- %s" % (42, 3.14159, [1, 2, 3])
    '42 -- 3.14159 -- [1, 2, 3]'

    >>> x = 1234
    >>> res = "integers: ...%d...%−6d...%06d" % (x, x, x)
    >>> res
    'integers: ...1234...1234 ...001234'

    >>> x = 1.23456789
    >>> x
    1.2345678899999999
    >>> '%e | %f | %g' % (x, x, x)
    '1.234568e+00 | 1.234568 | 1.23457'
    >>> '%E' % x
    '1.234568E+00'

    >>> '%−6.2f | %05.2f | %+06.1f' % (x, x, x)
    '1.23 | 01.23 | +001.2'
    >>> "%s" % x, str(x)
    ('1.23456789', '1.23456789')

    >>> '%f, %.2f, %.*f' % (1/3.0, 1/3.0, 4, 1/3.0)
    '0.333333, 0.33, 0.3333'

////////////////////////////////////////////////////////////////////////////////
    ******************基于字典的字符串格式化表达式**

    >>> "%(n)d %(x)s" % {"n":1, "x":"spam"}
    '1 spam'
    
    >>> reply = """ # Template with substitution targets
    Greetings...
    Hello %(name)s!
    Your age squared is %(age)s
    """
    >>> values = {'name': 'Bob', 'age': 40} # Build up values to substitute
    >>> print(reply % values) # Perform substitutions
    Greetings...
    Hello Bob!
    Your age squared is 40
    
    /////////////////vars()返回一个字典包含存在的变量
    >>> "%(age)d %(food)s" % vars()
    '40 spam'
///////////////////////////////////////////////////////////////////////////////////

###########################字符串格式化方法调用############################

    >>> template = '{0}, {1} and {2}' # By position
    >>> template.format('spam', 'ham', 'eggs')
    'spam, ham and eggs'
    >>> template = '{motto}, {pork} and {food}' # By keyword
    >>> template.format(motto='spam', pork='ham', food='eggs')
    'spam, ham and eggs'
    >>> template = '{motto}, {0} and {food}' # By both
    >>> template.format('ham', motto='spam', food='eggs')
    'spam, ham and eggs'

    //[1,2] 这里可以是任意对象类型
    >>> '{motto}, {0} and {food}'.format(42, motto=3.14, food=[1, 2])
    '3.14, 42 and [1, 2]'

    //返回一个字符串立即输出
    >>> X = '{motto}, {0} and {food}'.format(42, motto=3.14, food=[1, 2])
    >>> X
    '3.14, 42 and [1, 2]'
    >>> X.split(' and ')
    ['3.14, 42', '[1, 2]']
    >>> Y = X.replace('and', 'but under no circumstances')
    >>> Y
    '3.14, 42 but under no circumstances [1, 2]'
    
    >>> import sys
    >>> 'My {1[spam]} runs {0.platform}'.format(sys, {'spam': 'laptop'})
    'My laptop runs win32'
    >>> 'My {config[spam]} runs {sys.platform}'.format(sys=sys,config={'spam': 'laptop'})
    'My laptop runs win32'

    >>> somelist = list('SPAM')
    >>> somelist
    ['S', 'P', 'A', 'M']
    >>> 'first={0[0]}, third={0[2]}'.format(somelist)
    'first=S, third=A'
    >>> 'first={0}, last={1}'.format(somelist[0], somelist[-1]) # [-1] fails in fmt
    'first=S, last=M'
    >>> parts = somelist[0], somelist[-1], somelist[1:3] # [1:3] fails in fmt
    >>> 'first={0}, last={1}, middle={2}'.format(*parts)
    "first=S, last=M, middle=['P', 'A']"

    ##########################[[fill]align][sign][#][0][width][.precision][typecode]
    >>> '{0:10} = {1:10}'.format('spam', 123.4567)
    'spam = 123.457'
    >>> '{0:>10} = {1:<10}'.format('spam', 123.4567)
    ' spam = 123.457 '
    >>> '{0.platform:>10} = {1[item]:<10}'.format(sys, dict(item='laptop'))
    ' win32 = laptop

    >>> '{0:e}, {1:.3e}, {2:g}'.format(3.14159, 3.14159, 3.14159)
    '3.141590e+00, 3.142e+00, 3.14159'
    >>> '{0:f}, {1:.2f}, {2:06.2f}'.format(3.14159, 3.14159, 3.14159)
    '3.141590, 3.14, 003.14'
    
    >>> '{0:X}, {1:o}, {2:b}'.format(255, 255, 255) # Hex, octal, binary
    'FF, 377, 11111111'
    >>> bin(255), int('11111111', 2), 0b11111111 # Other to/from binary
    ('0b11111111', 255, 255)
    >>> hex(255), int('FF', 16), 0xFF # Other to/from hex
    ('0xff', 255, 255)
    >>> oct(255), int('377', 8), 0o377, 0377 # Other to/from octal
    ('0377', 255, 255, 255)
    
    >>> '{0:.2f}'.format(1 / 3.0) # Parameters hardcoded
    '0.33'
    >>> '%.2f' % (1 / 3.0)
    '0.33'
    >>> '{0:.{1}f}'.format(1 / 3.0, 4) # Take value from arguments
    '0.3333'
    >>> '%.*f' % (4, 1 / 3.0) # Ditto for expression
    '0.3333'
    
    >>> '{0:.2f}'.format(1.2345) # String method
    '1.23'
    >>> format(1.2345, '.2f') # Built-in function
    '1.23'
    >>> '%.2f' % 1.2345 # Expression
    '1.23'
    
##############################Comparison to the % Formatting Expression

    print('%s=%s' % ('spam', 42)) # 2.X+ format expression
    print('{0}={1}'.format('spam', 42)) # 3.0 (and 2.6) format method
        
    >>> template = '%s, %s, %s'
    >>> template % ('spam', 'ham', 'eggs') # By position
    'spam, ham, eggs'
    >>> template = '%(motto)s, %(pork)s and %(food)s'
    >>> template % dict(motto='spam', pork='ham', food='eggs') # By key
    'spam, ham and eggs'
    >>> '%s, %s and %s' % (3.14, 42, [1, 2]) # Arbitrary types
    '3.14, 42 and [1, 2]'
# Adding keys, attributes, and offsets
    >>> 'My %(spam)s runs %(platform)s' % {'spam': 'laptop', 'platform': sys.platform}
    'My laptop runs win32'
    >>> 'My %(spam)s runs %(platform)s' % dict(spam='laptop', platform=sys.platform)
    'My laptop runs win32'
    >>> somelist = list('SPAM')
    >>> parts = somelist[0], somelist[-1], somelist[1:3]
    >>> 'first=%s, last=%s, middle=%s' % parts
    "first=S, last=M, middle=['P', 'A']"
    
    # Adding specific formatting
    >>> '%-10s = %10s' % ('spam', 123.4567)
    'spam = 123.4567'
    >>> '%10s = %-10s' % ('spam', 123.4567)
    ' spam = 123.4567 '
    >>> '%(plat)10s = %(item)-10s' % dict(plat=sys.platform, item='laptop')
    ' win32 = laptop '
    
    # Floating-point numbers
    >>> '%e, %.3e, %g' % (3.14159, 3.14159, 3.14159)
    '3.141590e+00, 3.142e+00, 3.14159'
    >>> '%f, %.2f, %06.2f' % (3.14159, 3.14159, 3.14159)
    '3.141590, 3.14, 003.14'
    # Hex and octal, but not binary
    >>> '%x, %o' % (255, 255)
    'ff, 377'
    
    # Hardcoded references in both
    >>> import sys
    >>> 'My {1[spam]:<8} runs {0.platform:>8}'.format(sys, {'spam': 'laptop'})
    'My laptop runs win32'
    >>> 'My %(spam)-8s runs %(plat)8s' % dict(spam='laptop', plat=sys.platform)
    'My laptop runs win32'

    # Building data ahead of time in both
    >>> data = dict(platform=sys.platform, spam='laptop')
    >>> 'My {spam:<8} runs {platform:>8}'.format(**data)
    'My laptop runs win32'
    >>> 'My %(spam)-8s runs %(platform)8s' % data
    'My laptop runs win32'
        
