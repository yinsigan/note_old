#############################################
    L = [] An empty list
    L = [0, 1, 2, 3] Four items: indexes 0..3
    L = ['abc', ['def', 'ghi']] Nested sublists
    L = list('spam')
    L = list(range(-4, 4))
    Lists of an iterable’s items, list of successive integers
    L[i]
    L[i][j]
    L[i:j]
    len(L)

    L1 + L2
    L * 3
    Concatenate, repeat
    for x in L: print(x)
    3 in L
    Iteration, membership
    L.append(4)
    L.extend([5,6,7])
    L.insert(I, X)
    Methods: growing
    L.index(1)
    L.count(X)
    Methods: searching
    L.sort()
    L.reverse()
    Methods: sorting, reversing, etc.
    del L[k]
    del L[i:j]
    L.pop()
    L.remove(2)
    L[i:j] = []
    Methods, statement: shrinking
    L[i] = 1
    L[i:j] = [4,5,6]
    Index assignment, slice assignment
    L = [x**2 for x in range(5)]
    list(map(ord, 'spam'))
    List comprehensions and maps (Chapters 14, 20)

##########################################################################

****************基本的列表操作

    % python
    >>> len([1, 2, 3]) # Length
    3
    >>> [1, 2, 3] + [4, 5, 6] # Concatenation
    [1, 2, 3, 4, 5, 6]
    >>> ['Ni!'] * 4 # Repetition
    ['Ni!', 'Ni!', 'Ni!', 'Ni!']
    
    //列表与字符串的转化
    >>> str([1, 2]) + "34" # Same as "[1, 2]" + "34"
    '[1, 2]34'
    >>> [1, 2] + list("34") # Same as [1, 2] + ["3", "4"]
    [1, 2, '3', '4']
    
    //轮替
    >>> 3 in [1, 2, 3] # Membership
    True
    >>> for x in [1, 2, 3]:
    ...     print(x, end=' ') # Iteration
    ...
    1 2 3
    
    >>> res = [c * 4 for c in 'SPAM'] # List comprehensions
    >>> res
    ['SSSS', 'PPPP', 'AAAA', 'MMMM']
    
    //////////////append()
    >>> res = []
    >>> for c in 'SPAM': # List comprehension equivalent
    ...     res.append(c * 4)
    ...
    >>> res
    ['SSSS', 'PPPP', 'AAAA', 'MMMM']
    
    ***********************map()
    >>> list(map(abs, [−1, −2, 0, 1, 2])) # map function across sequence
    [1, 2, 0, 1, 2]
    

    ****************索引,切片,矩阵
    >>> L = ['spam', 'Spam', 'SPAM!']
    >>> L[2] # Offsets start at zero
    'SPAM!'
    >>> L[−2] # Negative: count from the right
    'Spam'
    >>> L[1:] # Slicing fetches sections
    ['Spam', 'SPAM!']

    >>> matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    >>> matrix[1]
    [4, 5, 6]
    >>> matrix[1][1]
    5
    >>> matrix[2][0]
    7
    >>> matrix = [[1, 2, 3],
    ...           [4, 5, 6],
    ...           [7, 8, 9]]
    >>> matrix[1][1]
    5
    ****************************************

    //////////列表的方法
    >>> L.append('please') # Append method call: add item at end
    >>> L
    ['eat', 'more', 'SPAM!', 'please']
    >>> L.sort() # Sort list items ('S' < 'e')
    >>> L
    ['SPAM!', 'eat', 'more', 'please']
    
    >>> L = ['abc', 'ABD', 'aBe']
    >>> L.sort() # Sort with mixed case
    >>> L
    ['ABD', 'aBe', 'abc']
    >>> L = ['abc', 'ABD', 'aBe']
    >>> L.sort(key=str.lower) # Normalize to lowercase
    >>> L
    ['abc', 'ABD', 'aBe']
    >>>
    >>> L = ['abc', 'ABD', 'aBe']
    >>> L.sort(key=str.lower, reverse=True) # Change sort order
    >>> L
    ['aBe', 'ABD', 'abc']
    
    //这样就有返回值了
    >>> L = ['abc', 'ABD', 'aBe']
    >>> sorted(L, key=str.lower, reverse=True) # Sorting built-in
    ['aBe', 'ABD', 'abc']
    >>> L = ['abc', 'ABD', 'aBe']
    >>> sorted([x.lower() for x in L], reverse=True) # Pretransform items: differs!
    ['abe', 'abd', 'abc']
    
    >>> L = [1, 2]
    >>> L.extend([3,4,5]) # Add many items at end
    >>> L
    [1, 2, 3, 4, 5]
    >>> L.pop() # Delete and return last item
    5
    >>> L
    [1, 2, 3, 4]
    >>> L.reverse() # In-place reversal method
    >>> L
    [4, 3, 2, 1]
    >>> list(reversed(L)) # Reversal built-in with a result
    [1, 2, 3, 4]
    
    >>> L = []
    >>> L.append(1) # Push onto stack
    >>> L.append(2)
    >>> L
    [1, 2]
    >>> L.pop() # Pop off stack
    2
    >>> L
    [1]
    
    >>> L = ['spam', 'eggs', 'ham']
    >>> L.index('eggs') # Index of an object
    1
    >>> L.insert(1, 'toast') # Insert at position
    >>> L
    ['spam', 'toast', 'eggs', 'ham']
    >>> L.remove('eggs') # Delete by value
    >>> L
    ['spam', 'toast', 'ham']
    >>> L.pop(1) # Delete by position
    'toast'
    >>> L
    ['spam', 'ham']
    
    >>> L
    ['SPAM!', 'eat', 'more', 'please']
    >>> del L[0] # Delete one item
    >>> L
    ['eat', 'more', 'please']
    >>> del L[1:] # Delete an entire section
    >>> L # Same as L[1:] = []
    ['eat']
    
    >>> L = ['Already', 'got', 'one']
    >>> L[1:] = []
    >>> L
    ['Already']
    >>> L[0] = []
    >>> L
    [[]]
    
