def isPrime(num):
    # all numbers can be divided by 1,so begins with 2
    i = 2
    isPrime = True
    while(i*i <= num):
        # can be divided by i,so is not prime
        if(num % i ==0):
            isPrime = False
            break
        else:
            i += 1
    return isPrime

print([i for i in range(1,100) if isPrime(i)])


import math
 
def isPrime(n):
    if n <= 1:
        return False
     
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
 
    return True


def isPrime(n):
    if n <= 1:
        return False
  
    i = 2
    while i*i <= n:
        if n % i == 0:
            return False
        i += 1
  
    return True


//高效方法：

def isPrime(n):
    if n <= 1:
        return False
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    i = 3
    while i * i <= n:
        if n % i == 0:
            return False
        i += 2
    return True

