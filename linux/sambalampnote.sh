启动服务: /etc/rc.d/init.d/smb start

Samba有两个守护进程: smbd和nmbd
----smbd监听139TCP端口
----nmbd监听137和138UDP端口

smbd进程的作用是处理SMB请求包,负责用户验证和文件共享;
nmbd进程的作用是处理浏览共享和计算机名称解析。

不可跨路由 可通over TCP/IP

Samba配置文件应存放在:

/etc/samba/smb.conf
    rpm -ql samba-common | grep smb.conf

包括四个设置段
[global] 设置全局环境
[homes] 设置用户宿主目录共享
[printers] 设置打印机共享
[sharefiles] 设置文件共享

注释符号# ;

[global] 段主要选项设置
workgroup = 指定工作组或域
server string = 描述
security = 指定安全模式 #share user server domain
hosts allow = 限定主机访问
host deny #允许优先
log file = 指定日志文件存放位置
max log size = 指定日志文件大小

[homes] 段主要选项
comment = Home Directories
browseable = no # 不可查看,并不是"可查看内容" 只能看到自己的
writable = yes

#######################
Samba应用示例一:
允许用户通过windows客户端访问自己的宿主目录
1.安装Samba, 不需对配置文件做修改,即可实现此功能,如果安装启用了SELinux,
需要先执行:
    setsebool -P samba_enable_home_dirs on
    #iptables -L
    #/etc/selinux/config SELINUX=disabled
2.设置用户Samba验证密码 #用户必须是系统用户
    smbpasswd -a 用户名

3.启动Samba服务
    /etc/rc.d/init.d/smb start

Windows客户端访问Samba服务器共享资源
'开始'->'运行',输入\\Samba服务器地址

Samba服务器查看访问的客户端信息
# smbstatus

