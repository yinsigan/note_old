###################################################################################
# 安装步骤
解压安装

# tar  zxf   samba-3.2.5.tar.gz

#cd  samba-3.2.5

cd source

#./configure
#make && make install 

配置动态链接库

#vim  /etc/ld.so.conf.d/samba.conf
加入/usr/local/samba/lib
然后执行ldconfig让配置生效
#ldconfig

验证samba是否安装成功

#cd /usr/local/samba/bin
#./testparm

如果没任何错误说明samba安装成功,有错误执行
#cp  samba-3.2.5/examples/smb.conf.default   /usr/local/samba/lib/smb.conf
#/usr/local/samba/bin/testparm

启动samba

#cd  /usr/local/samba/sbin
#./nmbd  -D
#./smbd  -D

添加samba用户
#/usr/local/samba/bin/smbpasswd -a  test
NEW SMB password:
Retyoe SMB password:

OK，基本上就 配置完成了


###################################################################################

[global]
	workgroup = WORKGROUP 
    netbios name = gansmb
	server string = This is gan's samba server

    unix charset = utf8
    display charset = utf8
    dos charset = cp936

	log file = /var/log/samba/%m.log
	max log size = 50

	security = share
	passdb backend = tdbsam
	load printers = yes
	cups options = raw
[temp]
    comment     = Temporary file space
    path        = /tmp
    read only   = no
    public       = yes
[homes]
	comment = Home Directories
	browseable = no
	writable = yes
[printers]
	comment = All Printers
	path = /var/spool/samba
	browseable = no
	guest ok = no
	writable = no
	printable = yes

###################################################################################
