
First: Download and Install Git
....

NEXT:Set Up SSH Keys //建立ssh安全连接

    1.打开git-bash,Check for SSH Keys
        cd ~/.ssh
    
    2.Backup and remove existing SSH keys. //备份与移除 (非必要,存在时才备份)
        $ ls

        $ mkdir key_backup
        $ cp id_rsa* key_backup
        $ rm id_rsa*
    
    3.Generate a new SSH key.

        $ ssh-keygen -t rsa -C "your_email@youremail.com"
    
    4.Add your SSH key to GitHub.

        On the GitHub site Click “Account Settings” > Click “SSH Public Keys” > Click “Add another public key”
        //添加产生的公钥id_rsa.pub
    
    5.Test everything out. //测试
        ssh git@github.com

Then:Set Up Your Info

    1.set you username and email
        //username是真实名字,不是账号名称
        Git tracks who makes each commit by checking the user’s name and email. In addition, we use this info to associate your commits with your GitHub account. To set these, enter the code below, replacing the name and email with your own. The name should be your actual name, not your GitHub username.
        
        // -- global 用户级别
        $ git config --global user.name "Firstname Lastname"
        $ git config --global user.email "your_email@youremail.com"
    
    2.有些工具是不使用ssh连接的,这种需要配置API Token

        On the GitHub site Click “Account Settings” > Click “Account Admin.”

        $ git config --global github.user username
        $ git config --global github.token 0123456789yourf0123456789token
        
        
