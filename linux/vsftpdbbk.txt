##################################################
# vsftpd服务的安装

rpm -qa | grep vsftpd
rpm -ivh vsftpd-2.0.5
rpm -ivh db4-utils

##################
Vsftpd的三种用户形式

    1.匿名用户形式:默认安装的情况下, 系统只提供匿名用户访问

    2.本地用户形式:以/etc/passwd中的用户名为认证方式

    3.虚拟用户形式:支持将用户名和口令保存在数据库文件或数据库服务器中。相对于FTP的本地用户形式来说,虚拟用户只是FTP服务器的专有用户,虚拟用户只能访问FTP服务器所提供的资源,这大增强系统本身的安全性。相对于匿名用户而言,虚拟用户需要用户名和密码才能获取FTP服务器的文件,增加了对用户和下载的可管理性。

    对于需要提供下载服务,但又不然望所有人都可以匿名下载;既需要对下载用户进行管理,又考虑到主机安全和管理方便的FTP站点来说，虚拟用户是一种极好的解决方案

#############################
# 匿名用户设置

    anonymous_enable=YES #是否启用匿名用户
    no_anon_password=YES #匿名用户login时不询问口令

    下面这四个主要语句控制这文件和文件夹的上传、下载、创建和重命名

        anon_upload_enable=(yes/no) #控制匿名用户对文件(非目录)上传权限
        anon_world_readable_only=(yes/no) #控制匿名用户对文件的下载权限
        anon_mkdir_write_enable=(yes/no) #控制匿名用户对文件夹的创建权限
        anon_other_write_enable=(yes/no) #控制匿名用户对文件和文件夹的删除和重命名

    注:匿名用户下载是使用的是nobody这个用户,所以相应的o这个位置要有R权限才能被下载。
    若想让匿名用户能上传和删除权限,必需设置

    write_enable=YES #全局设置,是否容许写入(无论是匿名用户还是本地用户,若要启用上传权限的话,就要开启他)

    anon_root=(none) #匿名用户主目录
    anon_max_rate=(0) #匿名用户速度限制
    anon_umask=(077) #匿名用户上传文件时有掩码(若想让匿名用户上传的文件能直接被匿名下载, 就这设置这里为073)

    chown_uploads=YES #所有匿名上传的文件的所属用户将会被更改为chown_username
    #chown_username=whoever#匿名上传文件所属用户名

vim /etc/vsftpd/vsftpd.conf

# 匿名用户名
    anonymous ftp
touch /var/ftp/test1

# 重新启动vsftpd
killall -9 vsftpd
service vsftpd start

# 默认在/var/ftp目录增加写权限也写不了

# chown ftp /var/ftp/pub 匿名可上传


###################################################
# 本地用户设置

write_enable=YES #可以上传(全局控制)删除,重命名
local_umask=022 #本地用户上传文件的umask
userlist_enable=YES # 限制了这里的用户不能访问
local_root # 设置一个本地用户登录后进入到的目录
user_config_dir #设置用户的单独配置文件,用哪个账号登录就用哪个账户命名
download_enable #限制用户的下载权限

chroot_list_enable=YES #
如果启动这项功能,则所有列在chroot_list_file之中的使用者不能更改根目录
chroot_list_file=/etc/vsftpd/chroot_list #指定限制的用户文件

user_config_dir=    # 后面跟存放配置文件的目录。用来实现不同用户不同权限。
                    # 在vsftpd.conf文件中加入这一句
                    # 在相应的目录里面,为每个用户创建自己的配置文件,用来实现不同的权限


################################################################
# 虚拟用户的设置

虚拟用户使用PAM认证方式
    pam_service_name=vsftpd # 设置PAM使用的名称,默认值为/etc/pam.d/vsftpd
    guest_enable=YES/NO # 启动虚拟用户。默认值为NO
    guest_username = ftp # 这里用来映射虚拟用户。默认值为ftp

    virtual_use_local_privs=YES/NO

    # 当该参数激活(YES)时,虚拟用户使用与本地用户相同的权限
    # 当该参数关闭(NO)时，虚拟用户使用与匿名用户相同的权限。
    # 默认情况下此参数是关闭的(NO) 

####
# vi /etc/vsftpd/vsftpd.conf
    guest_enable=YES
    guest_username=vtest

# 建立虚拟用户
vim /etc/vsftpd/vtest
    vftp
    123

# at /etc/vsftpd
# 生成数据库文件
db_load -T -t hash -f vuser vuser.db
chmod 600 vuser.db

vim /etc/pam.d/vsftpd # 全部注释掉
    # 加上两行
    auth          required     pam_userdb.do db=/etc/vsftpd/vuser
    account       required     pam_userdb.do db=/etc/vsftpd/vuser

rm vuser
useradd -s /sbin/nologin vtest

# 让虚拟用户可读目录
chmod 704 /home/vtest/

########################################################################
# 实例部分

[案例1]建立基于虚拟用户的FTP服务器,并根据以下要求配置FTP服务器
(1)配置FTP匿名用户的主目录/var/ftp/anon,下载带宽限制为100kB/s
(2)建立一个名为abc,口令为xyz的FTP账户。下载带宽限为500kb/s
(3)设置FTP服务器同时登录到FTP服务器的最大链接数为100;每个IP最大链接数为3;用户空闲时间超过限值为5分钟
