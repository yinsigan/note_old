#!/usr/bin/perl
print "This is my first perl program\n";
$a=<>;
print $a;

	数值变量
		整数
			12
			12e2(1200) e不分大小写
			-12
			1_200_000
			012 八进制,值为10
			0x1f 十六进制数,值为31 f不分大小写
			
		浮点数
			11.4
			-0.3
			.3
			3.
			5.4e2 e或E，+或- 1到3位数字
			
		整数的限制
		15位有效数字，16位后截断
		浮点数的精度
		指数范围-309-308 太小显示为0，太大显示为1.#INF
		
	字符串变量
		一般由双引号或单引号标识的一组字符组成
		最少0个字符""为空串，最多可以占满内存，末尾不含null('\0')
		""双引号
		变量替换
			$var="str";
			print "this is $var";->"this is str"
			
			最长匹配
				$str,$string都存在时，匹配$string.
				要匹配$str时，用${str}ing。
				
#!/usr/bin/perl
print "bell ring:\a\n";
print "back#\bspace\n";
print "copy\rabc\n";
print "abc\tdef\n";

print "the \$var\n";
print "a quote \" in string\n";
print "a quote \\ in string\n";
print "\045";
print "\x25";

	\的作用
	转义字符
	取消字符含义
		取消$的变量标识 print "the \$var is $var\n"
		取消"	print "A quote \" in a string\n";
		取消\	print "A quote \\ in a string\n";
		\nnn 十进制表示的ASCII码 print "\045" %
		\xnn 十六制表示的ASCII码 print "\x25" %
		
	'单引号
		不进行变量替换 print "this is $var'
		不进行转义 print 'this is $var\n'
		字符串可以跨行 print 'this is first line
											this is second line'
		
		\的作用
			字符串中含有单引号时'A quote \' in a string'
			字符串中含有\时 'A quote \\ in a string'
			
	字符串的特殊表示
		qq(string in qq):相当于"string in qq"
		q(string in q):相当于'string in q'
		()可换为<>,{},[]等配对字符qq{string in qq};
		字符串中也可出现qq[string [in] qq];
		一般换为其他字符 qq[string <in> qq];
		也可换为任意//,;;,等相同字符对qq/string in qq /
		
#!/usr/bin/perl
$var=1;
print q(the $var\n);
print qq(is $var\n);
print qq<string "str" (var) in qq>;

	变量初值
		未创建时状态为undef,到达文件尾也为undef
		说明变量为未定义：undef $a;
		用在条件判断中:if (undef $a)
		代替不关心的变量
			$s="a:b:c:d";($a1,undef,undef,$d1)=split(/:/,$s);
		如果有undef变量又不知在哪，可加-w参数进行提示
		#!/usr/bin/perl -w
		创建后状态为defined 一般用在条件判断中 if(defined $a)
		整数初值为0，字符串初值为空串""。一般未赋值就使用时
			$result=$undefined +2;
			
相关函数
	length()串长度，数字位数
	三角函数sin,数学函数sqrt,随机产生函数rand,srand
	Uc lc,ucfirst,lcfirst大小写
	Substr,index,pos
	转换函数ord,chr,pack,unpack
	
	$lastchar=chop(str)截去最后一个字符，无论是什么字符
	$result=chomp(str)截去末尾的行分隔符(\n),行分隔符可由$/字义
	
#!/usr/bin/perl
$a="abcd";
chop($a);
print "chop $a\n";
$a="abcd";
chomp($a);
print "chmop $a\n";

$a="abc\n";
chop($a);
print "chop $a|";
$a="abc\n";
chomp($a);
print "chomp $a|";

$a="ab\n\n\n";
$/="";
chomp($a);
print "chomp many $a\n";

$/="cd";
$a="abcd";
chop($a);
print "chop $a|";
$a="abcd";
chomp($a);
print "chomp $a|";

