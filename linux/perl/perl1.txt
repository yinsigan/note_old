﻿A Simple Program
	#!/usr/bin/perl
	print "Hello,world!\n";
	
	其他方式
	#!/usr/bin/perl
	use 5.010;#which Perl thinks is 5.100,a version we definitely don't have yet!
	say "Hello World!";

关于注释	
	In perl,comments run from a pound sign(#) to the end of line.
	There are no "block comments" in perl
关于!!/usr/bin/perl
	This #! line is actually the least portable part of a Perl program because you'll need to find out what goes there for each machine.Fortunately,it's almost always either /usr/bin/perl or /usr/local/bin/perl.If that's not it,you'll have to find where you system is hiding perl,then use that part.On Unix systems,you might use a shebang line that finds perl for you:

标量数据
	Floating-Point Literals:1.25 255.000 255.0 7.25e45 -6.5e24 -12e-24 -1.2E23
	Integer Literals:0 2001 -40 255 61298040283768
	Nondecimal Integer Literals 0377 0xff 0b11111111 
	Numeric Operators:+ - * / ** %
	Strings:Typical strings are printable sequences of letters and digits and punctuation in the ASCII 32 to ASCII 126 range.
	
	Single-Quoted String Literals 'fred' '' 'Dont\'t let an apostrophe end this string prematurely!' 'hello\n'
		'hello
		there'
		'\'\\'
		
		Note that the \n within a single-quoted string is not interpreted as a newline,but as the two characters backslash and n.Only when the backslash is followed by another backslash or a single quote does it have special meaning.
		
		双引号内的"\"只转义少量字符,例如"'","\",而不转义"\n",还可以用多行数据
		
	Double-Quoted String Literals
		"barney" "hello world\n" "The last character of this string is a quote mark: \"" "coke\tsprite"
	
		Double-quoted string backslash escapes
			Construct Meaning
			\n			Newline
			\r			Return
			\t			Tab
			\f			Form-feed
			\b			Backspace
			\a			Bell
			\e			Escape(ASCII escape)
			\007		Any octal ASCII value(here,007=bell)
			\x7f		Any hex ASCII value(here,7f = delete)
			\cC			A "control" character(here,Control-C)
			\\			Backslash
			\"			Double quote
			\l			Lowercase next letter
			\L			Lowercase all following letters until \E
			\u			Uppercase next letter
			\U			Uppercase all following letters until \E
			\Q			Quote nonword characters by adding a backslash until \E
			\E			End\L,\U,or\Q
	
			变量内插
			Another feature of double-quoted string is that they are variable interpolated,meaning that some variable names within the string are replaced with their current values when the strings are used.You haven't formally been introduced to what a variable looks like yet,so we'll get back to that later in this chapter.
		
		字符串操作符
			"hello"."world"
			"hello".' '."world"
			'hello world'."\n"
			
			"fred"x3 #is "fredfredfred"
			5x4 #is really "5" x 4,which is "5555"
		
		Automatic Conversion Between Numbers and Strings
			For the most part,Perl automatically converts between numbers and strings as needed.How does it know which is needed?It all depends upon the operator being used on the scalar value.If an operator expects a number(like + does),Perl will see the value as a number.If an operator expects a string(like .does),Perl will see the value as a string.
	
			"z".5*7 #same as "z".35
			"12"*"3" #same as 36
			"12fred34"*"3" #same as 36
	
	输出警告信息
		$ perl -w my_program
		#!/usr/bin/perl -w
		#!perl -w
		
		#!/usr/bin/perl
		use warnings; #!!!Perl 5.6 and later,
		
		#!/usr/bin/perl
		use diagnostics;
		
		$ perl -Mdiagnostics ./my_program
	
	
	
	Scalar Variables
		$A $a
			Upper and lowercase letters are distinct:the variable $Fred is a different variable from $fred.
	
	Binary Assignment Operators
		+= -= **= .=
	
	print "The answer is ",6*7,".\n";
	
字符串中的标量变量内插
	$what = "brontosaurus steak";
	print "fred ate $n $whats.\n";
	print "fred ate $n ${what}s.\n";
	print "fred ate $n $what"."s.\n";
	print 'fred ate '.$n.' '.$what."s.\n";
	
	print "$fred"; #!!!stupid
	
	打印#
	$fred = 'hello';
	print "The name is \$fred.\n"; # prints a dollar sign
	print 'The name is $fred' . "\n"; # so does this

Operator Precedence and Associativity
	4 ** 3 ** 2 # 4 ** (3 ** 2), or 4 ** 9 (right associative)
	72 / 12 / 3 # (72 / 12) / 3, or 6/3, or 2 (left associative)
	36 / 6 * 3 # (36/6)*3, or 18
	
Comparison Operators
	
	Comparison Numeric String
	Equal == eq
	Not equal != ne
	Less than < lt
	Greater than > gt
	Less than or equal to <= le
	Greater than or equal to >= ge

关于布尔值
	Boolean data type, like some languages have. Instead, it uses a few simple
		rules:
		• If the value is a number, 0 means false; all other numbers mean true.
		• Otherwise, if the value is a string, the empty string ('') means false; all other strings
		mean true.
		• Otherwise (that is, if the value is another kind of scalar than a number or a string),
		convert it to a number or a string and try again.
	没有布尔类型
	
	There’s one trick hidden in those rules. Because the string '0' is the exact same scalar
	value as the number 0, Perl has to treat them both the same. That means that the string
	'0' is the only nonempty string that is false.
	If you need to get the opposite of any Boolean value, use the unary not operator, !. If
	what follows it is a true value, it returns false; if what follows is false, it returns true:
	if (! $is_bigger) {
	# Do something when $is_bigger is not true
	}

Getting User Input
	<STDIN>
	#!/usr/bin/perl
	$line = <STDIN>;
	if ($line eq "\n") {
		print "That was just a blank line!\n";
		print $line;
	}else {
		print "That line of input was: $line";
	}

	#!!!The string value of <STDIN> typically has a newline character on the end of it
	
	<STDIN>末尾都有"/n"
	
The chomp Operator
	$text = "a line of text\n"; # Or the same thing from <STDIN>
	chomp($text); # Gets rid of the newline character
	
	chomp移除字符串变量最后一个"\n"，若没有"\n"则返回0
	$food = <STDIN>;
	$betty = chomp $food; # gets the value 1 - but you knew that!
	使用chomp时可以省略括号
	

defined function, which returns false for undef, and true for everything else
	$madonna = <STDIN>;
	if ( defined($madonna) ) {
		print "The input was $madonna";
	} else {
		print "No input available\n";
	}
	
	当按下end of file键时才返回No input available









