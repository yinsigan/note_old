Array and List
    A list is an ordered collection of scalars.An array is a variable that contains a list.
   
Accessing Elements of an Array
    $fred[0] = "yabba";
    $fred[1] = "dabba";
    $fred[2] = "doo";
       
    The array name itself(is this case,"fred") is from a completely separate namespace that scalars use;you could have a scalar variable named $fred in the same program,and Perl will treat them as different things and wouldn't be confused.
   
    同名数组和变量占用不同的空间
Special Array Indices
    $rocks[0] = 'bedrock';
    $rocks[1] = 'slate';
    $rocks[99] = 'schist'; // now there are 97 undef elements
   
    $#rocks 输出rocks数组最后的索引值
    negative array indices count from the end of the array

List Literals
    An array(the way your represent a list value within your program) is list of comma separated values enclosed in parentheses.These values form the elements of the list.
   
    (1,2,3) #list of three values 1,2,and 3
    (1,2,3,) #the same three values(the trailing comma is ignored)
    () #two values,"fred" and 4.5
    () #empty list -zero elements
    (1..100) #list of 100 integers
    (1..5) #same as(1,2,3,4,4)
    (1.7..5.7) #same thing - both values are truncated
    (0,2..6,10,12) #same as(0,2,3,4,5,6,10,12)
    (5..1) #empty list -.. onlycounts "uphill"
    ($m..$n) #range determined by current values of $m and $n
    (0..$#rocks) #the indices of the rocks array from the previous section
   
The qw Shortcut(quoted words)
    The qw shorcut makes it easy to generate them without typing a lot of extra quote marks
   
    qw( fred barney betty wilma dino )
   
    The whitespace(characters like spaces,tabs,and newlines) will be discarded,and whatever is left becomes the list of items.
   
    qw(fred
        barney    betty
    wilma dino) #same as above,but pretty strange whitespace
   
    qw! fred barney betty wilma dino !
    qw/ fred barney betty wilma dino /
    qw# fred barney betty wilma dino #
    qw( fred barney betty wilma dino ) # like in a comment!
    qw{ fred barney betty wilma dino }
    qw[ fred barney betty wilma dino ]
    qw< fred barney betty wilma dino >
   
    If you need to include the closing delimiter within the string as one of the characters,ou probably picked the wrong delimiter.But even if you can't or don't want to change the delimiter,you can still include the cahracter using the backslash
   
        qw! yahoo\! google ask msn ! #include yahho as an element
       
    qw{
        /usr/dict/words
        /home/rootbeer/.ispell_english
    }
   
List Assignment
   
    ($fred,$barney,$dino) = ("flintstone","rubble",undef);
   
    All three variables in the list on the left get new values,just as if you did three separate assignments.Since the list is built up before the assignment starts,this makes it easy to swap two variables'values in Perl
   
    ($fred,$barney) = ($barney,$fred); #swap those values
    ($betty[0],$betty[1]) = ($betty[1],$betty[0]);
   
    列表在分配值时就开始建立了,所以可以交换在列表中两个变量的值
   
    ($fred,$barney) = qw< flintstone rubble slate granite > #two ignored items
    ($wilma,$dino) = qw[flintstone]; #$dino gets undef
   
    Now that you can assign lists,you could build up an array of strings with a line of code like this:
        ($rocks[0],$rocks[1],$rocks[2],$rocks[3]) = qw/talc mica feldspar quartz/;
   
    But when you wish to refer to an entire array,Perl has a simpler notation.Just use the at sign(@) before the name of the array(and not index brackets after it) to refer to the entire array at once.
        @rocks = qw/ bedrock slate lava /;
        @tiny = (); #the empty list
        @giant = 1..1e5; #a list with 100,100 elements
        @stuff = (@giant,undef,@giant); #a list with 200,001 elements
        $dino = "granite";
        @quarry = (@rocks,"crushed rock",@tiny,$dino);
       
        列表中不能包含数组,只会包含数值的内容
        @copy = @quarry;#copy a list from one array to another

The pop and push Operators
   
    The pop operator takes the last element off of an array and return it
        @array = 5..9;
        @fred = pop(@array); #$fred gets 9,@array now has (5,6,7,8)
        @barney = pop @array; #$barney gets 8,@array now has (5,6,7)
        pop @array; # @array now has (5,6).(The 7 is discarded.)

    If the array is empty, pop will leave it alone (since there is no element to remove), and
it will return undef.
       
    The converse operation is push,which adds an element(or a list of element) to the end of an array:
        push(@array,0); #@array now has (5,6,0)
        push @array,8; #@array now has (5,6,0,8)
        push @array,1..10; #@array now has those ten new elements
        @others = qw/ 9 0 2 1 0/;
        push @array,@others; #@array now has those five new elements(19 total)
    Note that the first argument ot push or the only argument for pop must be an array variable--pushing and popping would not make sense on a literal list.

The shift and unshift Operators
   
    @array = wq# dino fred barney #;
    $m = shift(@array); # $m gets "dino",@array now has ("fred","barney")
    $n = shift(@array); # $n gets "fred",@array now has ("barney")
    shift @array; # @array is now empty
    $o = shift @array; # $o gets undef,@array is still empty
    unshift(@array,5); # @array now has the one-element list (5)
    unshift @array,4; # @array now has (4,5)
    @others = 1..3;
    unshift @array,@others; # @array now has (1,2,3,4,5)
   
    Analogous to pop,shift returns undef if given an empty array variable.
   
Interpolating Arrays into Strings

    Like scalars,array values may be interpolated into a double-quoted string.Elements of an array are automatically separated by spaces upon interpolation
        @rocks = qw{ flintstone slate rubble};
        print "quartz @rocks limestone\n"; # prints five rocks separated by spaces
       
        email address
       
        $email = "fred\@bedrock.edu"; #Correct
        $email = "fred@bedrock.edu";
       
        @fred = qw(hello dolly);
        $y = 2;
        $x = "This is $fred[1]'s place"; # "This is dolly's place"
        $x = "This is $fred[$y-1]'s palce"; # same thing
       
            if $y contains the string "2*4",we're still talking about element 1,not element 7,because "2*4" as number(the value of $y used in a numeric expression) is just plain 2.

        @fred = qw(eating rocks is wrong);
        $fred = "right"; # we are trying to say "this is right[3]"
        print "this is $fred[3]\n"; # prints "wrong" using $fred[3]
        print "this is ${fred}[3]\n"; # prints "right" (protected by braces)
        print "this is $fred"."[3]\n"; # right again (different string)
        print "this is $fred\[3]\n";  # right again (backslash hides it)

The foreach Control Structure
   
    foreach $rock (qw/bedrock slate java /) {
        print "One rock is $rock.\n"; # Prints names of three rocks
    }

    The control variable is not a copy of the list element--it actually is the list element.
   
    @rocks = qw/ bedrock slate java /;
    foreach $rock (@rocks) {
         $rock = "\t$rock";
         $rock .= "\n";
    }
    print "The rocks are:\n",@rocks;

    至于control variable $rock 它原先是什么值loop之后是什么值
   
Perl's Favorite Default: $_
    foreach (1..10) { #Uses $_ by default
        print "I can count to $_!\n";
    }
    $_ = "Yabba dabba doo\n";
    print; # prints $_ by default

The reverse Operator

    @fred = 6..10;
    @barney = reverse(@fred); # gets 10.9.8.7.6
    @wilma = reverse 6..10; # gets the same thing,without the other array
    @fred = reverse @fred; # puts the result back into the original array
   
    Perl always calculates the value being assigned(on the right)before it begins the actual assignment.Remember that reverse returns the reversed list;it doesn't affect its arguments.If the return value isn't assigned anywhere,it's useless:
        reverse @fred; #WRONG -- doesnt change @fred
        @fred = reverse @fred; # that's better
       
The sort Operator
   
    @rocks = qw/bedrock slate ruble granite /;
    @sorted = sort(@rocks);
    foreach (@sorted) {
        printf $_."\n";
    }
    @back = reverse sort @rocks;
    foreach (@back) {
        print $_."\n";
    }
    @rocks = sort @rocks;
    foreach (@rocks) {
        print $_."\n";
    }
    @numbers = sort 97..102;
    foreach (@numbers) {
        printf $_."\n";
    }
   
    sort默认按字符串来排序,所有以1开头的数字会排在以9开头的数字前面

标量和列表上下文
   
    42 + something # The something must be a scalar
    sort something # The something must be a list
   
    @people = qw( fred barney betty );
    @sorted = sort @people; # list context: barney,betty,fred
    $number = 42 + @people; # scalar context: 42 + 3 give 45
   
    @list = @people; # a list of three people
    $n = @people; # the nubmer 3
   
Using List-Producing Expressions in Scalar Context
   
    @backwards = reverse qw/ yabba dabba doo /;
        # gives doo,dabba,yabba
    $backwards = reverse qw/ yabba dabba doo /;
        # gives oodbabadabbay
    如果是sort 则返回undef
   
    Here are some common contexts to start you off
        $fred = something;                # scalar context
        @pebbles = something;            # list context
        ($wilma,$betty) = something     # list context
        ($dino) = something             # still list context!
    标量上下文
        $fred = something;
        $fred[3] = something;
        123 + something
        something + 654
        if (something) { ... }
        while (something) { ... }
        $fred[something] = something;
    列表上下文
        @fred = something;
        ($fred,$barney) = something;
        ($fred) = something;
        push $fred,something;
        foreach $fred (something) { ... }
        sort something
        reverse something
        print something
       
Using Scalar-Producing Expressions List Context
    Going this direction is straightforward:if an expression doesn't normally have a list value,the scalar value is automatically promoted to make a one-element list
        @fred = 6* 7; # gets the one-element list (42)
        @barney = "hello".' '."world";
    Well,there's one opossible catch:
        @wilma = undef;# OOPS! Gets the one-element list (undef)
            # which is not the same as this:
        @betty = (); # A correct way to empty an array

<STDIN> in list Context
    @lines = <STDIN>; # read standard input in list context
   
    @lines = <STDIN>; # read all the lines
    chomp(@lines); # discard all the newline characters
   
    chomp(@lines = <STDIN>); 