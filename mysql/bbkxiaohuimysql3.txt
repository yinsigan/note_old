﻿获得mysql帮助

	help contents;
	
	自动补全配置
	[mysql]
	#no-auto-rehash
	auto-rehash
	
mysql物理结构

	MySQL Database
		日志文件
		数据文件
		replication相关文件
		其他小文件
	MySQL日志系统
		error log
		binary log
		query log
		slow query log
		innodb redo log
	error log
		The error log file contains information indication when mysqld was started and stopped and also any critical errors that occur while the server is running.If myslqd notices a table that needs to be automatically checked or repaired,it writes a message to the error log
		
		You can specify where mysqld stores the error log file with the --log-error[=file_name] option.If no file_name value is given,mysqld uses the name host_name.err and writes the file in the data directory
		
		config variable "log_error" in my.conf
		
		how to change the place of error log?
		
	binary log
		The binary log contains all statements that update data or protentially could have updated it(for example,a DELETE which matched no rows).Statements are stored in the form of "events" that describe the modifications.The binary log also contains information about how long each statements took that updated data.
		
		The binary log does not contain statements that do not modify any data.If you want to log all statements(for example,to identify a problem query),use the general query log
		
		The primary purpose of the binary log is to be able to update databases during a restore operation as fully as possible,becase the binary log contains all updated done after a backup was made.The binary log is also used on master replication servers as a record of the statements to be sent go slave servers
		
		Running the server with the binary log enabled makes performance about 1% slower.However,the benefits of the binary log for restore operations and in allowing you to set up replication generally outweight this minor performance decrement.
		
		binary log相关变量和参数
		
		命令行参数
			--log-bin[=file_name]
			--log-bin-index[=file]
			--max_binlog_size
			--binlog-do-db=db_name
			--binlgo-ignore-db=db_name
			
		系统变量
			log_bin
			binlog_cache_size
			max_binlog_cache_size
			max_binlog_size
			binlog_cache_size
			binlog_cache_disk_use
			binlog_do_db
			binlog_ignore_db
			sync_binlog

#########################################补充

        二进制日志：binary log & binary log index

        二进制日志，也就是我们常说的binlog，也是mysql server 中最为重要的日志之一。
        当我们通过“--log-bin[=file_name]”打开了记录的功能之后，mysql 会将所有修改数据
        库数据的query 以二进制形式记录到日志文件中。当然，日志中并不仅限于query 语句这么
        简单，还包括每一条query 所执行的时间，所消耗的资源，以及相关的事务信息，所以binlog
        是事务安全的。

        和错误日志一样，binlog 记录功能同样需要“--log-bin[=file_name]”参数的显式指
        定才能开启，如果未指定file_name，则会在数据目录下记录为mysql-bin.******（*代表0～
        9 之间的某一个数字，来表示该日志的序号）。

        binlog 还有其他一些附加选项参数：

        “--max_binlog_size”设置binlog 的最大存储上限，当日志达到该上限时，mysql 会
        重新创建一个日志开始继续记录。不过偶尔也有超出该设置的binlog 产生，一般都是因为
        在即将达到上限时，产生了一个较大的事务，为了保证事务安全，mysql 不会将同一个事务
        分开记录到两个binlog 中。

        “--binlog-do-db=db_name”参数明确告诉mysql，需要对某个（db_name）数据库记
        录binlog，如果有了“--binlog-do-db=db_name”参数的显式指定，mysql 会忽略针对其他
        数据库执行的query，而仅仅记录针对指定数据库执行的query。

        “--binlog-ignore-db=db_name”与“--binlog-do-db=db_name”完全相反，它显式指
        定忽略某个（db_name）数据库的binlog 记录，当指定了这个参数之后，mysql 会记录指定
        数据库以外所有的数据库的binlog。
        
        “--binlog-ignore-db=db_name”与“--binlog-do-db=db_name”两个参数有一个共同
        的概念需要大家理解清楚，参数中的db_name 不是指query 语句更新的数据所在的数据库，
        而是执行query 的时候当前所处的数据库。不论更新哪个数据库的数据，MySQL 仅仅比较当
        前连接所处的数据库（通过use db_name 切换后所在的数据库）与参数设置的数据库名，而
        不会分析query 语句所更新数据所在的数据库。
        
        mysql-bin.index 文件（binary log index）的功能是记录所有Binary Log 的绝对路
        径，保证MySQL 各种线程能够顺利的根据它找到所有需要的Binary Log 文件。
                
##################################################################

		./bin/mysqld_safe --user=mysql --log-bin=/tmp/1.000000001 --log-bin-index=/tmp/logbin.index --max-binlog-size=10M --binlog-do-db=test &
		
		mysql>show variables like 'log_bin%';
		mysql>show binary logs;
		mysql>show master logs;
		mysql>flush logs;
		mysql>reset master;
		mysql>purge binary logs to '1.000003';
		mysql>show variables like '%expire%'
		mysql>set global expire_logs_days=5;
		
		binary log相关命令
		
			binlog的格式
			binlog_format:STATEMENT,ROW,mixed
			>SET SESSION binlog_format='STATEMENT';
			>SET SESSION binlog_format='ROW';
			>SET SESSION binlog_format='MIXED';
			>SET GLOBAL binlog_format='STATEMENT';
			>SET GLOBAL binlog_format='ROW'
			>SET GLOBAL binlog_format='MIXED';
		
			mysql>show varibales like '%format%'
            mysql>set global binlog_format = ROW;
            mysql>show global variables like '%format%';
			
		binary log工具
			mysqlbinlog
			
			The binary log files that the server generates are written in binary format.To examine these files in text format,use the mysqlbinlog utility.You can also use mysqlbinlog to read relay log files written by a slave server in a replication setup.Relay logs have the same format as binary log files.
			
			The output from mysqlbinlog can be re-executed(for example,by using it as input to mysql) to reapply the statements in the log.This is useful for recovery operations after a server crash.For other usage examples,see the discussion later in this section.
			
			shell>mysqlbinlog binlog.0000003
			
