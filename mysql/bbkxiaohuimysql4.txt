﻿mysql>show variables like '%binlog_cache%'每个session
mysql>show status like '%bilog_cache_use%';
mysql>shwo table status like 't' \G;
mysql>select @@autocommit;
###########################################################
几次一提交
mysql> truncate table t;
Query OK, 0 rows affected (0.09 sec)

mysql> call test();
Query OK, 0 rows affected (44.19 sec)

mysql> set global sync_binlog = 5;
Query OK, 0 rows affected (0.00 sec)

mysql> truncate table t;
Query OK, 0 rows affected (0.01 sec)

mysql> call test();
Query OK, 0 rows affected (34.49 sec)
###########################################################


query log

	The general query log is a general record of what mysqld is doing.The server writes information to this log when clients connect or disconnect,and it logs each SQL statement received from clients.The general query log can be very useful when you suspect an error in a client and want to know exactly what the client sent to mysqld.
	
	mysqld writes statements to the query log in the order that it receives them.This may be different from the order in which they are executed.This is contrast to the binary log,for which statements are written after they are executed,but before any locks are released.(Also,the query log contains all statements,whereas the binary log does not contain statements that only select data)
	
	To enable the general query log as of MySQL 5.1.6,start mysqld with the --log option,and optionaly use --log-output to specify the log output destination as described in Section5.11.1,"Server Log Tables".Before 5.1.6,enable the general query log file with the --log[=file_name] or -l [file_name] option.If no file_name value is given,the default name is host_name.log in the data directory.
	
	Server restarts and log flushing do not cause a new general query log file to be generated(although flushing closes and reopens it).On Unix,you can rename the file and create a new one by using the following commands:
	
#######################################################
    切换日志
	shell>mv host_name.log host_name-old.log
	shell>mysqladmin flush-logs
	shell>cp host_name-old.log backup-direcotry
	shell>rm host_name-old.log
	shell>mv host_name.log host_name-old.log
#######################################################

	实验部分
	1. ./bin/mysqld_safe --user=mysql --log=/tmp/mysqllog.log &
	2. mysql>show variables like 'log';
	3. mysql>show variables like 'gener%';
	4. mysql>set global log=1;
	5. tail -f /opt/mysql5152/data/oracleasm.log

	[mysqld]
		general_log=1
		generl_log_file=/tmp/mysqlgen.log
		
	shell>mysqld_safe --user=mysql --general_log=1 --general_log_file=/tmp/mysqlgen.log &
	
slow query log
	
	The slow query log consists of all SQL statements that took more than long_query_time seconds to execute.The time to acquire the initial table locks is not counted as execution time,The minimum and default values of long_query are 1 and 10,respectively
	
	To enable the slow query long as of MySQL 5.1.6 start mysqld with the --log-slow-queries option
	
	slow query long相关变量和参数
		命令行参数
			--log-slow-queries
		系统变量
			log_slow_queries
			slow_query_log
			slow_query_log_file
			long_query_time
			log_queries_not_using_indexes

			[mysqld]
			log_slow_queries=/tmp/mysqlslow.log (is deprecated)
			#slow_query_log=1
			#slow_query_log_file=/tmp/mysqlslow.log
			
			mysql>show variables like '%long%'

#######################################################
create table t as select * from information_schema.tables;
insert into t select * from t;
#######################################################
			mysql>set global log_queries_not_using_indexes=1
#######################################################
show index from t;
#######################################################
			mysql>show index from t;
			shell>mysqldumpslow
			shell>mysqldumpslow -s t -t 2 /tmp/mysqlslow.log

innodb redo log
	用来实现灾难恢复(crash recovery),突然断电会导致Innodb表空间(Table space)中的数据没有被更新到磁盘上,通过重新执行redo log能够重新执行这些操作来恢复数据
	
	提升InnoDB中I/O性能,Innodb引擎把数据和索引都载入到内存中的缓存池(buffer pool)中,如果每次修改数据和索引都需要更新到磁盘,必定会大大增加I/O请求,而且因为每次更新的位置都是随机的,磁头需要频繁定位导致效率低.所以Innodb每处理完一个请求(Transaction)后只添加一条日志log,另外有一个线程负责智能地读取日志文件并批量更新到磁盘上,实现最高效的磁盘写入。

	系统变量
	innodb_log_buffer_size #日志缓冲区大小
	innodb_log_file_size #日志文件大小 默认放在data目录下
	innodb_log_files_in_group # 一个组有多少个文件
	innodb_log_group_home_dir # ./ 相对于data来说的
	innodb_flush_log_at_trx_commit
	innodb_os_log_written
	innodb_os_log_fsyncs

#######################################################
#移除日志文件后再生成
#######################################################
	
	innodb_flush_log_at_trx_commit
		0:日志缓冲每秒一次地被写到日志文件,并且对日志文件做到磁盘操作的刷新,但是在一个事务提交不做任何操作。
		1:在每个事务提交时,日志缓冲被写到日志文件,对日志文件做到磁盘操作的刷新。
		2:在每个提交,日志缓冲被写到文件,但不对日志文件做到磁盘操作的刷新。对日志文件每秒刷新一次。
			
