网站重构标准1

块元素有:ul li div
内联元素有:a span


---------盒模型-------------

//盒子模型的属性
div{
	width:200px; /50%
	height:200px; //width 和 height不包括padding 和 borderd-width
	border:10px solid #000;
	margin:20px;
	padding:20px;
}

--------字体--------------------

//字体样式
font-family
font-size

color:rgb(255,0,0); //red
//是否加粗
font-weight:
//可对a标签去除下划线
font-decoration:none
//设置文本大小写
text-transform

//设置行高
line-height:100%

letter-spacing
word-spacing

//对齐属性
test-align:justify

//设置缩进
test-indent

tips://如果ie显示的字体有点大,就设置font-size

*{
    margin:0;
    padding:0;
}

------------CSS盒模型的显示模式和背景--------------------

display:block
display:inline;
display:none; //隐藏内容,去除位置

background-color:transparent;//背景色设为透明
background-image:
background-repeat
background-position:top left;//左上角 center top //中上 //top right //center left //center center
background-position:100px 20px; //左 上
background-position:20% 50px; //左 上
background-attachment:scroll; //背景图像是随对象内容滚动
background-attachment:fixed;

---------------------布局方式----------------------

//固定布局
#layout{
	width:500px;
	height:300px;
	background:#ccc;
    border:1px solid #000;
}

//自动适应布局
#layout{
	width:500px;
	height:300px;
	background:#ccc;
    border:1px solid #000;
}

tips://在设定test-align属性后如果firefox没有盒模型居中而只是文字居中,就加上margin:0 auto;自动适应

//两列宽度自动适应
#left,#right{height:300px;}
#left{width:500px;background:#666;float:left}
#right{width:100px;background:#ccc;float:left}

//tips:加了border属性(加了几个像素)后会把下面的层撑下来,所以要把80%改为78%
#left,#right{height:300px;border:1px solid #000}
#left{width:20%;background:#666;float:left}
#right{width:78%;background:#ccc;float:left}

//两列固定居中
#layout{
	width:604px;margin:0 auto; //因为有边框所以要加1(一个层有左右两个)
}
#left,#right{height:300px;border:1px solid #000}
#left{width:200px;background:#666;float:left}
#right{width:400px;background:#ccc;float:left}

tips://层自动换行可能是因为width大小给得不对

//---position
position:absolute


tips://可以定位后用dw移动层

//---z-ident--层的位置

//三列浮动中间列宽度自动适应(\?)

    //绝对定位的意义
        #left,#right{
            width:200px;
            height:300ps;
            background:	#ccc;
            position:absolute; //不管其他元素,从文档中分离出来,自己定位
        }
#left,#right{
	width:200px;
	height:300px;
	background:	#ccc;
	position:absolute;
}
#left{top:0;left:0}
#right{top:0;right:0}
#center{height:300px;margin:0 200px 0 200px;background:#666} //两边是脱离出来的层,所以要左右空上跟两边层一样大小的width

----float----(??)

.clear{clear:left;} //清除左浮动
#layout{
    width:500px;
    background:#000;
}
#left,#right{height:100px}
#right{
	background:#ccc;width:100px;float:left;height:50px;
}
#left{
	background:#666;width:400px;float:left;	
}
</style>
</head>

<body>
    <div id="layout">
    	<div id="left">content left</div>
    	<div id="right">content right</div>
        <div class="clear"></div>
    </div>
</body>


-----------------盒模型实现圆角效果-----------------

//图片制作 固定宽度,上下用图片,中间可伸长,用背景色
//ps:圆角矩形
#box{width:360px;background:#ccc url(images/bottom.gif) no-repeat left bottom;margin:50px}
#box h2{font-size:16px;background:url(images/top.gif) no-repeat left top;padding:20px;}
#box p{padding:10px 20px;line-height:180%}

//特殊效果圆角
//ps:内阴影

#box{width:360px;background:url(images/m.gif) repeat-y;margin:50px}
#box h2{font-size:16px;background:url(images/top01.gif) no-repeat left top;padding:20px;}
#box p{padding:10px 20px;line-height:180%}
#box .last{background:url(images/bottom01.gif) no-repeat left bottom;height:21px} //是插入的一个div,为了显示出背景图,指定了height大小,也可以指定padding,也可以在不插入类的情况下用,反正就是在末尾添加一个样式(有此背景图)


--------------导航条的制作------------------

#nav{margin:100px;list-style:none;}
#nav li{float:left;}
#nav li a{padding:10px 20px;background:#000;color:#fff;text-decoration:none;text-transform:capitalize;display:block;}//默认ie6的内联元素a没有上下边距,所以要改为块元素
#nav li a:hover{color:#000;background:#ccc;border:1px solid #000;}

<ul id="nav">
  <li><a href="#">home</a></li>
  <li><a href="#">about</a></li>
  <li><a href="#">news</a></li>
  <li><a href="#">products</a></li>
  <li><a href="#">services</a></li>
  <li><a href="#">clients</a></li>
  <li><a href="#">blog</a></li>
  <li><a href="#">link</a></li>
</ul>

//另类 中间才变背景
#nav{margin:100px;list-style:none;}
#nav li{float:left;padding:10px 20px;background:#000;}
#nav li a{padding:3px;background:#000;color:#fff;text-decoration:none;text-transform:capitalize;display:block;}
#nav li a:hover{color:#000;background:#ccc;}

//第三种 ps:渐变叠加 色相饱和度(ctrl+u 着色 色相)

----------全图片导航条------------------

#nav{width:601px;height:34px;background:#000;} //背景应该用图片代替
#nav li{float:left;list-style:none;}
#nav li a{display:block;height:34px;text-indent:-9999px;} //text-indent去除文字
#nav li #a{width:75px;} //根据图片大小来定
#nav li #b{width:74px;}
#nav li #c{width:70px;}
#nav li #d{width:90px;}
#nav li #e{width:88px;}
#nav li #f{width:81px;}
#nav li #g{width:63px;}
#nav li #h{width:60px;}

#nav li #a:hover{background:#ccc;}
#nav li #b:hover{background:#ccc -75px 0;} //背景图片要移动,才能定位准确
#nav li #c:hover{background:#ccc -149px 0;}

--------下拉列表导航条-----------------

line-height和height 一样就会垂直居中

#nav li ul {
	width:160px;
	background:#000;
	list-style:none;
}
#nav li ul li {
	float:none;
	display:block;
}
#nav li ul li a {
	text-indent:0;
	color:#fff;
	text-decoration:none;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:11px;
	padding:6px 0 6px 20px;
	height:0;
}
#nav li dl {
	width:160px;
	background:#333;
	display:none;
	position:absolute; //相对父级元素进行绝对定位 父级元素就应该position:relative
	top:34px; //跟父级元素的height一样大小
	left:0;
}
#nav li dl dd a {
	text-indent:0;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:11px;
	color:#ccc;
	text-decoration:none;
	line-height:34px;
	text-indent:20px;
	
}
#nav li dl dd a:hover {
	background:#666;
	cursor: e-resize;
}
#nav li dl dd a.list {
	background:url(images/nav_list_a.jpg) no-repeat left bottom; //IE6没有效果的,它只针对a进行伪类
}
#nav li dl dd a.list:hover {
	background:url(images/nav_list_b.jpg) no-repeat left bottom;
}
#nav li:hover dl{ //父级元素伪类,显示下联菜单
    display:block;
}

-----
<ul id="nav">
  <li><a href="#" id="a">home</a></li>
  <li><a href="#" id="b">about</a>
    <dl>
      <dd><a href="#">Anout us</a></dd>
      <dd><a href="#">Team</a></dd>
      <dd><a href="#" class="list">Corporate culture</a></dd>
    </dl>
  </li>
</ul>


--------------表格使用----------------

* {
	margin:0;
	padding:0;
}
table {
	font-family:Verdana, Geneva, sans-serif;
	font-size:11px;
	margin:100px;
	border-collapse:collapse; //单元格间距
}
th, td {
	border:1px solid #ccc;
	padding:5px 10px;
}
.blue{background:#DBECF4;}
tr:hover{background:#0066cc;color:white;}


------------------可伸缩圆角框--------------------

ps:ctrl + alt + z恢复裁切
ps:内阴影 阴影 描边

#box{margin:100px;width:36em;background:url(images/left_bottom.jpg) no-repeat left bottom;}
#right_bottom{background:url(images/right_bottom.jpg) no-repeat right bottom;}
#left_top{background:url(images/left-top.jpg) no-repeat left top;}
#right_top{background:url(images/right_top.jpg) no-repeat right top;}
h2{text-align:center;padding:1em 0;font-size:14px;} //font-size解决有些字体不相同
p{padding:1em 2em;line-height:180%;}


<div id="box">
  <div id="right_bottom">
    <div id="left_top">
      <div id="right_top">
        <h2>Welcome to Sitting Pretty</h2>
        <p>Located</p>
        
-------------------------文字排版-------------------------------

tips://浮动后脱离整个父元素的排版,此时要清除浮动(加一个层)
tips://当一个层设为display:none后,再显示时,后占用一定空间,用绝对定位可以解决这个问题,父级相对定位,自己绝对定位
    #layout img{float:right}
    .clear{clear:right;}

tips://+top 此属性只有IE6浏览器识别

----
#layout a span{color:#fff;background:#000;padding:10px;display:none;position:absolute;width:130px;text-decoration:none;left:0;top:20px;+top:30px;}
#layout a:hover span{display:block;}

-------------------制作表单----------------------

//属性选择器(ie6不支持) ie6不支持focus属性
input[type="password"]{width:80px;}
input[type="text"]:focus,input[type="password"]:focus{background:#F2DAFC}

//input text的对齐,先显示为块元素,在浮动,字体右对齐
#content label{width:130px;display:block;float:left;text-align:right;padding-right:10px;}

//for的值为text框id的值 点击label的文字会聚焦到input:text框上
<label for="email">email</label><input type="text" id="email" />    

-----滑动门导航条------------

#nav li a {
	display:block;
	padding:10px 20px; //所以才可以有两种背景
	text-decoration:none;
	color:#000;
	text-transform:capitalize;
	background:url(images/right_top1.jpg) no-repeat right top;
}

#nav li {
	float:left;
	background:url(images/left_top1.jpg) no-repeat left top;
}

------------------制作相册-------------

ps:描边(白色) 阴影 颜色叠加(白色) 白色背景 黑色填充框
ps:ctrl + t自由缩放 
