﻿php简介
   PHP是能让你生成动态网页的工具之一。
   PHP代表：超文本预处理器（PHP: Hypertext Preprocessor）。PHP是完全免费的，不用花钱，你可以从PHP官方站点自由下载。PHP遵守GNU公共许可（GPL)，在这一许可下诞生了许多流行的软件诸如Linux和Emacs。你可以不受限制的获得源码，甚至可以从中加进你自己需要的特色。PHP在大多数Unix平台，GUN/Linux和微软Windows平台上均可以运行。怎样在Windows环境的PC机器或Unix机器上安装PHP的资料可以在PHP官方站点上找到，也可以查阅网页陶吧的“PHP安装全攻备”专题文章。安装过程很简单。

PHP资源：
   http://www.php.net    //官方网站
   http://www.zend.com/en/
       http://devzone.zend.com/public/view   有关于php的最初级到最顶级的话题。
   http://www.hotscripts.com    代码下载站点，不止php的
   PHP Editor: http://www.soysal.com/PHPEd

PHP+Apache+MySQL的安装
   1.下载Apache: 官方下载地址:  http://httpd.apache.org/download.cgi
     安装配置Apach: 安装完成后，可以在【开始】->【程序】中找到安装后的APACHE程序，打开一个Edit the Apache httpd.conf Configuration File.TXT 文件。在这文件中查找“Listen 80”，这个80就是端口，可修改它，如改成8080。继续查找“DocumentRoot”，它后面的路径为服务器默认根目录，将后面的路径更改为你自己的网站路径，比如更改为与Eclipse相同的工作区域。同时路径的“\”全部改为“/”。继续查找“Directory”，将它后面的路径设置为以“DocumentRoot”相同。继续查找“DirectoryIndex”，它后面的路径“index.html”为默认工程首页，你也可以设置你自己的默认首页，比如index.php。接下来APACHE服务器的配置就算结束了。如果你没有更改APACHE的默认工程路径，可以进行测试。键入你的本机IP地址，如果有页面显示则证明你的配置已经成功。
     Apache网页服务器软件: http://www.apache.org/dist/httpd/binaries/win32/
   2.下载安装PHP:  下载地址：http://www.php.net/downloads.php
     将下载到的压缩包解压后释放到适宜控制的路径，并打开解压后的文件，找到php.ini-dist，将此文件后缀-dist删除。然后打开此文件编辑，查找“register_globals = Off”，如果设置为ON，页面之间进行数据传输时，可以直接使用【$变量名】获得，如果为Off，页面是GET方式传输数据，则获得数据时要使用【$_GET(“变量名”)】方式获得。当然设为Off比较安全，不会让人轻易将网页间传送的数据截取。继续查找“extension=php_mysql.dll”将前面的“;”去掉，表示PHP要加载mysql模块。其他模块都在ext文件中，同时加载模块越多，耗费资源越大。另外，为了防止其他错误，也请在环境变量中添加PHP的安装路径与PHP文件中EXT的路径。如【;D:\php;D:\php\ext】，当然“D：”是我的安装路径。
     现在开始将php以module方式与apache相结合，使php融入apache，然后打开apache的配置文件，添加“LoadModule php5_module D:php/php5apache2.dll”，表示以module方式加载php。再换行添加“PHPInidir "D:/php";”，表示php的配置文件php.ini的路径。实质是添加可以执行php的文件类型，设置可以添加.txt文件类型。如：“AddType application/x-http-php .php”和“AddType application/x-http-php .html”。接下来，重新启动你的apache，然后就可以运行php的工程了。
   3.MySQL的下载及安装，略。


php基本语法：
   每句都以分号“;”结束。
   变量区分大小写；而函数不区分大小写。

PHP 与 HTML 互嵌：
    动态内容较少时，用php嵌入到html中。
    页面较复杂时，用html嵌入到php中。

不同的 php 开始和结束 风格标签：
    <?php 和 closing ?>                    //php的开始和结束标签；建议使用。但结尾可简写为“?>”
    <?  and  ?>                            //上一种风格的简写，并非所有服务器都支持
    <script language="php"> and </script>  //html风格的写法
    <%  and  %>                            //asp风格，不建议使用，容易出错

显示：
    echo $variable       显示。显示多个字符串时，可用逗号“,”隔开；但print不可以这样。
    print $variable      显示。两个都可用，但echo更快些，所以更常用。
    printf($variable)    格式化显示。必须带圆括号。可用逗号隔开，显示多个字符串。
    print_r($array)      遍历数组，常用于array。常配合<pre>标签一起用，有自动换行效果。
    var_dump($variable)  显示类型和值
    get_defined_vars()   以array的形式返回定义过的变量，包括php自身的变量

    显示页面信息常用语句：
    echo "<pre>";
    print_r(get_defined_vars());
    echo "</pre>";

注释：comments
    // 单行注释
    #  单行注释(两种写法皆可)；UNIX Shell语法注释
    /* 多行注释  */


变量：variable
    变量可以当作是一种容器，容器里面是值
    值 和 对值的处理方式是，是编程语言的核心
    PHP变量数据类型的定义是通过变量的初始化,系统设定
    数据类型可分为二种：一是标量数据类型 ，二是复合数据类型。
变量命名：
    以“$”开始
    开头可以是字母或下划线，不能用数字开头
    可以包含：字母、数字、下划线、短横线“-”
    中间不能有空格
    区分大小写(case-sensitive)
    注意：短横线一般不用(因为它像减号)；
          首字符一般不会用下划线(因为php自定义的一些变量是这样写的，容易混淆)

常量 Constants:
    设定常量: define("常量名",值);
    常量名一般用大写，不需$开头。值是固定的，不可改。
    defined('常量'); //返回此常量是否已被定义。


标量数据类型：
   (1)布尔型（boolean）
   (2)整形  （integer）
   (3)浮点型（float\double)
   (4)字符串（string）
复合数据类型
   (1)数组  （array）
   (2)对象  （object）
另外，PHP中，还有两种特殊的数据类型：
   (1)资源  （resource）
   (2)空值  （NULL）

浮点数 float：(浮点数，也做”double”)
    整数(int)大小超出其范围后，自动转化为双精度型(double)。
    浮点数的字长和平台相关。
    由于不可能精确的用有限位数表达某些十进制分数，所以不能相信浮点数结果精确到了最后一位，也不能直接比较两个浮点数是否相等。
    如果确实需要更高的精度，应该使用任意精度数学函数库或者 gmp 函数库。
    8进制以“0”开头；16进制以“0x”开头。

    整型与双精度型的范围：
    声明类型    长度(位)    长度(字节)    值的范围
    int         32          4            -2147483647～2147483647
    double      32          4            1.7E-308～1.7E+308 (15位有效数字)

    round($FloatVar,n);  保留n位小数             round(3.14,1)==3.1
    ceil($FloatVar);     变大取整                ceil(3.14)==4;      ceil(-3.14) == -3
    floor($FloatVar);    变小取整                floor(3.14)==3;     floor(-3.14)== -4
    还有很多函数


布尔值 boolean :
    要指定一个布尔值，使用关键字 TRUE 或 FALSE。两个都是大小写不敏感的。如： $foo = True;
    true  返回 1
    false 返回 nothing, 即什么都没返回。
    isset($BooleanVar); 看这变量是否有被设定过，有则返回1，没则没返回

字符串 string:
    字符串可以用三种字面上的方法定义：单引号、双引号、定界符。
    注: 和其他语法不同，单引号字符串中出现的变量和转义序列不会被变量的值替代。
    双引号字符串最重要的一点是其中的变量名会被变量值替代。

    strlen($str1);       返回字符串长度。每个汉字长度为2
    合并 Concatenate:    使用点号“.”，而不是加号“+”
    substr($str2,n);     截取字符串；从第n个开始，到末尾。起始位置为0。也可用于数字等。
    substr($str3,n,m);   截取字符串；从第n个开始，到第m个。起始位置为0。也可用于数字等。
    $str4{n};            返回字符串的第n个字符。只能用于字符串，且也不能用于汉字。

    Case function:
    strtoupper($str);    全部变成大写。语句中可以有中文，但中文不受影响。
    strtolower($str);    全部变成小写。
    ucfirst($str);       只有首字母大写。
    ucwords($str);       每个单词首字母大写。以空格作为不同单词的划分依据。

    字符串的查找、替换：
    str_replace($nums, $n, $str);    $nums是写查找什么；$n是查找到之后替换成什么；$str是被查找的源字符串。
    例如：$str = "There are approcimately 3 other subjects to release.";
          $nums = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
          $newstr = str_replace($nums,"X", $str);
          echo $newstr, "<br><br><br><br>";
          //显示结果： There are approcimately X other subjects to release.

    Clearing Up 修剪、清理字符串：
    trim($str1);         修剪字符串。把字符串左右两边的空格都去掉。
    ltrim($str);         修剪字符串的左边。只把字符串左边的空格去掉。
    rtrim($str);         修剪字符串的右边。只把字符串右边的空格去掉。
    chop($str);          效果等同于 rtrim() 。

    PHP支持以“＼”后面的有特殊意义的字符，如“＼n”代表回车。更多参考正则表达式。


数组 array:
    数组就像是值的列表，其中每个值可以是字符串或数字，甚至是另一个数组。
    数组结构:
    1.基本结构：  $My_Array[0] :       这个0是下标、index、key。从0开始。
    2.名称索引：  $Some_Guy["name"]:   用名称作索引；也称联合数组、哈希表数组。类似枚举。
                  $a["apple"][4]["color"][0]="bad"; //四维数组，普通数组和联合数组连用

    创建 array:   $Some_Guy = array();
    带起始值的    $Some_Guy = array("John","30","5'12");
                  $user1 = Array("name"=>"Mike","city"=>"Oakville","age"=>"30");
    遍历数组，用 print_r() 函数 ；或者 foreach()循环。

    多维数组 Multidimensional array:
    可理解为数组里嵌套的数组，数组里的数组。各维长短不要求相等。
    实际上数组的维数最好不要超过三维，否则会给服务器带来极大的负担，就有些得不偿失了。
    如：$users=Array(
          Array("name"=>"Mike","city"=>"Oakville","age"=>"30"),
          Array("0",1,"bb",3,4),
          Array("kk","jj",array(99,88,"jiji"))
        );

    array functions：
    count($array1)           查出array里有多少个元素。
    max($array1)             找出array里的最大值。
    min($array1)             找出array里的最小值。
    sort($array1)            把array排序。
    rsort($array1)           反过来把array排序。
    implode($array1 ," * ")  把数组转化成字符串阵列。这里用“ * ”作为数组各元素间的分隔符。
                             写成 implode(" * " , $array1) 也可以。
    explode(" * ", $str1)    是implode()的反过程。
    in_array(15, $array1)    查找数组里是否有这个元素(这里用15)；有则返回1，没有则没返回。

    还可以用 array_walk 函数来实现数组的显示。这个函数对数组的每个内容执行同一个函数操作。例如：
    function printelement ($element){print ("$element< p>");}
    array_walk($myarray, "printelement");

<?php   //数组的定义举例：
        $monthName = array(
        /*定义$monthName[1]到$monthName[12]*/
                1=>"January", "February", "March","April", "May", "June",
                "July", "August", "September", "October", "November", "December",
        /*定义$monthName["Jan"]到$monthName["Dec"]*/
                "Jan"=>"January", "Feb"=>"February","Mar"=>"March", "Apr"=>"April",
                "May"=>"May", "Jun"=>"June", "Jul"=>"July", "Aug"=>"August",
                "Sep"=>"September", "Oct"=>"October", "Nov"=>"November", "Dec"=>"December",
        /*定义$monthName["Jan"]到$monthName["Dec"]*/
                "January"=>"January", "February"=>"February","March"=>"March", "April"=>"April",
                "May"=>"May", "June"=>"June", "July"=>"July", "August"=>"August",
                "September"=>"September", "October"=>"October", "November"=>"November", "December"=>"December"
                );
        /*打印相关的元素*/
        echo "Month <B>5</B> is <B>" , $monthName[5] , "</B><BR>\n";
        echo "Month <B>Aug</B> is <B>" , $monthName["Aug"] , "</B><BR>\n";
        echo "Month <B>June</B> is <B>" , $monthName["June"] , "</B><BR>\n";
?>


内置数组   Built-in php Arrays:
    $_SERVER     服务器的细节信息。
    $_ENV        环境信息。如操作系统、处理器。
    $GLOBALS     全局。
    $_POST       处理表单、提交的时候用。
    $_GET        类似上一个。
    $_COOKIE     跟踪客户信息的时候用。用于互动。
    $_SESSION    类似上一个。
    $_REQUEST    是 $_POST 加上 $_GET

    如：显示本页的相对地址 <?php  echo $_SERVER['SCRIPT_NAME']."<BR>"; ?>


类型转换 typecasting：
    php具有自动类型转换功能。但不能依赖它，因为那未必是你想要的。
    gettype($var);                  返回变量的类型。
    settype($var,"string");         设置变量的类型。
    (int) $var;                     指定变量的类型。
    String + integer = integer      这跟Java很不同。加号没有被重载，都按数字算。
    字符串的拼接用"." 点号。        注意：点号不能紧跟着数字，否则会被认为小数点。


运算符：
    算术运算符:   +   -   *   /   %             (加减乘除,求余)
    赋值运算符:   +=  -=  *=  /=  %=  <<=  >>=
    字符串合并:   .   .=                        (点是合并，点等于是赋值合并)
    自增、自减:   ++  --
    比较运算符:   >   >=  <   <=  ==  !=        (true则返回1；false则不返回)
    逻辑运算:     !(反相)                       (true则返回1；false则不返回)
      短路运算    &&  ||                        (true则返回1；false则不返回)
      非短路运算  &   |                         (true则返回1；false则返回0)
    移位运算:     >>  <<
    位运算:       &(AND)  |(OR)   ^(XOR异或)    ~(补码)按位取反 ＝ 加1再取反   (还不知这组怎么用)


条件语句
    if(expression){statement;}      //当statement是单行语句时，可不写大括号。
    if(){} elseif(){} else{}        //这里的elseif可以连在一起写；分开成 else if 也可以。

swith(值){
        case condition1: statement1; break;
        case condition2: statement2; break;
        case condition3: statement3; break;
        default:  statement4; break;
        }

循环 loop：
    while(expression){statement;}
    for(initial;expression;each){statement;}

    foreach() 循环： //用于遍历数组
    形式: foreach($数组 as $临时变量){echo $临时变量;}
          foreach($数组 as $key => $value){echo $key . ": " . $value;}


break 和 continue
    break 退出当前的循环体，在嵌套循环中，只退出当前的一层循环。
    continue 跳过本次循环，继续进行下一轮的循环。可以说，只是本次忽略循环内后面的语句。


函数 function:
    function name($arguments,$arg2,$arg3="test") {
        statement1;
        return statement2;  //返回。可不写
    }
    name($a1, &$a2); //$arg3有默认值，可不写。变量前加“&”，在函数里改变参数的值时，就直接改变了这个变量的值。
    注：PHP不允许重载(overload)。函数可以在定义之前调用。
        允许嵌套定义函数，但不建议这样做；易错。
        函数不调用，则不加载。
        函数名的大小写不敏感，但不能重名。

    global: PHP如果直接在函数中引用与页面变量同名的变量，它会认为函数的变量是一个新的局部变量；
    给变量加上 global ，则成为全局变量，就可以得到页面的同名变量的值。
    如：$bar = "inside";
        function foo() {
                global $bar;    //不写 global 则这函数没显示。
                echo $bar;
        }
        foo();  //显示为 inside

    函数的默认值 Defaultvalue:
    在定义函数时，可以赋予默认值。不一定要这样做，但这是个好习惯。
    由于PHP不允许函数重载(overload),所以有默认值的函数，可以以空参函数来调用。
    function paint($color="red") { statement; } //定义默认值，即是在函数的参数里赋值。
    paint();      //这是使用默认值。
    paint(blue);  //另外赋值。


动态调用函数
<?php   function write($text) {echo $text;}                //定义 write()函数
        function writeBold($text) {echo "<B>$text</B>";}   //定义 writeBold()函数

        $myFunction = "write";         //定义变量
        $myFunction("你好!<BR>\n");    //由于变量后面有括号，所以找名字相同的函数: write($text)

        $myFunction = "writeBold";     //定义变量
        $myFunction("再见!<BR>\n");    //由于变量后面有括号，所以找名字相同的函数: writeBold($text)
?>


变量的变量
    在php中变量与许多常用语言最大的区别就是增加了一个‘$’前缀，有了这一个前缀，又增加了PHP的独特的一种处理方式。
    一个前缀代表普通的变量，但是两个前缀呢？这就是变量的变量，例：
        <?php
         $name = "hello";
         ${$name}="world";              //等同于$hello=″world″;
         echo "$name $hello","<br>";    //输出：hello world
         echo "$name ${$name}","<br>";  //同样输出：hello world  //测试时是：hello $hello
         for($i=1;$i<=5;$i++)
         { ${"var"."$i"}=$i;}           //定义 $var1 ~ $var5
         echo $var3;                    //输出：3 。证明 $var3 刚才被生成了。  //测试不成功，可能版本问题。
        ?>
    上面的例子基本上可以理解$$name了，PHP的标准定义则是${$name}。
    我们有了变量的变量就可以实现动态增加变量了，这简直就是神奇。


MySQL crud:   Create Read Update Delete
    Create:   INSERT INTO table (column1,column2,column3) VALUES (val1,val2,val3);
    Read:     SELECT * FROM table WHERE column1 = 'some_text' ORDER BY column1,column2 ASC;
    Update:   UPDATE table SET column1 = 'some_text' WHERE id = 1;
    DELETE:   DELETE FROM table WHERE id = 1;

MySQL数据库连接步骤：
    //步骤一：初始化
    $dbHostname = "localhost";
    $dbUsername = "root";
    $dbPassword = "root";
    $dbName     = "samples";

    //步骤二：连接数据库
    $dblink = MYSQL_CONNECT($dbHostname, $dbUsername, $dbPassword) OR DIE("连接数据库失败");
    //进入MySQL的某个 database
    mysql_select_db($dbName) or die( "Unable to select database ".$dbName);

    //步骤三：crud
    mysql_query("set names GBK");  //设置字体编码

        //Create:  (以下的 $Customer_id 等变量须先赋值)
        $Customer_id = addslashes($_REQUEST['Customer_id_name']); //取得传过来的参数...多个...
        $sqlQuery = "INSERT INTO table (id , name , company)
        VALUES ('$Customer_id' , '$thisFname' , '$thisCompany')";
        mysql_query($sqlQuery) or die('Query failed: ' . mysql_error());

        //Read:
        $query = 'SELECT * FROM table' . 'ORDER BY customer_id ASC LIMIT 0,10'; //LIMIT 是数据库分页
        $result = mysql_query($query) or die('Query failed: ' . mysql_error());
        echo "<table border='1'>\n";
        while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
           echo "\t<tr>\n";
           foreach ($line as $col_value) {
                   echo "\t\t<td>$col_value</td>\n";
           }
           echo "\t</tr>\n";
        }echo "</table>\n";

        //Update:  (以下的 $Customer_id 等变量须先赋值)
        $sql = "UPDATE table SET id = '$Customer_id' , name = '$Fname' , company = '$thisCompany'
        WHERE id = '$Customer_id'";
        MYSQL_QUERY($sql);

        //Delete:
        $Customer_id = addslashes($_POST['Customer_idField']); //接受提交过来的数据
        $sql = "DELETE FROM table WHERE id = '$Customer_id'";
        MYSQL_QUERY($sql);

        //search: 搜索
        $thisKeyword = $_REQUEST['keyword'];   //接收要求搜索的关键字
        $sqlQuery = "SELECT * FROM table WHERE id like '%$thisKeyword%' OR name like '%$thisKeyword%'
          OR company like '%$thisKeyword%'";   //各个列都比较一下
        $result = MYSQL_QUERY($sqlQuery);
        $numberOfRows = MYSQL_NUM_ROWS($result); //取得返回列的数目
        if ($numberOfRows==0) { echo "Sorry. No records found !!"; }
        else if ($numberOfRows>0) {
           $i=0;
           echo "<table border='1'>";
           while ($i < $numberOfRows){
                 $Customer_id = MYSQL_RESULT($result,$i,"id");
                 $Fname = MYSQL_RESULT($result,$i,"name");
                 $Company = MYSQL_RESULT($result,$i,"company");
                <TR>
                <TD nowrap><?php echo $Customer_id; ?></TD>
                <TD nowrap><?php echo $Fname; ?></TD>
                <TD nowrap><?php echo $Company; ?></TD>
                </TR> $i++;
           } </TABLE>
        }

    //步骤四：关闭连接。这步可不写，数据库引擎会自动做；写出来更有条理
    /*显示大量数据库的内容时，网速慢的用户可能会拖累整个数据库。
      因此，尽快的连上数据库，取得需要的资料后，马上关闭数据库，再慢慢送给用户，应是最好的对策。*/
    mysql_close($dblink);


引用文件：
    引用文件的方法有：include、include-once 和 require、require-once
    include:PHP 的网页在遇到include 所指定的文件时，才将它读进来。这种方式，更符合人们的习惯。
            这个函数一般是放在流程控制的处理部分中；可以把程序执行时的流程简单化。
            如: include("./intro/hello.php"); 。  //注，路径的斜杠方向，正反都行。
    require:在 PHP 程序执行前，先读入 require 所指定引入的文件，使它变成 PHP 程序网页的一部份。
            如: require("intro\hello.php"); 。
            这个函数通常放在 PHP 程序的最前面。常用的函数，可以用这个方法将它引入网页中。
    once:   编程时，在我们极力保证函数和类的独立性和公用性的同时，而如果在程序里面我们调用了一个非公用文件里的类，但我们的另一个处理文件也调用了这个文件里的这个类，那么程序将会出错，说是重定义了这个类，因此，我们将不得不去重写文件或者丢弃它的独立性，但在PHP4以后不存在这个问题，因为PHP4可以使用require_once和include_once方法，顾名思义也就是他们只调用一次我们所需要的文件，如果有两次调用文件的话，第二次调用的文件不起作用。


文件操作：
1. 打开文件、读取内容
   <?php  echo "<H3>通过http协议打开文件</H3>\n";
        //打开同文件夹下的某文件
        if (!($myFile = fopen($_REQUEST['DOCUMENT_ROOT'] . "data.txt", "r")))
        {  print("文件不能打开");  exit;  }
        while(!feof($myFile))     // 按行读取文件中的内容
        {  $myLine = fgets($myFile, 255);  //fgets还可用fgetss、fgetc；效果有所不同
           echo "$myLine <BR>\n";
        }

        // 打开文件同时，打印每一行
        $myFile = file("data.txt");
        for ($index = 0; $index < count($myFile); $index++)
        {   echo $myFile[$index] , "<BR>";    }

        fclose($myFile);    //关闭文件的句柄
   ?>

   <?php //读取某文件夹(默认当前文件夹)下的所有文件夹及文件
         function aa($dirName="."){
            $d = dir($dirName);
            echo "Handle: " , $d->handle , "<br>\n";      //打印例子：Resource id #3
            //echo "Path: " , $d->path , "<br>\n";        //打印正在读取的文件夹路径
            while ($entry=$d->read()) {
                if ($entry == "." || $entry == "..") continue;
                echo $dirName , "/",$entry , "<br>\n";
                if (@filetype($dirName . "/".$entry)=="dir") aa($d->path . "/" . $entry);
           }
           $d->close();
        }
   ?>

2. 创建文件
   <?php
        if(!file_exists(myDir1)){mkdir("myDir1", 0777);} //创建文件夹
        $configfile="要写入的内容";               //内容按String里的写法，有特殊字符
        $filenum = fopen ("myDir1\DATA.TXT","w"); //如果没有这文件，会自动创建
        fwrite($filenum, $configfile);            //把内容写进文件里
        fclose($filenum);
   ?>



常用语句：
   让浏览器重定向到某页面：   Header("Location: ./name.php"); //“name.php”是页面名称

   $msg=base64_encode($msg);  //把页面接收的 $msg 用 BASE64 编码。解决中文冲突问题。
   //显示时：
   $msg=base64_decode($get_msg);  //$get_msg 即刚才用了 BASE64 编码的 $msg；现在翻译回来
   $msg=nl2br($msg); //将换行字符转成 <br>。不写这行则没有显示出换行
























PHP 函数索引
(共有 967 个函数)

Abs:                取得绝对值。
Acos:               取得反余弦值。
ada_afetch:         取得数据库的返回列。
ada_autocommit:     开关自动改动功能。
ada_close:          关闭 Adabas D 链接。
ada_commit:         改动 Adabas D 数据库。
ada_connect:        链接至 Adabas D 数据库。
ada_exec:           执行 SQL 指令。
ada_fetchrow:       取得返回一列。
ada_fieldname:      取得字段名称。
ada_fieldtype:      取得字段资料类型。
ada_freeresult:     释出返回资料的内存。
ada_numfields:      取得字段数目。
ada_numrows:        取得返回列数目。
ada_result:         取得返回资料。
ada_resultall:      返回 HTML 表格资料。
ada_rollback:       撤消当前交易。
AddSlashes:         字符串加入斜线。
apache_lookup_uri:  获得所有的 URI 相关信息。
apache_note:        获得及配置apache服务器的请求纪录。
array:              建立一个新的数组。
array_walk:         让使用者自订函数能处理数组中的每一个元素。
arsort:             将数组的值由大到小排序。
Asin:               取得反正弦值。
asort:              将数组的值由小到大排序。
aspell_check:       检查一个单字。
aspell_check-raw:   检查一个单字，即使拼错也不改变或修正。
aspell_new:         载入一个新的字典。
aspell_suggest:     检查一个单字，并提供拼写建议。
Atan:               取得反正切值。
Atan2:              计算二数的反正切值。
base64_decode($msg):将 BASE64 编码字符串解码。
base64_encode($msg):将字符串以 BASE64 编码。(可处理数据库中文冲突问题)锯箭法
basename:           返回不含路径的文件字符串。
base_convert:       转换数字的进位方式。
bcadd:              将二个高精确度数字相加。
bccomp:             比较二个高精确度数字。
bcdiv:              将二个高精确度数字相除。
bcmod:              取得高精确度数字的余数。
bcmul:              将二个高精确度数字相乘。
bcpow:              求一高精确度数字次方值。
bcscale:            配置程序中所有 BC 函数库的默认小数点位数。
bcsqrt:             求一高精确度数字的平方根。
bcsub:              将二个高精确度数字相减。
bin2hex:            二进位转成十六进位。
BinDec:             二进位转成十进位。
Ceil:               计算大于指定数的最小整数。
chdir:              改变目录。
checkdate:          验证日期的正确性。
checkdnsrr:         检查指定网址的 DNS 记录。
chgrp:              改变文件所属的群组。
chmod:              改变文件的属性。
Chop:               去除连续空白。
chown:              改变文件的拥有者。
Chr:                返回序数值的字符。
chunk_split:        将字符串分成小段。
clearstatcache:     清除文件状态快取。
closedir:           关闭目录 handle。
closelog:           关闭系统纪录。
connection_aborted: 若链接中断则返回 true。
connection_status:  取得连接状态。
connection_timeout: 若超过 PHP 程序执行时间则返回 true。
convert_cyr_string: 转换古斯拉夫字符串成其它字符串。
copy:               复制文件。
Cos:                余弦计算。
count:              计算变量或数组中的元素个数。
crypt:              将字符串用 DES 编码加密。
current:            返回数组中目前的元素。
date:               将服务器的时间格式化。    如：date("Y年m月d日 星期w H时i分s秒", time())
dbase_add_record:   加入资料到 dBase 资料表。
dbase_close:        关闭 dBase 资料表。
dbase_create:       建立 dBase 资料表。
dbase_delete_record:  删除 dBase 资料表的资料。
dbase_get_record:   取得 dBase 资料表的资料。
dbase_numfields:    取得 dBase 资料表的字段数。
dbase_numrecords:   取得 dBase 资料表的列数。
dbase_open:         打开 dBase 资料表。
dbase_pack:         清理 dBase 资料表。
dba_close:          关闭数据库。
dba_delete:         删除指定资料。
dba_exists:         检查键是否存在。
dba_fetch:          取回指定资料。
dba_firstkey:       取回首笔键值。
dba_insert:         加入资料。
dba_nextkey:        取回下笔键值。
dba_open:           打开数据库链接。
dba_optimize:       最佳化数据库。
dba_popen:          打开并保持数据库链接。
dba_replace:        改动或加入资料。
dba_sync:           数据库同步化。
dblist:             取得 DBM 的信息。
dbmclose:           关闭 DBM 数据库。
dbmdelete:          删除指定资料。
dbmexists:          检查键是否存在。
dbmfetch:           取回指定资料。
dbmfirstkey:        取回首笔键名。
dbminsert:          加入资料。
dbmnextkey:         取回下笔键值。
dbmopen:            打开 DBM 数据库链接。
dbmreplace:         改动或加入资料。
debugger_off:       关闭内建的 PHP 除错器。
debugger_on:        使用内建的 PHP 除错器。
DecBin:             十进位转二进位。
DecHex:             十进位转十六进位。
DecOct:             十进位转八进位。
define("常量名",值) 设定常量。
defined('常量')     返回此常量是否已被定义。
delete:             无用的项目。
die:                输出信息并中断 PHP 程序。
dir:                目录类别类。
dirname:            取得路径中的目录名。
diskfreespace:      取得目录所在的剩余可用空间。
dl:                 载入 PHP 扩充模块。
doubleval:          变量转成倍浮点数类型。
each:               返回数组中下一个元素的索引及值。
easter_date:        计算复活节日期。
easter_days:        计算复活节与三月廿一日之间日期数。
echo:               输出字符串。
empty:              判断变量是否已配置。
end:                将数组的内部指针指到最后的元素。
ereg:               字符串比对解析。
eregi:              字符串比对解析，与大小写无关。
eregi_replace:      字符串比对解析并取代，与大小写无关。
ereg_replace:       字符串比对解析并取代。
error_log:          送出一个错误信息。
error_reporting:    配置错误信息回报的等级。
escapeshellcmd:     除去字符串中的特殊符号。
eval:               将值代入字符串之中。
exec:               执行外部程序。
exit:               结束 PHP 程序。此页面之后的程序不再执行。
Exp:                自然对数 e 的次方值。
explode:            切开字符串。
extract:            汇入数组到符号表。
fclose:             关闭已打开的文件。
FDF_close:          关闭 FDF 文件。
FDF_create:         建立新的 FDF 文件。
FDF_get_file:       取得 /F 键的值。
FDF_get_status:     取得 /STATUS 键的值。
FDF_get_value:      取得字段的值。
FDF_next_field_name:下一字段的名称。
FDF_open:           打开 FDF 文件。
FDF_save:           将 FDF 文件存文件。
FDF_set_ap:         配置显示字段。
FDF_set_file:       配置 /F 键。
FDF_set_status:     配置 /STATUS 键。
FDF_set_value:      配置字段的值。
feof:               测试文件指针是否指到文件尾。
fgetc:              取得文件指针所指的字符。
fgetcsv:            取得文件指针所指行，并解析 CSV 字段。
fgets:              取得文件指针所指的行。(保留html标志)
fgetss:             取得文件指针所指的行，并去掉 HTML 语言标记。
file("file"):       将文件全部读入数组变量中。(数组中每个元素是文件的一行)
fileatime("file"):  取得文件最后的存取时间。
filectime("file"):  取得文件最后的改变时间。
filegroup:          取得文件所属的群组。
fileinode("file"):  取得文件的 inode 值。
filemtime("file"):  取得文件最后的修改时间。
fileowner("file"):  取得文件的拥有者(UID 值)。
fileperms:          取得文件的权限配置。
filepro:            读取 FilePro Map 文件。
filepro_fieldcount: 取得字段数目。
filepro_fieldname:  取得字段名称。
filepro_fieldtype:  取得字段类型。
filepro_fieldwidth: 取得字段宽度。
filepro_retrieve:   取得指定储存格资料。
filepro_rowcount:   取得列数目。
filesize("file"):   获得文件的大小(单位：字节)。
filetype("file"):   获得文件的类型。
file_exists:        检查文件是否存在。
flock:              锁住文件。
Floor:              计算小于指定数的最大整数。
flush:              清出输出缓冲区。
fopen:              打开文件或者 URL。
fpassthru:          输出所有剩余资料。
fputs:              写到文件指针。
fread:              位组的方式读取文件。
FrenchToJD:         将法国共和历法转换成为凯撒日计数。
fseek:              移动文件指针。
fsockopen:          打开网络的 Socket 链接。
ftell:              取得文件读写指针位置。
ftp_cdup:           回上层目录。
ftp_chdir:          改变路径。
ftp_connect:        打开 FTP 链接。
ftp_delete:         将文件删除。
ftp_fget:           下载文件，并存在已开的文件中。
ftp_fput:           上传已打开文件。
ftp_get:            下载文件。
ftp_login:          登入 FTP 服务器。
ftp_mdtm:           获得指定文件的最后修改时间。
ftp_mkdir:          建新目录。
ftp_nlist:          列出指定目录中所有文件。
ftp_pasv:           切换主被动传输模式。
ftp_put:            上传文件。
ftp_pwd:            取得目前所在路径。
ftp_quit:           关闭 FTP 连接。
ftp_rawlist:        详细列出指定目录中所有文件。
ftp_rename:         将文件改名。
ftp_rmdir:          删除目录。
ftp_size:           获得指定文件的大小。
ftp_systype:        显示服务器系统。
function_exists:    检查函数是否已定义。
fwrite:             二进位位方式写入文件。
getallheaders:      获得所有 HTTP 变量值。
getdate:            获得时间及日期信息。
getenv:             取得系统的环境变量
gethostbyaddr:      返回机器名称。
gethostbyname:      返回 IP 网址。
gethostbynamel:     返回机器名称的所有 IP。
GetImageSize:       取得图片的长宽。
getlastmod:         返回该网页的最后修改时间。
getmxrr:            取得指定网址 DNS 记录之 MX 字段。
getmyinode:         返回该网页的 inode 值。
getmypid:           返回 PHP 的行程代号。
getmyuid:           返回 PHP 的使用者代码。
getrandmax:         随机数的最大值。
getrusage:          返回系统资源使用率。
gettimeofday:       取得目前时间。
gettype:            取得变量的类型。
get_cfg_var:        取得 PHP 的配置选项值。
get_current_user:   取得 PHP 行程的拥有者名称。
get_magic_quotes_gpc:      取得 PHP 环境变量 magic_quotes_gpc 的值。
get_magic_quotes_runtime:  取得 PHP 环境变量 magic_quotes_runtime 的值。
get_meta_tags:      抽出文件所有 meta 标记的资料。
gmdate:             取得目前与 GMT 差后的时间。
gmmktime:           取得 UNIX 时间戳记的格林威治时间。
GregorianToJD:      将格里高里历法转换成为凯撒日计数。
gzclose:            关闭压缩文件。
gzeof:              判断是否在压缩文件尾。
gzfile:             读压缩文件到数组中。
gzgetc:             读压缩文件中的字符。
gzgets:             读压缩文件中的字符串。
gzgetss:            读压缩文件中的字符串，并去掉 HTML 指令。
gzopen:             打开压缩文件。
gzpassthru:         解压缩指针后全部资料。
gzputs:             资料写入压缩文件。
gzread:             压缩文件读出指定长度字符串。
gzrewind:           重设压缩文件指针。
gzseek:             设压缩文件指针至指定处。
gztell:             取得压缩文件指针处。
gzwrite:            资料写入压缩文件。
header:             送出 HTTP 协议的标头到浏览器
HexDec:             十六进位转十进位。
htmlentities:       将所有的字符都转成 HTML 字符串。
htmlspecialchars:   将特殊字符转成 HTML 格式。
hw_Children:        取得子类代码。
hw_ChildrenObj:     取得子类的类记录。
hw_Close:           关闭 Hyperwave 连接。
hw_Connect:         连上 Hyperwave 服务器。
hw_Cp:              复制类。
hw_Deleteobject:    删除类。
hw_DocByAnchor:     取得指定锚的文件类代码。
hw_DocByAnchorObj:          取得指定锚的文件类。
hw_DocumentAttributes:      取得指定文件类属性。
hw_DocumentBodyTag:         取得指定文件类的文件主体标记。
hw_DocumentContent:         取得指定文件类的内容。
hw_DocumentSetContent:      重设指定文件类的内容。
hw_DocumentSize:            取得文件大小。
hw_EditText:                改动文字档宁。
hw_Error:                   取得错误代码。
hw_ErrorMsg:                取得错误信息。
hw_Free_Document:           释放文件使用的内存。
hw_GetAnchors:              取得文件的链接锚。
hw_GetAnchorsObj:           取得文件的链接锚记录。
hw_GetAndLock:              取得并锁住类。
hw_GetChildColl:            取得子类们的 ID。
hw_GetChildCollObj:         取得子类们的资料。
hw_GetChildDocColl:         取得全部子文件聚集。
hw_GetChildDocCollObj:      取得全部子文件聚集记录。
hw_GetObject:               取得类。
hw_GetObjectByQuery:        搜寻类。
hw_GetObjectByQueryColl:    搜寻聚集类。
hw_GetObjectByQueryCollObj: 搜寻聚集类。
hw_GetObjectByQueryObj:     搜寻类。
hw_GetParents:              取得父类的 ID。
hw_GetParentsObj:           取得父类的资料。
hw_GetRemote:               取得远端文件。
hw_GetRemoteChildren:       取得远端的子文件。
hw_GetSrcByDestObj:         取得指定目的的文件内容。
hw_GetText:                 取得纯文字档宁。
hw_GetUsername:     目前使用者名字。
hw_Identify:        使用者身份确认。
hw_InCollections:   检查类聚集。
hw_Info:            连接信息。
hw_InsColl:         插入聚集。
hw_InsDoc:          插入文件。
hw_InsertDocument:  上传文件。
hw_InsertObject:    插入类记录。
hw_Modifyobject:    修改类记录。
hw_Mv:              移动类。
hw_New_Document:    建立新文件。
hw_Objrec2Array:    类记录转为数组。
hw_OutputDocument:  输出文件。
hw_pConnect:        连上 Hyperwave 服务器。
hw_PipeDocument:    取得文件。
hw_Root:            取得根类代码。
hw_Unlock:          取消锁定。
hw_Who:             列出目前使用者。
ibase_bind:         链接 PHP 变量到 InterBase 参数。
ibase_close:        关闭 InterBase 服务器连接。
ibase_connect:      打开 InterBase 服务器连接。
ibase_execute:      执行 SQL 的指令部分。
ibase_fetch_row:    返回单列的各字段。
ibase_free_query:   释放查询指令占用内存。
ibase_free_result:  释放返回占用内存。
ibase_pconnect:     保持 InterBase 服务器连接。
ibase_prepare:      分析 SQL 语法。
ibase_query:        送出一个 query 字符串。
ibase_timefmt:      配置时间格式。
ifxus_close_slob:   删除 slob 类。
ifxus_create_slob:  建立 slob 类。
ifxus_open_slob:    打开 slob 类。
ifxus_read_slob:    读取指定数目的 slob 类。
ifxus_seek_slob:    配置目前文件或找寻位置。
ifxus_tell_slob:    返回目前文件或找寻位置。
ifxus_write_slob:   将字符串写入 slob 类中。
ifx_affected_rows:  得到 Informix 最后操作影响的列数目。
ifx_blobinfile_mode:配置长位类模式。
ifx_byteasvarchar:  配置位组模式默认值。
ifx_close:          关闭 Informix 服务器连接。
ifx_connect:        打开 Informix 服务器连接。
ifx_copy_blob:      复制长位类。
ifx_create_blob:    建立长位类。
ifx_create_char:    建立字符类。
ifx_do:             执行已准备 query 字符串。
ifx_error:          取得 Informix 最后的错误。
ifx_errormsg:       取得 Informix 最后错误信息。
ifx_fetch_row:      返回单列的各字段。
ifx_fieldproperties:列出 Informix 的 SQL 字段属性。
ifx_fieldtypes:     列出 Informix 的 SQL 字段。
ifx_free_blob:      删除长位类。
ifx_free_char:      删除字符类。
ifx_free_result:    释放返回占用内存。
ifx_free_slob:      删除 slob 类。
ifx_getsqlca:       取得 query 后的 sqlca 信息。
ifx_get_blob:       取得长位类。
ifx_get_char:       取得字符类。
ifx_htmltbl_result: 将 query 返回资料转成 HTML 表格。
ifx_nullformat:     配置空字符模式默认值。
ifx_num_fields:     取得返回字段的数目。
ifx_num_rows:       取得返回列的数目。
ifx_pconnect:       打开 Informix 服务器持续连接。
ifx_prepare:        准备 query 字符串。
ifx_query:          送出一个 query 字符串。
ifx_textasvarchar:  配置文字模式默认值。
ifx_update_blob:    更改长位类。
ifx_update_char:    更改字符类。
ignore_user_abort:  连接中断后程序是否执行。
ImageArc:           画弧线。
ImageChar:          写出横向字符。
ImageCharUp:        写出直式字符。
ImageColorAllocate: 匹配颜色。
ImageColorAt:       取得图中指定点颜色的索引值。
ImageColorClosest:  计算色表中与指定颜色最接近者。
ImageColorExact:    计算色表上指定颜色索引值。
ImageColorResolve:  计算色表上指定或最接近颜色的索引值。
ImageColorSet:      配置色表上指定索引的颜色。
ImageColorsForIndex:    取得色表上指定索引的颜色。
ImageColorsTotal:       计算图的颜色数。
ImageColorTransparent:  指定透明背景色。
ImageCopyResized:       复制新图并调整大小。
ImageCreate:        建立新图。
ImageCreateFromGIF: 取出 GIF 图型。
ImageCreateFromPNG: 取出 PNG 图型。
ImageDashedLine:    绘虚线。
ImageDestroy:       结束图形。
ImageFill:          图形着色。
ImageFilledPolygon: 多边形区域着色。
ImageFilledRectangle:   矩形区域着色。
ImageFillToBorder:  指定颜色区域内着色。
ImageFontHeight:    取得字型的高度。
ImageFontWidth:     取得字型的宽度。
ImageGIF:           建立 GIF 图型。
ImageInterlace:     使用交错式显示与否。
ImageLine:          绘实线。
ImageLoadFont:      载入点阵字型。
ImagePNG:           建立 PNG 图型。
ImagePolygon:       绘多边形。
ImagePSBBox:        计算 PostScript 文字所占区域。
ImagePSEncodeFont:  PostScript 字型转成向量字。
ImagePSFreeFont:    卸下 PostScript 字型。
ImagePSLoadFont:    载入 PostScript 字型。
ImagePSText:        写 PostScript 文字到图中。
ImageRectangle:     绘矩形。
ImageSetPixel:      绘点。
ImageString:        绘横式字符串。
ImageStringUp:      绘直式字符串。
ImageSX:            取得图片的宽度。
ImageSY:            取得图片的高度。
ImageTTFBBox:       计算 TTF 文字所占区域。
ImageTTFText:       写 TTF 文字到图中。
imap_8bit:          将八位转成 qp 编码。
imap_alerts:        所有的警告信息。
imap_append:        附加字符串到指定的邮箱中。
imap_base64:        解 BASE64 编码。
imap_binary:        将八位转成 base64 编码。
imap_body:          读信的内文。
imap_check:         返回邮箱信息。
imap_clearflag_full:清除信件标志。
imap_close:         关闭 IMAP 链接。
imap_createmailbox: 建立新的信箱。
imap_delete:        标记欲删除邮件。
imap_deletemailbox: 删除既有信箱。
imap_errors:        所有的错误信息。
imap_expunge:       删除已标记的邮件。
imap_fetchbody:     从信件内文取出指定部分。
imap_fetchheader:   取得原始标头。
imap_fetchstructure:获取某信件的结构信息。
imap_getmailboxes:  取得全部信件详细信息。
imap_getsubscribed: 列出所有订阅邮箱。
imap_header:        获取某信件的标头信息。
imap_headers:       获取全部信件的标头信息。
imap_last_error:    最后的错误信息。
imap_listmailbox:   获取邮箱列示。
imap_listsubscribed:获取订阅邮箱列示。
imap_mailboxmsginfo:取得目前邮箱的信息。
imap_mail_copy:     复制指定信件到它处邮箱。
imap_mail_move:     移动指定信件到它处邮箱。
imap_msgno:         列出 UID 的连续信件。
imap_num_msg:       取得信件数。
imap_num_recent:    取得新进信件数。
imap_open:          打开 IMAP 链接。
imap_ping:          检查 IMAP 是否连接。
imap_qprint:        将 qp 编码转成八位。
imap_renamemailbox: 更改邮箱名字。
imap_reopen:        重开 IMAP 链接。
imap_rfc822_parse_adrlist:     解析电子邮件位址。
imap_rfc822_write_address:     电子邮件位址标准化。
imap_scanmailbox:   寻找信件有无特定字符串。
imap_search:        搜寻指定标准的信件。
imap_setflag_full:  配置信件标志。
imap_sort:          将信件标头排序。
imap_status:        目前的状态信息。
imap_subscribe:     订阅邮箱。
imap_uid:           取得信件 UID。
imap_undelete:      取消删除邮件标记。
imap_unsubscribe:   取消订阅邮箱。
implode:            将数组变成字符串。
intval:             变量转成整数类型。
iptcparse:          使用 IPTC 模块解析位资料。
isset:              判断变量是否已配置。
is_array:           判断变量类型是否为数组类型。
is_dir:             测试文件是否为目录。
is_double:          判断变量类型是否为倍浮点数类型。
is_executable:      测试文件是否为可执行文件。
is_file:            测试文件是否为正常文件。
is_float:           判断变量类型是否为浮点数类型。
is_int:             判断变量类型是否为整数类型。
is_integer:         判断变量类型是否为长整数类型。
is_link:            测试文件是否为链接文件。
is_long:            判断变量类型是否为长整数类型。
is_object:          判断变量类型是否为类类型。
is_readable:        测试文件是否可读取。
is_real:            判断变量类型是否为实数类型。
is_string:          判断变量类型是否为字符串类型。
is_writeable:       测试文件是否可写入。
JDDayOfWeek:        返回日期在周几。
JDMonthName:        返回月份名。
JDToFrench:         将凯撒日计数转换成为法国共和历法。
JDToGregorian:      将凯撒日计数 (Julian Day Count) 转换成为格里高里历法 (Gregorian date)。
JDToJewish:         将凯撒日计数转换成为犹太历法。
JDToJulian:         将凯撒日计数转换成为凯撒历法。
JewishToJD:         将犹太历法转换成为凯撒日计数。
join:               将数组变成字符串。
JulianToJD:         将凯撒历法转换成为凯撒日计数。
key:                取得数组中的索引资料。
ksort:              将数组的元素依索引排序。
ldap_add:           增加 LDAP 名录的条目。
ldap_bind:          系住 LDAP 目录。
ldap_close:         结束 LDAP 链接。
ldap_connect:       连上 LDAP 服务器。
ldap_count_entries: 搜寻结果的数目。
ldap_delete:        删除指定资源。
ldap_dn2ufn:        将 dn 转成易读的名字。
ldap_explode_dn:    切开 dn 的字段。
ldap_first_attribute: 取得第一笔资源的属性。
ldap_first_entry:   取得第一笔结果代号。
ldap_free_result:   释放返回资料内存。
ldap_get_attributes:取得返回资料的属性。
ldap_get_dn:        取得 DN 值。
ldap_get_entries:   取得全部返回资料。
ldap_get_values:    取得全部返回值。
ldap_list:          列出简表。
ldap_modify:        改变 LDAP 名录的属性。
ldap_mod_add:       增加 LDAP 名录的属性。
ldap_mod_del:       删除 LDAP 名录的属性。
ldap_mod_replace:   新的 LDAP 名录取代旧属性。
ldap_next_attribute:取得返回资料的下笔属性。
ldap_next_entry:    取得下一笔结果代号。
ldap_read:          取得目前的资料属性。
ldap_search:        列出树状简表。
ldap_unbind:        结束 LDAP 链接。
leak:               泄出内存。
link:               建立硬式链接。
linkinfo:           取得链接信息。
list:               列出数组中元素的值。
Log:                自然对数值。
Log10:              10 基底的对数值。
lstat:              取得链接文件相关信息。
ltrim:              去除连续空白。
mail:               寄出电子邮件。
max:                取得最大值。
mcrypt_cbc:         使用 CBC 将资料加/解密。
mcrypt_cfb:         使用 CFB 将资料加/解密。
mcrypt_create_iv:   从随机源将向量初始化。
mcrypt_ecb:         使用 ECB 将资料加/解密。
mcrypt_get_block_size:     取得编码方式的区块大小。
mcrypt_get_cipher_name:    取得编码方式的名称。
mcrypt_get_key_size:       取得编码钥匙大小。
mcrypt_ofb:          使用 OFB 将资料加/解密。
md5:                 计算字符串的 MD5 哈稀。
mhash:               计算哈稀值。
mhash_count:         取得哈稀 ID 的最大值。
mhash_get_block_size:取得哈稀方式的区块大小。
mhash_get_hash_name: 取得哈稀演算法名称。
microtime:           取得目前时间的 UNIX 时间戳记的百万分之一秒值。
min:                 取得最小值。
mkdir:               建立目录。
mktime:              取得 UNIX 时间戳记。
msql:                送出 query 字符串。
msql_affected_rows:  得到 mSQL 最后操作影响的列数目。
msql_close:          关闭 mSQL 数据库连接。
msql_connect:        打开 mSQL 数据库连接。
msql_createdb:       建立一个新的 mSQL 数据库。
msql_create_db:      建立一个新的 mSQL 数据库。
msql_data_seek:      移动内部返回指针。
msql_dbname:         取得目前所在数据库名称。
msql_dropdb:         删除指定的 mSQL 数据库。
msql_drop_db:        删除指定的 mSQL 数据库。
msql_error:          取得最后错误信息。
msql_fetch_array:    返回数组资料。
msql_fetch_field:    取得字段信息。
msql_fetch_object:   返回类资料。
msql_fetch_row:      返回单列的各字段。
msql_fieldflags:     获得字段的标志。
msql_fieldlen:       获得字段的长度。
msql_fieldname:      返回指定字段的名称。
msql_fieldtable:     获得字段的资料表 (table) 名称。
msql_fieldtype:      获得字段的类型。
msql_field_seek:     配置指针到返回值的某字段。
msql_freeresult:     释放返回占用内存。
msql_free_result:    释放返回占用内存。
msql_listdbs:        列出可用数据库 (database)。
msql_listfields:     列出指定资料表的字段 (field)。
msql_listtables:     列出指定数据库的资料表 (table)。
msql_list_dbs:       列出可用数据库 (database)。
msql_list_fields:    列出指定资料表的字段 (field)。
msql_list_tables:    列出指定数据库的资料表 (table)。
msql_numfields:      取得返回字段的数目。
msql_numrows:        取得返回列的数目。
msql_num_fields:     取得返回字段的数目。
msql_num_rows:       取得返回列的数目。
msql_pconnect:       打开 mSQL 服务器持续连接。
msql_query:          送出一个 query 字符串。
msql_regcase:        将字符串逐字返回大小写字符。
msql_result:         取得查询 (query) 的结果。
msql_selectdb:       选择一个数据库。
msql_select_db:      选择一个数据库。
msql_tablename:      返回指定资料表的名称。
mssql_affected_rows: 取得最后 query 影响的列数。
mssql_close:         关闭与数据库的连接。       参数：(mysql_connect('localhost', $user, $passwd))
mssql_connect:       连上数据库。
mssql_data_seek:     移动列指针。
mssql_fetch_array:   返回数组资料。
mssql_fetch_field:   取得字段信息。
mssql_fetch_object:  返回类资料。
mssql_fetch_row:     返回单列的各字段。
mssql_field_seek:    配置指针到返回值的某字段。
mssql_free_result:   释放返回占用内存。
mssql_num_fields:    取得返回字段的数目。
mssql_num_rows:      取得返回列的数目。
mssql_pconnect:      打开 MS SQL 服务器持续连接。
mssql_query:         送出一个 query 字符串。
mssql_result:        取得查询 (query) 的结果。
mssql_select_db:     选择一个数据库。
mt_getrandmax:       随机数的最大值。
mt_rand:             取得随机数值。
mt_srand:            配置随机数种子。
mysql_affected_rows: 得到 MySQL 最后操作影响的列数目。
mysql_close:         关闭 MySQL 服务器连接。
mysql_connect:       打开 MySQL 服务器连接。    参数：('localhost', $user, $passwd)
mysql_create_db:     建立一个 MySQL 新数据库。
mysql_data_seek:     移动内部返回指针。
mysql_db_query:      送查询字符串 (query) 到 MySQL 数据库。
mysql_drop_db:       移除数据库。
mysql_errno():       返回错误信息代码。
mysql_error:         返回错误信息。
mysql_fetch_array:   返回数组资料。   参数：(mysql_query($query语句), MYSQL_ASSOC))
mysql_fetch_field:   取得字段信息。
mysql_fetch_lengths: 返回单列各栏资料最大长度。
mysql_fetch_object:  返回类资料。
mysql_fetch_row:     返回单列的各字段。
mysql_field_flags:   获得目前字段的标志。
mysql_field_len:     获得目前字段的长度。
mysql_field_name:    返回指定字段的名称。
mysql_field_seek:    配置指针到返回值的某字段。
mysql_field_table:   获得目前字段的资料表 (table) 名称。
mysql_field_type:    获得目前字段的类型。
mysql_free_result:   释放返回占用内存。
mysql_insert_id:     返回最后一次使用 INSERT 指令的 ID。
mysql_list_dbs:      列出 MySQL 服务器可用的数据库 (database)。
mysql_list_fields:   列出指定资料表的字段 (field)。
mysql_list_tables:   列出指定数据库的资料表 (table)。
mysql_num_fields:    取得返回字段的数目。
mysql_num_rows:      取得返回列的数目。
mysql_pconnect:      打开 MySQL 服务器持续连接。
mysql_query:         送出一个 query 字符串。
mysql_result:        取得查询 (query) 的结果。
mysql_select_db:     选择一个数据库。          参数：($db) //数据库名，String类型
mysql_tablename:     取得资料表名称。
next:                将数组的内部指针向后移动。
nl2br:               将换行字符转成 <br>。
number_format:       格式化数字字符串。
OCIBindByName:          让动态 SQL 可使用 PHP 变量。
OCIColumnIsNULL:        测试返回行是否为空的。
OCIColumnSize:          取得字段类型的大小。
OCICommit:              将 Oracle 的交易处理付诸实行。
OCIDefineByName:        让 SELECT 指令可使用 PHP 变量。
OCIExecute:             执行 Oracle 的指令部分。
OCIFetch:               取得返回资料的一列 (row)。
OCIFetchInto:           取回 Oracle 资料放入数组。
OCILogOff:              关闭与 Oracle 的链接。
OCILogon:               打开与 Oracle 的链接。
OCINewDescriptor:       初始新的 LOB/FILE 描述。
OCINumRows:             取得受影响字段的数目。
OCIParse:               分析 SQL 语法。
OCIResult:              从目前列 (row) 的资料取得一栏 (column)。
OCIRollback:            撤消当前交易。
OctDec:                 八进位转十进位。
odbc_autocommit:        开关自动改动功能。
odbc_binmode:           配置二进位资料处理方式。
odbc_close:             关闭 ODBC 链接。
odbc_close_all:         关闭所有 ODBC 链接。
odbc_commit:            改动 ODBC 数据库。
odbc_connect:           链接至 ODBC 数据库。
odbc_cursor:            取得游标名。
odbc_do:                执行 SQL 指令。
odbc_exec:              执行 SQL 指令。
odbc_execute:           执行预置 SQL 指令。
odbc_fetch_into:        取得返回的指定列。
odbc_fetch_row:         取得返回一列。
odbc_field_len:         取得字段资料长度。
odbc_field_name:        取得字段名称。
odbc_field_type:        取得字段资料类型。
odbc_free_result:       释出返回资料的内存。
odbc_longreadlen:       配置返回栏的最大值。
odbc_num_fields:        取得字段数目。
odbc_num_rows:          取得返回列数目。
odbc_pconnect:          长期链接至 ODBC 数据库。
odbc_prepare:           预置 SQL 指令。
odbc_result:            取得返回资料。
odbc_result_all:        返回 HTML 表格资料。
odbc_rollback:          撤消当前交易。
odbc_setoption:         调整 ODBC 配置。
opendir:                打开目录 handle。
openlog:                打开系统纪录。
Ora_Bind:               链接 PHP 变量到 Oracle 参数。
Ora_Close:              关闭一个 Oracle 的 cursor。
Ora_ColumnName:         得到 Oracle 返回列 (Column) 的名称。
Ora_ColumnSize:         取得字段类型的大小。
Ora_ColumnType:         得到 Oracle 返回列 (Column) 的类型。
Ora_Commit:             将 Oracle 的交易处理付诸实行。
Ora_CommitOff:          关闭自动执行 Oracle 交易改动的功能。
Ora_CommitOn:           打开自动执行 Oracle 交易改动的功能。
Ora_Do:                 快速的 SQL 查询。
Ora_Error:              获得 Oracle 错误信息。
Ora_ErrorCode:          获得 Oracle 错误代码。
Ora_Exec:               执行 Oracle 的指令部分。
Ora_Fetch:              取得返回资料的一列 (row)。
Ora_FetchInto:          取回 Oracle 资料放入数组。
Ora_GetColumn:          从返回列 (row) 的资料取得一栏 (column)。
Ora_Logoff:             结束与 Oracle 的链接。
Ora_Logon:              打开与 Oracle 的链接。
Ora_Numcols:            取得字段的数目。
Ora_Open:               打开 Oracle 的 cursor。
Ora_Parse:              分析 SQL 语法。
Ora_PLogon:             打开与 Oracle 的长期链接。
Ora_Rollback:           撤消当前交易。
Ord:                    返回字符的序数值。
pack:                   压缩资料到位字符串之中。
parse_str:              解析 query 字符串成变量。
parse_url:              解析 URL 字符串。
passthru:               执行外部程序并不加处理输出资料。
pclose:                 关闭文件。
PDF_add_annotation:     加入注释。
PDF_add_outline:        目前页面加入书签。
PDF_arc:                绘弧。
PDF_begin_page:         启始 PDF 文件页面。
PDF_circle:             绘圆。
PDF_clip:               组合所有向量。
PDF_close:              关闭 PDF 文件。
PDF_closepath:               形成封闭的向量形状。
PDF_closepath_fill_stroke:   形成封闭的向量形状沿向量绘线并填满。
PDF_closepath_stroke:        形成封闭的向量形状并沿向量绘线。
PDF_close_image:        关闭图文件。
PDF_continue_text:      输出文字。
PDF_curveto:            绘贝氏曲线。
PDF_endpath:            关闭目前向量。
PDF_end_page:           关闭 PDF 文件页面。
PDF_execute_image:      放置 PDF 文件中图片到指定位置。
PDF_fill:               填满目前的向量。
PDF_fill_stroke:        填满目前的向量并沿向量绘线。
PDF_get_info:           返回文件信息。
PDF_lineto:             绘直线。
PDF_moveto:             配置处理的坐标点。
PDF_open:               建立新的 PDF 文件。
PDF_open_gif:           打开 GIF 图文件。
PDF_open_jpeg:          打开 JPEG 图文件。
PDF_open_memory_image:  打开内存图文件。
PDF_place_image:        放置图片到 PDF 文件指定位置。
PDF_put_image:          放置图片到 PDF 文件。
PDF_rect:               绘长方形。
PDF_restore:            还原环境变量。
PDF_rotate:             旋转类。
PDF_save:               储存环境变量。
PDF_scale:              缩放类。
PDF_setdash:            配置虚线样式。
PDF_setflat:            配置平滑值。
PDF_setgray:            指定绘图的颜色为灰阶并填入。
PDF_setgray_fill:       指定填入的颜色为灰阶。
PDF_setgray_stroke:     指定绘图的颜色为灰阶。
PDF_setlinecap:         配置 linecap 参数。
PDF_setlinejoin:        配置连接参数。
PDF_setlinewidth:       配置线宽。
PDF_setmiterlimit:      配置斜边界限。
PDF_setrgbcolor:        指定绘图的颜色为彩色并填入。
PDF_setrgbcolor_fill:   指定填入的颜色为彩色。
PDF_setrgbcolor_stroke: 指定绘图的颜色为彩色。
PDF_set_char_spacing:   配置字符间距。
PDF_set_duration:       配置二页的切换时间。
PDF_set_font:           配置使用的字型及大小。
PDF_set_horiz_scaling:  配置文字水平间距。
PDF_set_info_author:    配置文件作者。
PDF_set_info_creator:   配置建文件者字符串。
PDF_set_info_keywords:  配置文件的关键字。
PDF_set_info_subject:   配置文件主题。
PDF_set_info_title:     配置文件标题。
PDF_set_leading:        配置行距。
PDF_set_text_matrix:    配置文字矩阵。
PDF_set_text_pos:       配置文字位置。
PDF_set_text_rendering: 配置文字表现方式。
PDF_set_text_rise:      配置文字高度。
PDF_set_transition:     配置页的转换。
PDF_set_word_spacing:   配置字间距。
PDF_show:               输出字符串到 PDF 文件。
PDF_show_xy:            输出字符串到指定坐标。
PDF_stringwidth:        计算字符串的宽度。
PDF_stroke:             沿向量绘线。
PDF_translate:          移动原点。
pfsockopen:             打开网络的 Socket 持续链接。
pg_Close:               关闭 PostgreSQL 服务器连接。
pg_cmdTuples:           取得被 SQL 指令影响的资料笔数。
pg_Connect:             打开 PostgreSQL 服务器连接。
pg_DBname:              取得目前的数据库名称。
pg_ErrorMessage:        返回错误信息。
pg_Exec:                执行 query 指令。
pg_Fetch_Array:         返回数组资料。
pg_Fetch_Object:        返回类资料。
pg_Fetch_Row:           返回单列的各字段。
pg_FieldIsNull:         检查字段是否有资料。
pg_FieldName:           返回指定字段的名称。
pg_FieldNum:            取得指定字段的行数。
pg_FieldPrtLen:         计算可列示的长度。
pg_FieldSize:           计算指定字段的长度。
pg_FieldType:           获得目前字段的类型。
pg_FreeResult:          释放返回占用内存。
pg_GetLastOid:          取得最后的类代码。
pg_Host:                取得连接机器名称。
pg_loclose:             关闭大型类。
pg_locreate:            建立大型类。
pg_loopen:              打开大型类。
pg_loread:              读取大型类。
pg_loreadall:           读取大型类并输出。
pg_lounlink:            删除大型类。
pg_lowrite:             读取大型类。
pg_NumFields:           取得返回字段的数目。
pg_NumRows:             取得返回列的数目。
pg_Options:             取得连接机器选项。
pg_pConnect:            打开 PostgreSQL 服务器持续连接。
pg_Port:                取得连接机器埠号。
pg_Result:              取得查询 (query) 的结果。
pg_tty:                 取得连接机器终端机。
phpinfo():              返回 PHP 所有相关信息。
phpversion:             返回 PHP 版本信息。
pi:                     圆周率。
popen:                  打开文件。
pos:                    返回数组目前的元素。
pow:                    次方。
preg_match:             字符串比对解析。
preg_match_all:         字符串整体比对解析。
preg_replace:           字符串比对解析并取代。
preg_split:             将字符串依指定的规则切开。
prev:                   将数组的内部指针往前移动。
print:                  输出字符串。
printf:                 输出格式化字符串。
putenv:                 配置系统环境变量。
quoted_printable_decode: 将 qp 编码字符串转成 8 位字符串。
QuoteMeta:              加入引用符号。
rand("1111","9999"):    取得随机数值。(产生1111~9999范围内的一个随机数，这范围可自定义)
range:                  建立一个整数范围的数组。
rawurldecode:           从 URL 专用格式字符串还原成普通字符串。
rawurlencode:           将字符串编码成 URL 专用格式。
readdir:                读取目录 handle。
readfile:               输出文件。
readgzfile:             读出压缩文件。
readlink:               返回符号链接 (symbolic link) 目标文件。
recode_file:            记录文件或文件请求到记录中。
recode_string:               记录字符串到记录中。
register_shutdown_function:  定义 PHP 程序执行完成后执行的函数。
rename:                      更改文件名。
reset:                  将数组的指针指到数组第一个元素。
rewind:                 重置开文件的读写位置指针。
rewinddir:              重设目录 handle。
rmdir:                  删除目录。
round:                  四舍五入。
rsort:                  将数组的值由大到小排序。
sem_acquire:            捕获信号。
sem_get:                取得信号代码。
sem_release:            释出信号。
serialize:              储存资料到系统中。
session_decode:         Session 资料解码。
session_destroy:        结束 session。
session_encode:         Session 资料编码。
session_id:             存取目前 session 代号。
session_is_registered:  检查变量是否注册。
session_module_name:    存取目前 session 模块。
session_name:           存取目前 session 名称。
session_register:       注册新的变量。
session_save_path:      存取目前 session 路径。
session_start:          初始 session。
session_unregister:     删除已注册变量。
setcookie:              送出 Cookie 信息到浏览器。
setlocale:              配置地域化信息。
settype:                配置变量类型。
set_file_buffer:        配置文件缓冲区大小。
set_magic_quotes_runtime:   配置 magic_quotes_runtime 值。
set_socket_blocking:    切换搁置与无搁置模式。
set_time_limit:         配置该页最久执行时间。
shm_attach:             打开建立共享内存空间。
shm_detach:             中止共享内存空间链接。
shm_get_var:            取得内存空间中指定的变量。
shm_put_var:            加入或更新内存空间中的变量。
shm_remove:             清除内存空间。
shm_remove_var:         删除内存空间中指定的变量。
shuffle:                将数组的顺序弄混。
similar_text:           计算字符串相似度。
Sin:                    正弦计算。
sizeof:                 获知数组的大小。
sleep:                  暂停执行。
snmpget:                取得指定类识别码。
snmpwalk:               取得所有类。
snmpwalkoid:            取得网络本体树状信息。
snmp_get_quick_print:   取得 UCD 函数库中的 quick_print 值。
snmp_set_quick_print:   配置 UCD 函数库中的 quick_print 值。
solid_close:            关闭 solid 链接。
solid_connect:          链接至 solid 数据库。
solid_exec:             执行 SQL 指令。
solid_fetchrow:         取得返回一列。
solid_fieldname:        取得字段名称。
solid_fieldnum:         取得字段数目。
solid_freeresult:       释出返回资料的内存。
solid_numfields:        取得字段数目。
solid_numrows:          取得返回列数目。
solid_result:           取得返回资料。
sort:                   将数组排序。
soundex:                计算字符串的读音值
split:                  将字符串依指定的规则切开。
sprintf:                将字符串格式化。
sql_regcase:            将字符串逐字返回大小写字符。
Sqrt:                   开平方根。
srand:                  配置随机数种子。
stat:                   取得文件相关信息。
strchr:                 寻找第一个出现的字符。
strcmp:                 字符串比较。
strcspn:                不同字符串的长度。
strftime:               将服务器的时间本地格式化。
StripSlashes:           去掉反斜线字符。
strip_tags:             去掉 HTML 及 PHP 的标记。
strlen:                 取得字符串长度。
strpos:                 寻找字符串中某字符最先出现处。
strrchr:                取得某字符最后出现处起的字符串。
strrev:                 颠倒字符串。
strrpos:                寻找字符串中某字符最后出现处。
strspn:                 找出某字符串落在另一字符串遮罩的数目。
strstr:                 返回字符串中某字符串开始处至结束的字符串。
strtok:                 切开字符串。
strtolower:             字符串全转为小写。
strtoupper:             字符串全转为大写。
strtr:                  转换某些字符。
strval:                 将变量转成字符串类型。
str_replace:            字符串取代。
substr:                 取部份字符串。
sybase_affected_rows:   取得最后 query 影响的列数。
sybase_close:           关闭与数据库的连接。
sybase_connect:         连上数据库。
sybase_data_seek:       移动列指针。
sybase_fetch_array:     返回数组资料。
sybase_fetch_field:     取得字段信息。
sybase_fetch_object:    返回类资料。
sybase_fetch_row:       返回单列的各字段。
sybase_field_seek:      配置指针到返回值的某字段。
sybase_free_result:     释放返回占用内存。
sybase_num_fields:      取得返回字段的数目。
sybase_num_rows:        取得返回列的数目。
sybase_pconnect:        打开服务器持续连接。
sybase_query:           送出一个 query 字符串。
sybase_result:          取得查询 (query) 的结果。
sybase_select_db:       选择一个数据库。
symlink:                建立符号链接 (symbolic link)。
syslog:                 纪录至系统纪录。
system:                 执行外部程序并显示输出资料。
Tan:                    正切计算。
tempnam:                建立只一的临时文件。
time():                 取得目前时间的 UNIX 时间戳记。
touch:                  配置最后修改时间。
trim:                   截去字符串首尾的空格。
uasort:                 将数组依使用者自定的函数排序。
ucfirst:                将字符串第一个字符改大写。
ucwords:                将字符串每个字第一个字母改大写。
uksort:                 将数组的索引依使用者自定的函数排序。
umask:                  改变目前的文件属性遮罩 umask。
uniqid:                 产生只一的值。
unlink:                 删除文件。
unpack:                 解压缩位字符串资料。
unserialize:            取出系统资料。
unset:                  删除变量。
urldecode:              还原 URL 编码字符串。
urlencode:              将字符串以 URL 编码。
usleep:                 暂停执行。
usort:                  将数组的值依使用者自定的函数排序。
utf8_decode:            将 UTF-8 码转成 ISO-8859-1 码。
utf8_encode:            将 ISO-8859-1 码转成 UTF-8 码。
virtual:                完成apache服务器的子请求 (sub-request)。
vm_addalias:            加入新别名。
vm_adduser:             加入新使用者。
vm_delalias:            删除别名。
vm_deluser:             删除使用者。
vm_passwd:              改变使用者密码。
wddx_add_vars:                           将 WDDX 封包连续化。
wddx_deserialize:                        将 WDDX 封包解连续化。
wddx_packet_end:                         结束的 WDDX 封包。
wddx_packet_start:                       开始新的 WDDX 封包。
wddx_serialize_value:                    将单一值连续化。
wddx_serialize_vars:                     将多值连续化。
xml_error_string:                        取得 XML 错误字符串。
xml_get_current_byte_index:              取得目前解析为第几个位组。
xml_get_current_column_number:           获知目前解析的第几字段。
xml_get_current_line_number:             取得目前解析的行号。
xml_get_error_code:                      取得 XML 错误码。
xml_parse:                               解析 XML 文件。
xml_parser_create:                       初始 XML 解析器。
xml_parser_free:                         释放解析占用的内存。
xml_parser_get_option:                   取得解析使用的选项。
xml_parser_set_option:                   配置解析使用的选项。
xml_set_character_data_handler:          建立字符资料标头。
xml_set_default_handler:                 建立默认标头。
xml_set_element_handler:                 配置元素的标头。
xml_set_external_entity_ref_handler:     配置外部实体参引的标头。
xml_set_notation_decl_handler:           配置记法宣告的标头。
xml_set_object:                          使 XML 解析器用类。
xml_set_processing_instruction_handler:  建立处理指令标头。
xml_set_unparsed_entity_decl_handler:    配置未解析实体宣告的标头。
yp_errno:                                取得先前 YP 操作的错误码。
yp_err_string:                           取得先前 YP 操作的错误字符串。
yp_first:                                返回 map 上第一笔符合的资料。
yp_get_default_domain:                   取得机器的 Domain。
yp_master:                               取得 NIS 的 Master。
yp_match:                                取得指定资料。
yp_next:                                 指定 map 的下笔资料。
yp_order:                                返回 map 的序数。






