﻿/**
 * <P> Title: JavaScript Util                                    </P>
 * <P> Description: JavaScript 工具                              </P>
 * <P> Copyright: Copyright (c) 2011/04/06                       </P>
 * <P> Company:Everunion Tech. Ltd.                              </P>
 * @author 冯万里
 * @version 0.1
 *
 * 为减少 js 关键词的占用,此文件只占用“cm”一个关键词; 使用时用: cm.函数名(参数列表);
 * 如果名称有冲突，请将此文件的 cm 都替换成想要的名称。这是此工具类别唯一占用的关键词
 * 字符串操作函数,直接增加到 String 类别里面,便于直接使用
 */


/**
 * 获取元素
 * @param element 元素的ID或者name
 * @param dom 需要选择的DOM对象,默认是 window.document
 * @return 如果能找到指定名称的元素，返回元素对象
 *         如果找不到指定名称的元素，返回 false
 * @example cm("mytext") 返回: id或者name为"mytext"的对象
 */
var cm = window.cm = function ( element, dom )
{
    // 如果没有参数 ，则返回 本对象
    if ( arguments.length === 0 )
        return cm;
    // 如果本身是类别,直接返回
    if ( "object" === typeof(element) )
    {
        return element;
    }
    // 根据 string 来获取对象
    else if ( "string" === typeof(element) || "number" === typeof(element) )
    {
        try
        {
            dom = dom || window.document;
            // 根据 id 获取元素
            var get_element = dom.getElementById(element);
            if ( get_element )
            {
                return get_element;
            }
            // 根据 name 获取元素
            get_element = dom.getElementsByName(element);
            if ( get_element && get_element.length )
            {
                // 只有一个的时候，直接返回；多个则返回数组
                if ( get_element.length === 1 )
                    return get_element[0];
                else
                    return get_element;
            }
            // 根据 TagName 获取元素
            get_element = dom.getElementsByTagName(element);
            if ( get_element && get_element.length )
            {
                // 只有一个的时候，直接返回；多个则返回数组
                if ( get_element.length === 1 )
                    return get_element[0];
                else
                    return get_element;
            }
            // 以上都找不到对象，则返回 false
            return false;
        }
        //如果取元素时发生异常
        catch ( e )
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


/**
 * 这是错误调试程序
 * 当页面发生错误时，提示错误讯息；仅测试环境里会提示，正式环境下不提示错误。
 * @param msg   出错讯息
 * @param url   出错档案的地址
 * @param sLine 发生错误的行
 * @return true 返回true,会消去 IE下那个恼人的“网页上有错误”的提示
 */
window.onerror = function ( msg, url, sLine )
{
    var errorMsg = "There was an error on this page.\n\n";
    errorMsg += "Error: " + msg + "\n";
    errorMsg += "URL: " + url + "\n";
    errorMsg += "Line: " + sLine + "\n\n";
    errorMsg += "Click OK to continue.\n\n";
    // 判断网址,测试时可以提示出错信息；正式发布时不提示
    if ( url.indexOf("http://localhost") === 0 || url.indexOf("http://127.0.0.1") === 0 ||
        url.indexOf("http://192.168.") === 0 || url.indexOf("file:///") === 0 )
    {
        window.alert( errorMsg );
    }
    // 返回true,会消去 IE下那个恼人的“网页上有错误”的提示
    return true;
}


/**
 * 当页面上按下 Enter 时执行指定的 doEnter() 函数
 * @param event firefox时用以接收事件
 */
window.document.onkeydown = function ( event )
{
    // 为兼顾 IE 和 FireFox
    event = event || window.event;
    // 如果取不到页面事件
    if ( !event ) return;
    // 获取页面上的按键码
    var currentKey = event.charCode || event.keyCode;
    // 获取事件源
    var eventSource = event.target || event.srcElement;
    // 多行输入框,允许输入换行符
    if ( "TEXTAREA" === eventSource.tagName ) return;
    // 如果是 Enter 键, 执行指定程序
    if ( 13 === currentKey && doEnter )
    {
        // 执行指定程序
        try
        {
            // 这里约定页面的回车触发 doEnter() 函数
            doEnter();
        }
        catch (e)
        {
        }
        event.returnValue = false;
    }
}



// ******************************************************** //
// ******************** String start ********************** //

/**
 * 获取字符长度
 * @param chsLength 一个中文占多少个字符，默认为2个 (改数据库时需改这里)
 * @return 字符长度，非拉丁文(如中文)全部计算为3位
 */
String.prototype.chsLeng = function(chsLength)
{
    chsLength = (parseInt(chsLength) >= 0) ? parseInt(chsLength) : 2;
    //去除中文的长度
    var noChsLength = str.replace(/[^\x00-\xff]/g, "").length;
    //中文长度
    var chineseLength = (str.length - noChsLength) * chsLength;
    return noChsLength + chineseLength;
}


/**
 * 全部替换字符串中的指定内容
 * @param regexp 把字符串里的regexp内容替换成newSubStr
 * @param newSubStr 把字符串里的regexp内容替换成newSubStr
 * @return 替换后的字符串。注意：当regexp为空时，字符串的每个字前面都会加上newSubStr
 */
String.prototype.replaceAll = function(regexp, newSubStr)
{
    //如果regexp为空
    if ( !regexp )
        regexp = "";
    //如果newSubStr为空
    if ( !newSubStr )
        newSubStr = "";
    //gm: g = global, m = multiLine
    var raRegExp = new RegExp("" + regexp, "gm");
    return this.replace(raRegExp, "" + newSubStr);
}


/**
 * 字符串比较，忽略大小写，忽略类型
 * 使用时建议将比较的字符串值放前面，需比较的内容放后面。如： "true".is(value)
 * @param data 需比较的数据
 * @return 如果字符串相同，返回true；否则返回false
 */
String.prototype.is = function(data)
{
    return ( this.toLowerCase() === ("" + data).toLowerCase() );
}


/**
 * 去除字符串的前后空格
 * @param direct 去除前空格、后空格还是前后都去除；
 *        direct 为 -1 或 Left，去除前空格；为 1 或 Right 去除后空格；其余情况前后空格都去除
 * @return 去除对应空格的字符串
 */
String.prototype.trim = function(direct)
{
    //去除前空格，注：IE不认为中文全角的空格为空白
    if ( "left".is(direct) || "-1".is(direct) )
        return this.replace(/(^(\s*　*)*)|/g, "");
    //去除后空格
    if ( "right".is(direct) || "1".is(direct) )
        return this.replace(/((\s*　*)*$)/g, "");
    //除上面两种情况外，都是去除前后空格
    return this.replace(/(^(\s*　*)*)|((\s*　*)*$)/g, "");
}


/**
 * 检查字符串是否为中文，是则返回true，否则返回false
 * @param isAllChinese 是否要求全部都为中文
 * @return 不填 isAllChinese，或者 isAllChinese 为 false 时，只要包含有中文即返回true，不包含一个中文则返回false
 * @return 当 isAllChinese 为 true 时，要求字符串全部都为中文则返回 true，如果有一个不为中文则返回false
 */
String.prototype.isChinese = function(isAllChinese)
{
    // 如果字符串为空，则返回true
    if ( "" === this )
        return true;
    var clearChineseLength = this.replace(/([\u4E00-\u9FA5]|[\uFE30-\uFFA0])/g, "").length;
    // 要求全部都为中文
    if ( true === isAllChinese )
        return ( 0 === clearChineseLength );
    else
        return ( this.length !== clearChineseLength );
}


/**
 * 转换字符串成 Java 的 Unicode 编码
 * @param needChangeChinese 是否需要转换中文，为true则转换，否则不转换
 * @return 转换后的字符串
 * @example "哈哈".toUnicode(true) 返回: \u54C8\u54C8
 */
String.prototype.toUnicode = function(needChangeChinese)
{
    var retValue = "";
    //逐字转换
    for ( var i = 0; i < this.length; i++ )
    {
        var tem = this.charAt(i);
        var StrLength = escape(tem).length;
        //如果是中文
        if ( StrLength >= 6 )
        {
            // 需要转换中文
            if ( true === needChangeChinese )
                retValue += escape(tem).replace("%", "\\");
            // 不转换中文
            else
                retValue += tem;
        }
        //如果是符号。注，不会编码的字符： @ * / +
        else if ( StrLength > 1 )
        {
            var repaceStr = "\\u";
            //补上0
            for ( var j = 0; j < 5 - StrLength; j++ )
            {
                repaceStr += "0";
            }
            retValue += escape(tem).replace("%", repaceStr);
        }
        //如果是字母
        else
            retValue += tem;
    }
    return retValue;
}


/**
 * 转换字符串成 Html 页面上显示的编码
 * @return 转换后的字符串
 * @example " ".toHtmlCode() 返回: &nbsp;
 */
String.prototype.toHtmlCode = function ()
{
    var html = this;
    // 以下逐一转换
    html = html.replaceAll("&", "&amp;");
    html = html.replaceAll("%", "&#37;");
    html = html.replaceAll("<", "&lt;");
    html = html.replaceAll(">", "&gt;");
    html = html.replaceAll("\n", "\n<br/>");
    html = html.replaceAll("\"", "&quot;");
    html = html.replaceAll(" ", "&nbsp;");
    html = html.replaceAll("'", "&#39;");
    html = html.replaceAll("[+]", "&#43;");
    return html;
}


/**
 * 转换字符串由 Html 页面上显示的编码变回正常编码(以上面的方法对应)
 * @return 转换后的字符串
 * @example " ".toHtmlCode() 返回: &nbsp;
 */
String.prototype.toTextCode = function ()
{
    var sour = this;
    // 以下逐一转换
    // 先转换百分号
    sour = sour.replaceAll("&#37;", "%");
    // 小于号,有三种写法
    sour = sour.replaceAll("&lt;", "<");
    sour = sour.replaceAll("&LT;", "<");
    sour = sour.replaceAll("&#60;", "<");
    // 大于号,有三种写法
    sour = sour.replaceAll("&gt;", ">");
    sour = sour.replaceAll("&GT;", ">");
    sour = sour.replaceAll("&#62;", ">");
    // 单引号
    sour = sour.replaceAll("&#39;", "'");
    sour = sour.replaceAll("&#43;", "+");
    // 转换换行符号
    sour = sour.replaceAll("\n?<[Bb][Rr]\\S*/?>\n?", "\n");
    // 双引号号,有三种写法
    sour = sour.replaceAll("&quot;", "\"");
    sour = sour.replaceAll("&QUOT;", "\"");
    sour = sour.replaceAll("&#34;", "\"");
    // 空格,只有两种写法, &NBSP; 浏览器不承认
    sour = sour.replaceAll("&nbsp;", " ");
    sour = sour.replaceAll("&#160;", " ");
    // & 符号,最后才转换
    sour = sour.replaceAll("&amp;", "&");
    sour = sour.replaceAll("&AMP;", "&");
    sour = sour.replaceAll("&#38;", "&");
    return sour;
}

// ********************* String end *********************** //
// ******************************************************** //



// ******************************************************** //
// ******************** Ajax start ************************ //

/**
 * XMLHttpRequest
 * 注意：使用这个属性，得保证上次的请求已经返回，否则会影响上一次的请求
 * 建议在回调方法里接收 xmlHttp 参数，保证短时间内多次发送请求而不受影响
 */
cm.xmlHttp = null;

/**
 * 创建 XMLHttpRequest
 * @return XMLHttpRequest
 */
cm.createXMLHttpRequest = function()
{
    var request = false;
    //如果是 firefox 等浏览器
    if ( window.XMLHttpRequest )
    {
        // 调用XMLHttpRequest
        request = new XMLHttpRequest();
        if ( request.overrideMimeType )
        {
            //设置类型
            request.overrideMimeType('text/xml');
        }
    }
    //如果是 IE
    else if ( window.ActiveXObject )
    {
        //IE 的各个版本
        var versions = ['Microsoft.XMLHTTP', 'MSXML.XMLHTTP','Msxml2.XMLHTTP.7.0',
                        'Msxml2.XMLHTTP.6.0', 'Msxml2.XMLHTTP.5.0',
                        'Msxml2.XMLHTTP.4.0','MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP'];
        //由新至旧，尝试各版本
        for ( var i = 0; i < versions.length; i++ )
        {
            try
            {
                //调用ActiveXObject
                request = new ActiveXObject(versions[i]);
                // 如果能获取值
                if ( request )
                    return request;
            }
            catch (e) {}
        }
    }
    //设定 cm.xmlHttp
    cm.xmlHttp = request;
    return request;
}


/**
 * 发送 Ajax 请求
 *
 * cm.Ajax({
 *    url : "submit.html",                         // 需要发送的地址(必须项)
 *    param : "a=1&b=2",                           // 需要发送的传参字符串
 *    async : true,                                // 异步或者同步请求(默认: true, 异步请求)。如果需要发送同步请求，请将此选项设置为 false。
 *    method : "POST",                             // 请求方式(默认: "POST"),也可用"GET"
 *    beforsend : function(){....},                // 发送请求前的动作(此参数也可以是要执行的字符串)
 *    aftersend : function(){....},                // 发送请求后的动作(此参数也可以是要执行的字符串)
 *    success : function(xmlHttp){....},           // 请求成功返回的动作
 *    error : function(xmlHttp, status){....},     // 请求失败时的动作
 *    complete : function(xmlHttp, status){....}   // 请求返回后的动作(不管成败,且在 success 和 error 之后运行)
 * });
 */
cm.Ajax = function ( paramObj )
{
    //如果没有参数类，不再执行
    if ( !paramObj ) return;

    // 执行某函数,参数可能是个函数，也可能是个字符串
    var runFunction = function(fun)
    {
        // 没有参数，则不执行
        if ( !fun ) return;
        try
        {
            // 传的是函数,直接执行
            if ( "function" === typeof(fun) )
            {
                fun();
            }
            // 传的是字符串,eval执行
            else if ( "string" === typeof(fun) )
            {
                eval(fun);
            }
        }
        catch (e) {}
    };
    // 发送请求前的动作
    if ( paramObj.beforsend )
        runFunction(paramObj.beforsend);

    //如果没有发送地址，则下面的都不用执行
    if ( !paramObj.url && '' !== paramObj.url )
        return;

	//创建 XMLHttpRequest
    var xmlHttp = cm.createXMLHttpRequest();
    //如果不支缓 Ajax，提示信息
    if ( !xmlHttp )
    {
        alert( "您的浏览器不支持 Ajax，部分功能无法使用！" );
        return;
    }

    //异步或者同步请求(默认: true, 异步请求)
    paramObj.async = paramObj.async || true;
    //发送地址,加上时间戳,避免浏览器缓存
    paramObj.url = cm.addTimeStamp(paramObj.url);
    //请求方式(默认: "POST")
    paramObj.method = paramObj.method || "POST";
    //get形式，将参数放到URL上
    if ( "GET".is(paramObj.method) && paramObj.param )
    {
        paramObj.url += "&" + paramObj.param;
        paramObj.param = null;
    }
    //发送请求
    xmlHttp.open(paramObj.method, paramObj.url, paramObj.async);
    //执行回调方法
    xmlHttp.onreadystatechange = function()
    {
        // XMLHttpRequest对象响应内容解析完成
        if ( 4 !== xmlHttp.readyState )
            return;
        // 状态正常时
        if ( 200 === xmlHttp.status )
        {
            // 请求成功时的动作
            if ( paramObj.success )
                paramObj.success(xmlHttp);
        }
        else
        {
            // 请求失败时的动作
            if ( paramObj.error )
                paramObj.error(xmlHttp, xmlHttp.status);
        }
        // 请求返回后的动作(不管成败,且在 success 和 error 之后运行)
        if ( paramObj.complete )
            paramObj.complete(xmlHttp, xmlHttp.status);
    };
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;");
    //发送参数
    xmlHttp.send(paramObj.param);

    // 发送请求后的动作
    if ( paramObj.aftersend )
        runFunction(paramObj.aftersend);
}


/**
 * 获取xmlHttp里符合的资料
 * @param  xmlHttp XMLHttpRequest
 * @param  tagName 资料的 TagName
 * @param  init    默认值
 * @param  index   第几个子元素
 * @return 符合的数据的字符串
 */
cm.getAjaxValue = function ( xmlHttp, tagName, init, index )
{
    var value = "";
    var i = 0;
    //如果指定默认值
	if ( init || "0" === "" + init || "false" === "" + init )
        value = "" + init;
    //如果没有 xmlHttp，取属性的
    if ( !xmlHttp )
        xmlHttp = cm.xmlHttp;
    //如果还是没有 xmlHttp，没法继续执行
    if ( !xmlHttp )
        return value;
    //如果没有 tagName ，没法继续执行
    if ( !tagName )
        return value;
    //如果指定第几个子元素
    if ( index )
        i = index;

    try
    {
        //获取xmlHttp里对应的值
        var element1 = xmlHttp.responseXML.getElementsByTagName(tagName)[i].firstChild;
        value = element1.nodeValue;
        //如果能获取值
        if ( value )
            return value;
        return element1.data;
    }
    catch ( e )
    {
        //发生异常时，如果有默认值，返回默认值
        if ( init )
            return init;
        //如果没有默认值，且 value 为空，则返回""
        if ( !value )
            return "";
        //如果 value 能获取值
        return value;
    }
}


// ********************* Ajax end *********************** //
// ******************************************************** //



/**
 * 将对象转换成字符串
 * @param strValue 包含字符串的对象
 * @return 对象的字符串
 */
cm.toStr = function (strValue)
{
    if ( strValue || 0 === strValue || false === strValue )
    {
        return ("" + strValue);
    }
    //返回
    return "";
}


/**
 * 转成 int 类型
 * @param str 需转换的字符串
 * @return int 数值,转换不成功则返回0
 */
cm.toInt = function(str)
{
    return (parseInt(str, 10) || 0 );
}


/**
 * 转成 float 类型
 * @param str 需转换的字符串
 * @return float 数值,转换不成功则返回0
 */
cm.toFloat = function(str)
{
    return (parseFloat(str) || 0 );
}


/**
 * 把数值转化成指定的数值字符串,指定小数位,可加逗号
 * @param value 需转化的数值
 * @param decimal 小数位。不指定则有小数的预设两位,没小数的不加小数(舍去部分会四舍五入)
 * @param signNumber 数字每隔多少位加一个逗号，预设不加逗号
 * @return String 处理后的数值字符串
 */
cm.toNumberStr = function ( value, decimal, signNumber )
{
    var retValue = "";
    //如果没有value值,赋值为0
    value = value || "0";
    //如果不是数值
    if ( false === cm.isNumber(value) )
        value = "0";
    //去除前后空格
    value = ("" + value).trim();
    //去除数值中的逗号
    value = value.replaceAll(",");
    //去除非数值，以及0开头的内容
    value = "" + parseFloat(value);

    //分解字符串； number[0]为整数部分， number[1]为小数部分
    var number = value.split(".");
    //如果字符串是个整数
    if ( 1 === number.length )
        number[1] = "";
    number[2] = "";

    //如果是负数
    if ( 0 === value.indexOf("-") )
    {
        number[0] = number[0].replace("-", "");
        retValue += "-";
    }
    //如果是加号开头
    if ( 0 === value.indexOf("+") )
    {
        number[0] = number[0].replace("+", "");
        retValue += "+";
    }

    //如果没有设定小数字，看数值情况，有小数的预设两位，没小数的不加小数
    if ( !decimal && 0 !== decimal && value.indexOf(".") > 0 )
        decimal = 2;
    decimal = parseInt(decimal);
    //如果是科学计数法的数字
    if ( number[1].indexOf("e") > 0 )
    {
        var numberArry = number[1].split("e");
        number[1] = numberArry[0];
        number[2] = "e" + numberArry[1];
        decimal -= parseInt(numberArry[1]);
    }
    //如果指定保留多少位小数
    if ( 0 < decimal )
    {
        //给结果补足要求保留的小数字
        for ( var i = 0; i < decimal; i++ )
        {
            number[1] += "0";
        }
        //四舍五入
        var tem_number = retValue + number[0] + number[1].substring(0, decimal) + "." + number[1].substring(decimal, number[1].length);
        tem_number = "" + Math.round(tem_number);
        var intLength = (retValue + number[0]).length;
        number[1] = tem_number.substring(intLength, intLength + decimal);
    }

    signNumber = parseInt(signNumber) || 0;
    //如果需要分隔数字
    if ( 0 < signNumber )
    {
        //整数的第一部分的长度
        var tem = number[0].length % signNumber;
        //如果整数部分的长度刚好是signNumber的倍数
        if ( 0 === tem )
            tem = signNumber;
        //整数的第一部分
        retValue += number[0].substring(0, tem );
        //整数的其他部分
        for ( var i = 1; i < parseInt((number[0].length + signNumber - 1) / signNumber); i++ )
        {
            var j = (i - 1) * signNumber + tem;
            retValue += "," + number[0].substring( j, j + signNumber );
        }
    }
    //如果不需要分隔数字
    else
    {
        retValue += number[0];
    }

    // 如果有小数则加上
    if ( (number[1] + number[2]).length > 0 )
    {
        retValue += "." + number[1] + number[2];
    }

    // 返回数值字符串
    return retValue;
}


/**
 * 检查窗体是否可送出
 * @param formField 待检查的窗体
 * @return boolean 检查是否通过
 */
cm.checkForm = function (formField)
{
    //检查窗体的所有元素
    for ( var index=0; index < formField.elements.length; index++ )
    {
       if ( false === cm.checkElement(formField.elements[index]) )
       {
           return false;
       }
    }
    //检查通过
    return true;
}


/**
 * 检查一个元素
 * @param element 需检查的元素
 * @retrn boolean 检查通过返回true,否则返回false
 * @example <input type="text" checkType="R{请输入人数}F{max:999.99, min:0, dec:2, msg:'请输入正确的金额'}" />
 * 说明: 检查类型的关键词需紧跟大括号,不跟大括号则提示“M”的讯息或者默认的讯息;
 * 使用“C”关键词则是选择提示框,点选“确定”可忽略此提示
 * 大括号里面的 msg 属性是对应的提示讯息,需用引号括起来; 大括号里面没有冒号则认为全是提示讯息; 注:不能嵌套大括号
 */
cm.checkElement = function (element)
{
    //防呆
    if ( !element )
        return true;

    //所有需检查讯息
    var message = cm.toStr(element.getAttribute("checkType")).trim();
    //获取需检查的类型,去除大括号里面的内容
    var checkType = message.replace(/({[^}]*})*/g, "").toUpperCase();
    var value = element.value;

    //验证类型
    //必须输入 (用法: checkType="R{请输入名称}" 或者 checkType="R{msg:'请输入名称'}" 或者 checkType="M{请输入名称}R" )
    var R_pos = (checkType.indexOf('R') > -1 );

    //整型 (用法: checkType="I{max:100, min:-50, name:'人数'}" 或者 checkType="I{min:-50,max:100,msg:'请输入正确的人数'}" )
    // 注:有 name 属性时, msg 属性将不会生效; name属性是提示讯息的名称; 属性 max, min 分别表示最大值最小值(含)
    var I_pos = (checkType.indexOf('I') > -1 );

    //浮点型 (用法: checkType="F{max:999.99, min:0, dec:2, name:'金额'}" 或者 checkType="F{dec:2,max:999.99,msg:'请输入正确的金额'}" )
    // 注:有 name 属性时, msg 属性将不会生效; name属性是提示讯息的名称; dec 属性表示小数字数限制; 属性 max, min 分别表示最大值最小值(含)
    var F_pos = (checkType.indexOf('F') > -1 );

    //验证字母数字 (用法: checkType="E{ID只能是字母和数字}" 或者 checkType="E{msg:'ID只能是字母和数字'}")
    var E_pos = (checkType.indexOf("E") > -1 );

    //验证字母数字下划线 (用法: checkType="N{ID只能是字母,数字和下划线}" )
    var N_pos = (checkType.indexOf("N") > -1 );

    //验证输入长度 (用法: checkType="L{len:100, msg:'名称长度不能超过100'}" )
    var L_pos = (checkType.indexOf("L") > -1 );

    //验证 email (用法: checkType="@{请输入正确的电子邮件}" )
    var EMail_pos = (checkType.indexOf("@") > -1 );

    //确认提示框,以提示框的形式提示出来,并可以选择是否忽略此提示
    var C_pos = (checkType.indexOf('C') > -1 );

    // 共享的提示讯息,可以多个验证都提示此一个(用法: M{提示讯息})
    var M_pos = (checkType.indexOf('M') > -1 );


    /**
     * 获取所需检查的类型的类别(匿名函数,仅供此函数内部使用)
     * @param key 对应的键
     * @return Object 所需检查的类型的类别
     */
    var getCheckTypeObj = function(key)
    {
        var upMess = message.toUpperCase();
        // 如果此关键词没有跟大括号,返回空类别
        if ( upMess.indexOf(key + "{") < 0 )
            return {};
        // 取出对应的大括号的讯息
        var key_mess = message.substring(upMess.indexOf(key)+1);
        key_mess = key_mess.substring(0, key_mess.indexOf('}') + 1);
        try
        {
            // 将大括号的讯息转成的类别
            eval("var tem_obj = " + key_mess);
            return tem_obj;
        }
        catch (e)
        {
            // 如果大括号里面的讯息不能转成类别,则认为它全是提示讯息
            return {msg: key_mess.substring(1, key_mess.length - 1)};
        }
    }

    /**
     * 提示出对应的讯息(匿名函数,仅供此函数内部使用)
     * @param key 对应的键
     * @param iniMess 默认的提示讯息
     * @return boolean 一般都返回false,仅当显示选择框点选“是”时返回true
     */
    var showDialog = function (key, iniMess)
    {
        // 获取提示讯息
        var alert_message = (getCheckTypeObj(key).msg || getCheckTypeObj("M").msg || iniMess)  + "!";

        cm.setFocus(element);

        //"C"类型用 confirm 选择框
        if ( C_pos )
        {
            return window.confirm(alert_message);
        }
        window.alert(alert_message);
        return false;
    }


    //一定要输入
    if ( R_pos )
    {
        if ( "" === value )
        {
            //提示
            if ( false === showDialog("R", "请输入必要的值") )
            {
                return false;
            }
        }
    }
    //可以不输入时
    else if ( "" === value )
    {
        return true;
    }
    //验证整形
    if ( I_pos )
    {
        var tem_obj = getCheckTypeObj("I");
        if ( false === cm.isInt(element, tem_obj.max, tem_obj.min, tem_obj.name, false) )
        {
            //有name属性时按名称提示,这里不用重复提示; 否则取提示讯息
            if ( tem_obj.name || false === showDialog("I", "请输入整数的值") )
            {
                return false;
            }
        }
    }
    //验证浮点型
    if ( F_pos )
    {
        var tem_obj = getCheckTypeObj("F");
        if ( false === cm.isNumber(element, tem_obj.max, tem_obj.min, tem_obj.dec, tem_obj.name, false) )
        {
            //有name属性时按名称提示,这里不用重复提示; 否则取提示讯息
            if ( tem_obj.name || false === showDialog("F", "请输入正确的数值") )
            {
                return false;
            }
        }
    }
    //验证字母数字组合
    if ( E_pos )
    {
         if ( false === cm.isLetterNumber(value) )
         {
             //提示
             if ( false === showDialog("E") )
             {
                  return false;
             }
         }
    }
    //验证字母数字下划线组合
    if ( N_pos )
    {
         if ( false === cm.isLetter_Number(value) )
         {
              //提示
              if ( false === showDialog("N") )
              {
                  return false;
              }
         }
    }
    //验证输入长度
    if ( L_pos )
    {
        var tem_obj = getCheckTypeObj("L");
        if ( value.length > tem_obj.len )
        {
            //提示讯息
            if ( false === showDialog("L", "请输入整数的值") )
            {
                return false;
            }
        }
    }
    //email验证
    if ( EMail_pos )
    {
         if ( false === cm.isEmail(value) )
         {
              //提示
              if ( false === showDialog("@", "请输入正确的电子邮件") )
              {
                  return false;
              }
         }
    }
    //检查通过
    return true;
}


/**
 * 整数检查，判断对象是否为数字(包括负数、科学记数法、逗号分隔的数)
 * @param element 需检查的对象,或者是字符串
 * @param max 最大值(含), 预设为 2147483647
 * @param min 最小值(含), 预设为 -2147483648
 * @param name 提示的讯息的名称
 * @param isMust 是否可以为空,true表示不可以为空, 否则可为空
 * @return boolean 验证符合则返回true,否则返回false
 * @example: if( cm.isInt(document.getElementById("amt"), 9999999, -9999999, "人数", flase) )
 */
cm.isInt = function (element, max, min, name, isMust)
{
    // 没有最大值时,预设为整型的最大值
    if ( max !== 0 && !max )
    {
        max = 2147483647;
    }
    // 没有最大值时,预设为整型的最大值
    if ( min !== 0 && !min )
    {
        min = -2147483648;
    }
    return cm.isNumber(element, max, min, 0, name, isMust);
}


/**
 * 数字检查，判断对象是否为数字(包括负数、科学记数法、逗号分隔的数)
 * @param element 需检查的对象,或者是字符串
 * @param max 最大值(含)
 * @param min 最小值(含)
 * @param decimal 小数多少位
 * @param name 提示的讯息的名称
 * @param isMust 是否可以为空,true表示不可以为空, 否则可为空
 * @return boolean 验证符合则返回true,否则返回false
 * @example: if( cm.isNumber(document.getElementById("amt"), 9999999.999, -9999999.999, 3, "金额", flase) )
 */
cm.isNumber = function (element, max, min, decimal, name, isMust)
{
    //出错时: 提示讯息,设定焦点,返回true
    var doError = function( msg )
    {
        // 有 name 属性时提示讯息,没有则不提示
        if ( name )
        {
            cm.setFocus(element);
            window.alert( msg );
        }
        return false;
    }

    //获取字符串
    var str = element.value || "" + element;
    //去除空白; 去除逗号; 转成大写
    str = str.replace(/\s/gi, '').replace(/[,]/gi, '').toUpperCase();

    //不可以为空; 提示讯息: 请输入 name !
    if ( true === isMust && "" === str )
    {
        return doError( unescape("请输入 ") + name + "!" );
    }
    //可以为空
    if ( !isMust && "" === str )
    {
        return true;
    }

    //是否为数值; 提示讯息: name 不是正确的数字!
    if ( !str.match(/^[+-]?\d+([.]?\d*)([eE][+-]\d+)?$/g) )
    {
        return doError( name + unescape(" 不是正确的数字!") );
    }
    // 如果没有指定最大值,不可以超过数值的最大值
    if ( 0 !== max && !max )
        max = Number.MAX_VALUE;
    //判断最大值,提示讯息: name 不能大于 max
    if ( parseFloat(str) > parseFloat(max) )
    {
        return doError( name + unescape(" 不能大于 ") + max );
    }
    // 如果没有指定最小值,不可以小于数值的最小值
    if ( 0 !== min && !min )
        min = -1 * Number.MAX_VALUE;
    //判断最小值,提示讯息: name 不能小于 min
    if ( parseFloat(str) < parseFloat(min) )
    {
        return doError( name + unescape(" 不能小于 ") + min );
    }

    //小数判断
    decimal = parseInt(decimal);
    if ( decimal >= 0 && (str.indexOf(".") > -1 || str.indexOf("E") > -1) )
    {
        //获取小数点后的数字
        var val = str.replace(/^[+-]?\d+[.]/g, '');
        val = val.replace(/0*([E][+-]\d+)?$/g, '');
        //小数长度
        var decimalLength = val.length;

        //如果是科学记算法
        if ( str.indexOf("E") > -1 )
        {
            decimalLength = val.length - parseInt(str.substring(str.indexOf("E") + 1, str.length));
        }
        // 要求整数时; 提示讯息: name 必须是整数
        if ( decimal === 0 && (decimalLength > 0 || (str.indexOf(".") > -1 && str.indexOf("E") == -1)) )
        {
            return doError( name + unescape(" 必须是整数!") );
        }
        //小数位判断; 提示讯息: name 请保留 decimal 位小数
        if ( decimalLength > decimal )
        {
            return doError( name + unescape(" 请保留 ") + decimal + unescape(" 位小数!") );
        }
    }
    //检验通过
    return true;
}


/**
 * 由数字、26个英文字母或者下划线组成的字符串
 * @param value 待验证字符串
 * @return boolean 符合返回true,否则返回false
 */
cm.isLetter_Number = function (value)
{
    return !!(value.match(/^\w+$/g));
}


/**
 * 由数字和26个英文字母组成的字符串
 * @param value 待验证字符串
 * @return boolean 符合返回true,否则返回false
 */
cm.isLetterNumber = function (value)
{
    return !!(value.match(/^[A-Za-z0-9]+$/g));
}


/**
 * 是否为email地址
 * @param value 需检查的字符串
 * @return boolean 符合返回true,否则返回false
 */
cm.isEmail = function (value)
{
    //去除空格
    value = value.replace(/\s/gi, '');
    return !!( value.match(/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/g) );
}


/**
 * 清除form里各元素的值
 * @param form 需操作的Form,默认所有form
 */
cm.clearForm = function (form)
{
    //储存元素
    var elements = [];
    // 获取指定form的元素
    if ( form )
    {
        elements = form.elements;
    }
    // 获取form的所有元素
    else
    {
        var formArray = window.document.forms;
        for ( var i = 0; i < formArray.length; i++ )
        {
            form = formArray[i];
            for ( var j = 0; j < form.length; j++ )
            {
                elements[elements.length] = form[j];
            }
        }
    }

    // 逐个元素处理
    for ( var i = 0; i < elements.length; i++ )
    {
        var type = elements[i].type;
        if ( "button" === type || "submit" === type || "reset" === type || "hidden" === type )
            continue;
        // 选择项
        if ( "checkbox" === type || "radio" === type )
            elements[i].checked = false;
        // 单行输入框, 多行输入框, 下拉选单
        else if ( "text" === type || "textarea" === type || "select-one" === type || "select-multiple" === type )
            elements[i].value = "";
        // 档案上传框
        else if ( "file" === type )
        {
            // firfox 时使用
            elements[i].value = "";
            // IE 时使用
            elements[i].outerHTML = elements[i].outerHTML;
        }
    }
}


/**
 * 清空file的值
 * @param file file元素
 */
cm.clearFileInput = function(file)
{
	// 建立临时form来实现
    var form = window.document.createElement('form');
    window.document.body.appendChild(form);
    // 记住file在旧窗体中的地址
    var pos = file.nextSibling;
    form.appendChild(file);
    form.reset();
    pos.parentNode.insertBefore(file,pos);
    window.document.body.removeChild(form);
}


/**
 * 表单序列化
 * @param form 表单的id,name 或者表单对象
 * @return 表单序列化后的字符串
 */
cm.serialize = function(form)
{
    // 获取表单里的所有对象
    var elements = cm(form).elements;
    // 拼接参数
    var param = "";
    for ( var i = 0; i < elements.length; i++ )
    {
        var type = elements[i].type;
        // 选择项,没选中时跳过
        if ( type == "radio" || type == "checkbox" )
        {
            if ( !elements[i].checked )
            {
                continue;
            }
        }
        // 按钮不需要序列化
        if ( "button" === type || "submit" === type || "reset" === type )
        {
            continue;
        }
        var name = elements[i].name;
        var value = elements[i].value;
        param += name + "=" + value + "&";
    }
    return param.substr(0, param.length - 1);
}


/**
 * 过滤数字
 * @param event 兼容 IE 和 FireFox 的事件
 * @param isFloat 是否允许输入一个小数点
 * @example <input type="text" name="stdcost" onkeydown="return cm.inputNumber(event,true);"/>
 */
cm.inputNumber = function(event, isFloat)
{
    // 兼容 IE 和 FireFox
    event = event || window.event;
    // 获取事件源
    var source = event.target || event.srcElement;
    // 不允许 shift 键
    if (  event.shiftKey === true ) return false;

    var keyCode = event.charCode || event.keyCode;
    // 只允许输入数字、删除、左右键; 小数时可输入一个小数点
    if (  (keyCode >= 48 && keyCode <= 57 ) || (keyCode >= 96 && keyCode <= 105 ) ||
       keyCode === 8  || keyCode === 46 || keyCode === 39 || keyCode === 37 ||
       ( isFloat && (keyCode === 110 || keyCode === 190) && source.value.length>0 && source.value.indexOf(".") == -1 )
    )
    return true;
    return false;
}


/**
 * 过滤字母
 * @param event 兼容 IE 和 FireFox 的事件
 * @example <input type="text" name="stdcost" onkeydown="return cm.inputLetter(event);"/>
 */
cm.inputLetter = function(event)
{
    // 兼容 IE 和 FireFox
    event = event || window.event;
    // 不允许 ctrl 键和 shift 键
    if (  event.ctrlKey === true || event.shiftKey === true ) return false;

    var keyCode = event.charCode || event.keyCode;
    // 只允许输入字母、删除、左右键
    if ( (keyCode >= 65 && keyCode <= 90 ) || keyCode == 8 || keyCode == 37 || keyCode == 39 || keyCode == 46 )
    return true;
    return false;
}


/**
 * 过滤字母数字
 * @param event 兼容 IE 和 FireFox 的事件
 * @example <input type="text" name="stdcost" onkeydown="return cm.inputNumLetter(event);"/>
 */
cm.inputNumLetter = function(event)
{
    // 兼容 IE 和 FireFox
    event = event || window.event;
    // 不允许 ctrl 键和 shift 键
    if (  event.ctrlKey === true || event.shiftKey === true ) return false;

    var keyCode = event.charCode || event.keyCode;
    // 只允许输入字母、数字、删除、左右键
    if ( (keyCode >= 65 && keyCode <= 90) ||
        (keyCode >= 96 && keyCode <= 105) || (keyCode >= 48 && keyCode <= 57) ||
        keyCode == 8 || keyCode == 37 || keyCode == 39 || keyCode == 46
    )
    return true;
    return false;
}


/**
 * 获取元素的值
 * @param name 元素的ID或者name
 * @param init 默认值
 * @return 元素的值(value 或者 innerHTML 或者 checkbox、radio选中的值)
 */
cm.getValue = function (name, init)
{
    //返回值
    var retValue = "";
    //如果没有传入默认值
    if ( arguments.length <= 1 )
        init = "";
    var data = cm.getElement( name );
    //如果没有找到所要的元素，则直接返回默认值
    if ( !data )
        return init;

    //如果它是选择框
    if ( "checkbox" === data.type )
    {
        if ( data.checked )
            return data.value;
        return init;
    }
    //如果它是单选按钮
    if ( "radio" === data.type || (1 < data.length && "radio" === data[0].type) )
        return cm.getRadioValue(name, init);

    //获取元素的值; 没有value属性则取innerHTML
    retValue = data.value || data.innerHTML;
    //如果没有取到值，返回默认值
    return ( retValue || init );
}


/**
 * 获取单选按钮对应的编号和名称
 * @param boxName 单选按钮的名称
 * @param init    单选按钮的默认值
 * @param needID  是否需要单选按钮的编号
 * @return 如果参数needID为false或者没有这参数，返回被选中的当选按钮的value
 *         如果参数needID为true,返回数组: 被选中的value,和对应的第几个单选按钮被选中
 */
cm.getRadioValue = function (boxName, init, needID)
{
    //返回值，预设为 ""
    var retValue = "";
    //单选按钮的编号，默认为 -1
    var boxID = -1;
    //单选按钮的值(value)
    var boxValue = "";
    //如果有默认值
    if ( arguments.length >= 2 )
    {
        retValue = init;
        boxValue = init;
    }

    //如果需要单选按钮的编号
    if ( true === needID )
    {
        //如果需要单选按钮的编号，默认返回值为 ("", -1)
        retValue = new Array(boxValue, boxID);
    }
    var box = cm.getElement( boxName );
    //如果没有找到所要的单选按钮，则直接返回
    if ( !box )
    {
        return retValue;
    }

    //如果只有一个单选按钮
    if ( "radio" === box.type && box.checked )
    {
        boxID = 0;
        boxValue = box.value;
    }
    //循环检查选中哪个
    for ( var i = 0; i < box.length; i++ )
    {
        //如果选中此单选按钮
        if ( box[i].checked )
        {
            boxID = i;
            boxValue = box[i].value;
        }
    }

    retValue = boxValue;
    //如果需要单选按钮的编号
    if ( true === needID )
    {
        retValue = new Array(boxValue, boxID);
    }
    return retValue;
}


/**
 * 设定iframe适应高度
 * @param iframe iframe元素
 */
cm.SetCwinHeight = function (iframe)
{
    //获取 iframe元素
    iframe = iframe || window.document.getElementsByTagName("iframe")[0] || window.parent.document.getElementsByTagName("iframe")[0];

    //主页面时
    if ( iframe )
    {
        //设定高度(IE)
        if ( iframe.Document && iframe.Document.body.scrollHeight )
        {
            iframe.height = iframe.Document.body.scrollHeight;
        }
        //设定高度(Firefox)
        else if ( iframe.contentDocument && iframe.contentDocument.documentElement )
        {
            iframe.height = iframe.contentDocument.documentElement.scrollHeight;
        }
    }
}


/**
 * 设定input标签是否可用
 * @param isDisabled 是否不可用,true为不可用，false为可用
 */
cm.setButtonUse = function(isDisabled)
{
    isDisabled = isDisabled || false;
    //INPUT对象
    var obj = document.getElementsByTagName("INPUT");
    //INPUT对象判断
    for ( var i = 0; i < obj.length; i++ )
    {
        var type = obj[i].type.toUpperCase();
        if ( type == "BUTTON" || type == "SUBMIT" )
        {
            //是否不可编辑
            obj[i].disabled = isDisabled;
        }
    }
}


/*
 * 宣告一个让单个元素不可用
 * @param object 元素对象
 */
cm.setElementDisable = function( object )
{
	var type = object.type;
	// 输入框,可用而不可写
	if ( "text" === type || "textarea" === type )
	{
		objEle.disabled = false;
		objEle.readOnly = true;
	}
	// 选择框,下拉选单,按钮, 不可用
	else if ( "radio" === type || "checkbox" === type || "button" === type || "submit" === type ||
		"reset" === type || "select-one" === type || "select-multiple" === type )
	{
		objEle.disabled = true;
	}
	else if ( "hidden" === type )
	{
		//do nothing
	}
	else if ( "IMG" === object.tagName )
	{
		object.disabled = true;
	}
	// 数组，要递归
	else if ( 1 < object.length )
	{
		for ( var i = 0; i < object.length; i++ )
		{
			cm.setElementDisable(object[i]);
		}
	}
}


cm.overlay = null;
/**
 * 设置是否灰显
 * @param flag (true or false)
 */
cm.SetDisabled = function (flag)
{
    //不存在
    if ( !cm.overlay )
    {
        cm.overlay = document.createElement("div");
        cm.overlay.id = "overlay";
        document.body.appendChild(cm.overlay);
    }
    //为true
    if ( flag )
    {
        var pt = cm.getPageSize();
        cm.overlay.style.height = pt[1];
        cm.overlay.style.width = pt[0];
        cm.overlay.style.display = "";
    }
    //其它
    else
    {
        cm.overlay.style.display = "none";
    }
}


/**
 * 获取窗口大小
 */
cm.getPageSize = function ()
{
    var xScroll, yScroll;
    var windowWidth, windowHeight;
    if ( window.innerHeight && window.scrollMaxY )
    {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    }
    else if ( document.body.scrollHeight > document.body.offsetHeight )
    { // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    }
    else
    { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }
    if ( self.innerHeight )
    {    // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    }
    else if ( document.documentElement && document.documentElement.clientHeight )
    { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    }
    else if ( document.body )
    { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }
    // for small pages with total height less then height of the viewport
    if ( yScroll < windowHeight )
    {
        pageHeight = windowHeight;
    }
    else
    {
        pageHeight = yScroll;
    }
    // for small pages with total width less then width of the viewport
    if ( xScroll < windowWidth )
    {
        pageWidth = windowWidth;
    }
    else
    {
        pageWidth = xScroll;
    }
    return [pageWidth, pageHeight, windowWidth, windowHeight];
}


/**
 * 共享灰显事件
 */
cm.lockWindowForm = function()
{
    /**
     * 还原灰显
     */
    window.onunload = function()
    {
        if ( opener.cm.SetDisabled )
            opener.cm.SetDisabled(false);
    };


    /**
     * 灰显
     */
    window.onload = function()
    {
        if ( opener.cm.SetDisabled )
            opener.cm.SetDisabled(true);
    };
}



/**
 * 设定为焦点
 * @param ob 对象
 */
cm.setFocus = function(element)
{
    try
    {
        //光标位置
        //element.focus();
        setTimeout(function(){element.focus();}, 10);
    }
    //抛出异常
    catch ( e )
    {
    }
}


/**
 * 打开窗口(没有高和宽参数时默认全屏)
 * @param url 窗口地址
 * @param name 窗口名称
 * @param width 窗口宽度
 * @param height 窗口高度
 * @example cm.openWin('../common/Member.html?pform=form1&retid=Memid&retname=MemName', '');
 */
cm.openWin = function(url, name, width, height)
{
    // 默认宽度和高度为全屏
    width = width || window.screen.width;
    height = height || window.screen.height;
    // 让窗口显示在屏幕中间
    var top = (window.screen.height - height)/2;
    var left = (window.screen.width - width)/2;
    name = name || "";
    var param = 'scrollbars=yes,top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',resizable=yes';
    window.open(url, name, param);
}


/**
 * 关闭窗口(IE上的关闭窗口时不提示)
 */
cm.closeWin = function()
{
    window.opener = null; // 关闭IE6不提示
    window.open("","_self"); // 关闭IE7不提示
    //关闭窗口
    window.close();
}


/**
 * 给网址加上时间戳,避免浏览器缓存
 * @param url 网址
 */
cm.addTimeStamp = function(url)
{
    url = cm.toStr(url);
    // 判断网址是否已经有时间戳(时间戳在第一个参数)
    if ( url.indexOf("?timeStamp=") > 0 )
    {
        // 删除旧的时间戳
        url = url.replace(/timeStamp=\d*[&]?/g, "");
    }
    // 判断网址是否已经有时间戳(时间戳不是第一个参数)
    else if ( url.indexOf("&timeStamp=") > 0 )
    {
        // 删除旧的时间戳
        url = url.replace(/[&]timeStamp=\d*/g, "");
    }

    // 给网址增加时间戳参数
    url += (url.indexOf("?") > 0 ) ? "&" : "?";
    url += "timeStamp=" + new Date().getTime();
    return url;
}


/**
 * 添加收藏
 * @param sURL 网站地址
 * @param sTitle 网站名称
 */
cm.AddFavorite = function(sURL, sTitle) {
    //预设值
    sURL = sURL || window.location.href;
    sTitle = sTitle || document.title;
    try { // for IE
        window.external.addFavorite(sURL, sTitle);
    }
    catch (e) {
        try { // for firefox
            window.sidebar.addPanel(sTitle, sURL, "");
        }
        catch (e) {
            alert("加入收藏失败，请使用Ctrl+D进行添加");
        }
    }
}


/**
 * 设为首页
 * @param obj 设置对象
 * @param vrl 网站地址
 * @example  <a onclick="SetHome(this);">设为首页</a>
 */
cm.SetHome = function(obj, vrl) {
    // 默认地址为本页面
    vrl = vrl || window.location.href;
    try {
        obj.style.behavior = 'url(#default#homepage)';
        obj.setHomePage(vrl);
    }
    catch(e) {
        if ( window.netscape ) {
            try {
                window.netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
            }
            catch (e) {
                alert("此操作被浏览器拒绝！\n请在浏览器地址栏输入“about:config”并回车\n然后将 [signed.applets.codebase_principal_support]的值设置为'true',双击即可。");
            }
            var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
            prefs.setCharPref('browser.startup.homepage',vrl);
         }
    }
}


/**
 * 获取URL里面的网站域名
 * @param url 网址；没有参数时默认使用所在网页的网址
 * @return 返回网站的域名字符串；不是有效网址时返回空字符串
 */
cm.getHost = function(url) {
    // 没有参数时，默认用本站的
    url = url || window.location.href;
    // 用正则表达式来匹配
    var match = url.match(/.*\:\/\/([^\/]*).*/);
    // 能匹配则返回匹配的内容
    if ( match )
        return match[1];
    // 无法匹配(不是有效的网址)，则返回 null
    return "";
}

