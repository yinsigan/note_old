﻿/**
 * <P> Title: 工具類別                      </P>
 * <P> Description: 工具類別                </P>
 * <P> Copyright: Copyright (c) 2010-07-13  </P>
 * <P> Company:Everunion Tech. Ltd.         </P>
 * @author Holer
 * @version 0.1 Original Design from design document.
 */
using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Text;

namespace Com.Everunion.Util
{
    class FileUtil
    {

        /// <summary>
        /// 創建檔案
        /// </summary>
        /// <param name="fileName">檔案名稱</param>
        /// <param name="strMsg">檔案裡需要寫入的資料</param>
        /// <param name="overwritten">如果檔案已經存在,是否覆蓋;true則覆蓋舊檔,false則保留舊檔而追加資料到檔案後面</param>
        public static void WriteFile(string fileName, string strMsg, bool overwritten)
        {
            FileInfo file = new FileInfo(fileName);
            //如果檔案路徑不存在,先創建資料夾
            if ( !file.Directory.Exists )
            {
                file.Directory.Create();
            }
            //如果檔案已經存在;overwritten 為true則覆蓋舊檔,false則保留舊檔而追加資料到檔案後面
            FileMode fileMode = overwritten ? FileMode.Create : FileMode.Append;
            FileStream fs = new FileStream(fileName, fileMode);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
            //寫入資料
            sw.WriteLine(strMsg);
            sw.Flush();
            //關閉流
            sw.Close();
            fs.Close();
            //銷毀物件
            sw.Dispose();
            fs.Dispose();
        }


        /// <summary>
        /// 創建檔案; 如果檔案已經存在,則覆蓋舊檔
        /// </summary>
        /// <param name="fileName">檔案名稱</param>
        /// <param name="strMsg">檔案裡需要寫入的資料</param>
        public static void WriteFile(string fileName, string strMsg)
        {
            WriteFile( fileName, strMsg, true );
        }


        /// <summary>
        /// 刪除目錄下的所有檔案
        /// </summary>
        /// <param name="Dir">檔案目錄(不存在時創建)</param>
        public static void DeleteDirFiles(string Dir)
        {
            //目錄不存在
            if (!System.IO.Directory.Exists(Dir))
            {
                System.IO.Directory.CreateDirectory(Dir);
            }
            //目錄存在
            else
            {
                //檔案陣列
                string[] Files = System.IO.Directory.GetFiles(Dir);
                foreach (string file in Files)
                {
                    //刪除
                    System.IO.File.Delete(file);
                }
            }
        }


        /// <summary>
        /// 刪除目錄下的所有檔案
        /// </summary>
        /// <param name="Dir">檔案目錄(不存在時創建)</param>
        public static void DeleteFile(string fileName)
        {
            try
            {
                System.IO.File.Delete(fileName);
            }
            catch
            {
            }
        }


    }
}
