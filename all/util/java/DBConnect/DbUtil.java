﻿//2010/09/17 dennis 加入取得編號 1073-1122
/**
 * <P> Title: 公用類別                                      </P>
 * <P> Description: 資料庫操作工具                          </P>
 * <P> Copyright: Copyright (c) 2010/07/31                  </P>
 * <P> Company:Everunion Tech. Ltd.                         </P>
 */

package com.everunion.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;


/**
 * 資料庫操作工具
 * @author <a href='daillow@gmail.com'>Holer</a>
 * @version 0.2
 */
public final class DbUtil
{
    /**
     * Connection Pool
     */
    private static DataSource dataSource = null;
    
    
    /**
     * 取得 Connection Pool
     */
    private synchronized static void setupDataSource()
    {
        // 讀取配置檔(位於: 項目/WEB-INF/classes/database.properties)
        ResourceBundle actionDB = ResourceBundle.getBundle("database");
        // 取得對應的屬性值
        String driverName = actionDB.getString("DriverName");
        String DBUrl = actionDB.getString("DBUrl");
        String Username = actionDB.getString("Username");
        String Password = actionDB.getString("Password");
        
        // 最大等待時間
        String MaxWaitStr = actionDB.getString("MaxWait");
        if ( MaxWaitStr == null || "".equals(MaxWaitStr) )
            MaxWaitStr = "3000";
        long MaxWait = (new Long(MaxWaitStr)).longValue();
        
        String MaxIdleStr = actionDB.getString("MaxIdle");
        if ( MaxIdleStr == null || "".equals(MaxIdleStr) )
            MaxIdleStr = "10";
        int MaxIdle = (new Integer(MaxIdleStr)).intValue();
        
        String MaxActiveStr = actionDB.getString("MaxActive");
        if ( MaxActiveStr == null || "".equals(MaxActiveStr) )
            MaxActiveStr = "100";
        int MaxActive = (new Integer(MaxActiveStr)).intValue();
        
        // 建立 Connection Pool
        BasicDataSource bds = new BasicDataSource();
        bds.setDriverClassName(driverName);
        bds.setUrl(DBUrl);
        bds.setUsername(Username);
        bds.setPassword(Password);
        bds.setMaxWait(MaxWait);
        bds.setMaxIdle(MaxIdle);
        bds.setMinIdle(2);
        bds.setMaxActive(MaxActive);
        dataSource = bds;
    }
    

    /**
     * 取得 資料庫連結
     * @return 資料庫連結
     * @throws SQLException 資料庫操作例外
     */
    public static Connection getConn() throws SQLException
    {
        Connection conn = null;
        try
        {
            if ( dataSource == null )
            {
                // 取得Connection Pool
                setupDataSource();
            }
            // 取得資料庫連結
            conn = dataSource.getConnection();
        }
        // 拋出 資料庫操作例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.getConn -> SQLException:" + e.toString());
            throw e;
        }
        return conn;
    }
    

    /**
     * 取得資料庫連結
     * @param driverName 資料庫驅動名
     * @param url 資料庫連結地址
     * @param username 資料庫登錄名
     * @param password 資料庫登錄密碼
     * @return Connection 資料庫連結
     * @throws ClassNotFoundException 資料庫驅動找不到例外
     * @throws SQLException 資料庫操作例外
     */
    public static Connection getConn(String driverName, String url, String username, String password)
        throws ClassNotFoundException, SQLException
    {
        Connection conn = null;
        try
        {
            // 加載驅動
            Class.forName(driverName);
            // 取得連結
            conn = DriverManager.getConnection(url, username, password);
        }
        // 拋出 資料庫驅動找不到例外
        catch ( ClassNotFoundException e )
        {
            System.out.println("DbUtil.getConn -> ClassNotFoundException:" + e.toString());
            throw e;
        }
        // 拋出 資料庫操作例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.getConn -> SQLException:" + e.toString());
            throw e;
        }
        return conn;
    }
    

    /**
     * 還原交易處理
     * @param conn 資料庫連結
     */
    public static void rollback(Connection conn)
    {
        try
        {
            boolean isAutoCommit = conn.getAutoCommit();
            if ( false == isAutoCommit )
            {
                // 還原交易處理
                conn.rollback();
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.rollback -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 設定交易送出模式
     * @param conn 資料庫連結
     * @param isAutoCommit 是否自動送出
     */
    public static void setAutoCommit(Connection conn, boolean isAutoCommit)
    {
        try
        {
            conn.setAutoCommit(isAutoCommit);
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.setAutoCommit -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉資料庫連結
     * @param conn 資料庫連結
     */
    public static void close(Connection conn)
    {
        try
        {
            if ( conn != null )
            {
                // 如果不是自動送出模式,交易送出
                if ( false == conn.getAutoCommit() )
                {
                   conn.commit();
                }
                // 關閉資料庫連結
                conn.close();
                conn = null;
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.close -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉資料庫操作連結
     * @param statement 資料庫Statement
     */
    public static void close(Statement statement)
    {
        try
        {
            if ( statement != null )
            {
                // 關閉資源
                statement.close();
                statement = null;
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.close -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉結果集連結
     * @param resultSet 結果集連結
     */
    public static void close(ResultSet resultSet)
    {
        try
        {
            if ( resultSet != null )
            {
                // 關閉資源
                resultSet.close();
                resultSet = null;
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.close -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉連結
     * @param conn 資料庫連結
     * @param stmt 資料庫操作連結
     * @param resultSet 結果集連結
     */
    public static void close(Connection conn, Statement stmt, ResultSet resultSet)
    {
        close(resultSet);
        close(stmt);
        close(conn);
    }
    

    /**
     * 取得資料
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws SQLException 資料庫操作例外
     */
    public static ArrayList<HashMap<String, String>> getData(Connection conn, String sql, List<Object> valueList)
            throws SQLException
    {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        // 資料庫資源
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try
        {
            // 操作資料庫
            pstmt = conn.prepareStatement(sql);
            // 如果需要傳參數
            putSQLParams(pstmt, valueList);
            rs = pstmt.executeQuery();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int col = rsMetaData.getColumnCount();
            // 加入資料
            while ( rs.next() )
            {
                HashMap<String, String> map = new LinkedHashMap<String, String>();
                for ( int i = 1; i <= col; i++ )
                {
                    map.put(rsMetaData.getColumnLabel(i).toLowerCase(), rs.getString(i));
                }
                list.add(map);
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.getData -> SQLException: " + e.toString());
            System.out.println("DbUtil.getData -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
        return list;
    }
    

    /**
     * 取得資料
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws SQLException 資料庫操作例外
     */
    public static ArrayList<HashMap<String, String>> getData(Connection conn, String sql) throws SQLException
    {
        return getData(conn, sql, null);
    }
    

    /**
     * 取得資料 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static ArrayList<HashMap<String, String>> getData(String sql, List<Object> valueList)
    {
        Connection conn = null;
        try
        {
            // 開啟資料庫連結
            conn = getConn();
            return getData(conn, sql, valueList);
        }
        // 發生資料庫例外時,拋出,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.getData -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 取得資料 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static ArrayList<HashMap<String, String>> getData(String sql)
    {
        return getData(sql, null);
    }
    

    /**
     * 取得資料，number 從1開始
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws SQLException 資料庫操作例外
     */
    public static ArrayList<HashMap<String, String>> getDataByNum(Connection conn, String sql, List<Object> valueList)
            throws SQLException
    {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        // 資料庫資源
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try
        {
            // 操作資料庫
            pstmt = conn.prepareStatement(sql);
            // 如果需要傳參數
            putSQLParams(pstmt, valueList);
            rs = pstmt.executeQuery();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int col = rsMetaData.getColumnCount();
            // 加入資料
            while ( rs.next() )
            {
                HashMap<String, String> map = new LinkedHashMap<String, String>();
                for ( int i = 1; i <= col; i++ )
                {
                    map.put("" + i, rs.getString(i));
                }
                list.add(map);
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.getDataByNum -> SQLException: " + e.toString());
            System.out.println("DbUtil.getDataByNum -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
        return list;
    }
    

    /**
     * 取得資料，number 從1開始
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws SQLException 資料庫操作例外
     */
    public static ArrayList<HashMap<String, String>> getDataByNum(Connection conn, String sql) throws SQLException
    {
        return getDataByNum(conn, sql, null);
    }
    

    /**
     * 取得資料，number 從1開始 <br/>
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static ArrayList<HashMap<String, String>> getDataByNum(String sql, List<Object> valueList)
    {
        Connection conn = null;
        try
        {
            // 開啟資料庫連結
            conn = getConn();
            return getDataByNum(conn, sql, valueList);
        }
        // 發生資料庫例外時,拋出,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.getDataByNum -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 取得資料，number 從1開始 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static ArrayList<HashMap<String, String>> getDataByNum(String sql)
    {
        return getDataByNum(sql, null);
    }
    

    /**
     * 取得一筆資料
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return HashMap 資料集,查不到資料時傳回size為0的HashMap
     * @throws SQLException 資料庫操作例外
     */
    public static HashMap<String, String> getOneData(Connection conn, String sql, List<Object> valueList)
            throws SQLException
    {
        // 資料庫資源
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        HashMap<String, String> map = new LinkedHashMap<String, String>();
        try
        {
            // 操作資料庫
            pstmt = conn.prepareStatement(sql);
            // 如果需要傳參數
            putSQLParams(pstmt, valueList);
            rs = pstmt.executeQuery();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int col = rsMetaData.getColumnCount();
            // 加入資料
            if ( rs.next() )
            {
                for ( int i = 1; i <= col; i++ )
                {
                    map.put(rsMetaData.getColumnLabel(i).toLowerCase(), rs.getString(i));
                }
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.getOneData -> SQLException: " + e.toString());
            System.out.println("DbUtil.getOneData -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
        return map;
    }
    

    /**
     * 取得一筆資料
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return HashMap 資料集,查不到資料時傳回size為0的HashMap
     * @throws SQLException 資料庫操作例外
     */
    public static HashMap<String, String> getOneData(Connection conn, String sql) throws SQLException
    {
        return getOneData(conn, sql, null);
    }
    

    /**
     * 取得一筆資料 <br/>
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return HashMap 資料集,查不到資料時傳回size為0的HashMap
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static HashMap<String, String> getOneData(String sql, List<Object> valueList)
    {
        Connection conn = null;
        try
        {
            // 開啟資料庫連結
            conn = getConn();
            return getOneData(conn, sql, valueList);
        }
        // 發生資料庫例外時,拋出,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.getOneData -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 取得一筆資料 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @return HashMap 資料集,查不到資料時傳回size為0的HashMap
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static HashMap<String, String> getOneData(String sql)
    {
        return getOneData(sql, null);
    }
    

    /**
     * 查詢一個欄位
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return String 結果；查無資料，以及資料為 null 時，傳回 null
     * @throws SQLException 資料庫操作例外
     */
    public static String queryOne(Connection conn, String sql, List<Object> valueList) throws SQLException
    {
        // 資料庫資源
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try
        {
            // 操作資料庫
            pstmt = conn.prepareStatement(sql);
            // 如果需要傳參數
            putSQLParams(pstmt, valueList);
            rs = pstmt.executeQuery();
            // 只查詢一個欄位
            if ( rs.next() )
                return rs.getString(1);
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.queryOne -> SQLException: " + e.toString());
            System.out.println("DbUtil.queryOne -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
        // 查不到內容時，傳回null
        return null;
    }
    

    /**
     * 查詢一個欄位
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return String 結果； 查無資料，以及資料為 null 時，傳回 null
     * @throws SQLException 資料庫操作例外
     */
    public static String queryOne(Connection conn, String sql) throws SQLException
    {
        return queryOne(conn, sql, null);
    }
    

    /**
     * 查詢一個欄位
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return String 結果；查無資料，以及資料為 null 時，傳回 null
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static String queryOne(String sql, List<Object> valueList)
    {
        Connection conn = null;
        try
        {
            // 開啟資料庫連結
            conn = getConn();
            return queryOne(conn, sql, valueList);
        }
        // 發生資料例外時,拋出例外,但可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.queryOne -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 查詢一個欄位
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return String 結果；查無資料，以及資料為 null 時，傳回 null
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static String queryOne(String sql)
    {
        return queryOne(sql, null);
    }
    

    /**
     * 取得資料, 要求每筆資料只有兩個欄位
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return HashMap 資料集，查不到資料時傳回size為0的HashMap
     * @throws SQLException 資料庫操作例外
     */
    public static HashMap<String, String> getMap(Connection conn, String sql, List<Object> valueList)
            throws SQLException
    {
        // 資料庫資源
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        HashMap<String, String> map = new LinkedHashMap<String, String>();
        try
        {
            // 操作資料庫
            pstmt = conn.prepareStatement(sql);
            // 如果需要傳參數
            putSQLParams(pstmt, valueList);
            rs = pstmt.executeQuery();
            // 加入資料
            while ( rs.next() )
            {
                map.put(rs.getString(1), rs.getString(2));
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.getMap -> SQLException: " + e.toString());
            System.out.println("DbUtil.getMap -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
        return map;
    }
    

    /**
     * 取得資料, 要求每筆資料只有兩個欄位
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return HashMap 資料集，查不到資料時傳回size為0的HashMap
     * @throws SQLException 資料庫操作例外
     */
    public static HashMap<String, String> getMap(Connection conn, String sql) throws SQLException
    {
        return getMap(conn, sql, null);
    }
    

    /**
     * 取得資料, 要求每筆資料只有兩個欄位 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return HashMap 資料集，查不到資料時傳回size為0的HashMap
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static HashMap<String, String> getMap(String sql, List<Object> valueList)
    {
        Connection conn = null;
        try
        {
            // 取得資料庫連結
            conn = getConn();
            return getMap(conn, sql, valueList);
        }
        // 發生資料庫例外時,拋出,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.getMap -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 取得資料, 要求每筆資料只有兩個欄位 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 查詢的SQL
     * @return HashMap 資料集，查不到資料時傳回size為0的HashMap
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static HashMap<String, String> getMap(String sql)
    {
        return getMap(sql, null);
    }
    

    /**
     * 執行資料庫操作SQL，如 INSERT、UPDATE 或 DELETE 等
     * @param conn 資料庫連結
     * @param sql 操作資料庫的SQL
     * @param valueList 操作資料庫的SQL的參數資料集
     * @return int 新增、修改或刪除 等所影響的筆數(如果是新增,且主鍵是自動增長的,傳回主鍵)
     * @throws SQLException 資料庫操作例外
     */
    public static int execute(Connection conn, String sql, List<Object> valueList) throws SQLException
    {
        // 資料庫資源
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try
        {
            // 取得資料庫 Statement
            pstmt = conn.prepareStatement(sql);
            // 傳遞參數
            putSQLParams(pstmt, valueList);
            int affect = pstmt.executeUpdate();
            
            // 操作成功時
            if ( affect >= 1 )
            {
                sql = sql.trim().toLowerCase();
                // 確定是新增,新增時傳回主鍵
                if ( sql.startsWith("insert ") )
                {
                    // 檢索由於執行此 Statement 物件而建立的所有自動生成的鍵
                    rs = pstmt.getGeneratedKeys();
                    if ( rs.next() )
                    {
                        // 僅有一列，故取得第一列
                        long oid = rs.getLong(1);
                        if ( oid > 0 )
                        {
                            return Integer.parseInt("" + oid);
                        }
                    }
                }
            }
            return affect;
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.execute -> SQLException: " + e.toString());
            System.out.println("DbUtil.execute -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
    }
    

    /**
     * 執行無傳回值的 SQL，如 INSERT、UPDATE 或 DELETE 等
     * @param conn 資料庫連結
     * @param sql 操作資料庫的SQL
     * @return int 新增、修改或刪除 等所影響的筆數
     * @throws SQLException 資料庫操作例外
     */
    public static int execute(Connection conn, String sql) throws SQLException
    {
        return execute(conn, sql, null);
    }
    

    /**
     * 執行資料庫操作SQL，如 INSERT、UPDATE 或 DELETE 等 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 操作資料庫的SQL
     * @param valueList 操作資料庫的SQL的參數資料集
     * @return int 新增、修改或刪除 等所影響的筆數
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static int execute(String sql, List<Object> valueList)
    {
        Connection conn = null;
        try
        {
            // 取得資料庫連結
            conn = getConn();
            return execute(conn, sql, valueList);
        }
        // 發生資料庫例外時,拋出,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.execute -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 執行資料庫操作SQL，如 INSERT、UPDATE 或 DELETE 等 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param sql 操作資料庫的SQL
     * @return int 新增、修改或刪除 等所影響的筆數;
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static int execute(String sql)
    {
        return execute(sql, null);
    }
    

    /**
     * 驗證欄位的某值是否已經存在(通常用來驗證主鍵是否重複)
     * @param conn 資料庫連結
     * @param tableName 資料庫操作的表名
     * @param key 欄位名
     * @param value 欄位的值
     * @return boolean 驗證結果:欄位此值已經存在時傳回true，不存在時傳回false
     * @throws SQLException 資料庫操作例外
     */
    public static boolean isHave(Connection conn, String tableName, String key, String value) throws SQLException
    {
        // 防呆
        if ( tableName != null && key != null )
        {
            // 驗證 主鍵 是否已經存在
            List<Object> valueList = new ArrayList<Object>();
            valueList.add(value);
            String sql = "select " + key + " from " + tableName + " where " + key + "= ? ";
            // 如果欄位此值已經存在，傳回true
            if ( getOneData(conn, sql, valueList).size() > 0 )
            {
                return true;
            }
        }
        return false;
    }
    

    /**
     * 驗證欄位的某值是否已經存在(通常用來驗證主鍵是否重複) 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param tableName 資料庫操作的表名
     * @param key 欄位名
     * @param value 欄位的值
     * @return boolean 驗證結果:欄位此值已經存在時傳回true,不存在時傳回false
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     */
    public static boolean isHave(String tableName, String key, String value)
    {
        Connection conn = null;
        try
        {
            // 取得資料庫連結
            conn = getConn();
            return isHave(conn, tableName, key, value);
        }
        // 發生資料庫例外時,拋出,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.isHave -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    

    /**
     * 傳遞參數到 PreparedStatement, 僅供本類內部使用 (註:此方法有待改善,目前僅字串且不為空情況下測試過)
     * @param pstmt PreparedStatement
     * @param valueList 參數列表
     * @return PreparedStatement連結
     * @throws SQLException 資料庫操作例外
     */
    public static PreparedStatement putSQLParams(PreparedStatement pstmt, List<Object> valueList) throws SQLException
    {
        try
        {
            // 查詢的參數資料集的筆數
            int valueListSize = (valueList == null) ? 0 : valueList.size();
            for ( int i = 0; i < valueListSize; i++ )
            {
                // 取得參數
                Object obj = valueList.get(i);
                // 根據參數類型來賦值
                if ( obj == null )
                    pstmt.setNull(i + 1, java.sql.Types.NULL);
                else if ( obj instanceof String )
                    pstmt.setString(i + 1, "" + valueList.get(i));
                else if ( obj instanceof Integer )
                    pstmt.setInt(i + 1, Integer.parseInt("" + obj));
                else if ( obj instanceof Long )
                    pstmt.setLong(i + 1, Long.parseLong("" + obj));
                else if ( obj instanceof Double )
                    pstmt.setDouble(i + 1, Double.parseDouble("" + obj));
                else if ( obj instanceof java.util.Date )
                    pstmt.setDate(i + 1, new java.sql.Date(((java.util.Date)obj).getTime()));
                else if ( obj instanceof java.sql.Blob )
                    pstmt.setBlob(i + 1, (java.sql.Blob)obj);
                else
                    pstmt.setObject(i + 1, obj);
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.putSQLParams -> SQLException: " + e.toString());
            throw e;
        }
        return pstmt;
    }
    

    /**
     * 增加SQL查詢條件，和對應的值 <br />
     * 動作為 like 時，使用模糊查詢，忽略大小寫，空格轉變成匹配任意內容; “%”、“_”將被轉義 <br />
     * @param name 需要增加的SQL的欄位名稱
     * @param action 需要增加的SQL的對應動作，如 like, =, >= 等
     * @param value 對應的值
     * @param valueList 參數列表
     * @param canNull 空值參數是否加入查詢,為true時""和null都將加入查詢條件,否則不加入查詢條件;預設為false
     * @return SQL條件
     * @example sql = "select * from tableName a where 1=1 "; <br />
     *          sql += DbUtil.addSqlCondition("a.aporder", "like", request.getParameter("aporder"), valueList, false);  <br />
     *     結果: sql = "select * from tableName a where 1=1 and a.aporder like ? "; <br />
     *           valueList.add("%value%");
     */
    public static String addSqlCondition(String name, String action, String value, List<Object> valueList,
            boolean canNull)
    {
        // 防呆 和 去除前後空格
        name = (name == null) ? "" : name.trim();
        // 動作,轉成小寫並且刪除中間的空格
        action = (action == null) ? "" : action.trim().toLowerCase().replaceAll("\\s+", " ");
        // 如果 name,action 值為空的話, 不用加上; value為空是有可能的
        if ( "".equals(name) || "".equals(action) )
            return "";
        // 參數為空時，不加入查詢條件
        if ( !canNull && (value == null || "".equals(value)) )
            return "";
        
        // value為null值時
        if ( value == null )
        {
            // 等於 null
            if ( "like".equals(action) || "=".equals(action) || "is".equals(action) )
                return (" and " + name + " is null ");
            // 不等於null
            else if ( "!=".equals(action) || "<>".equals(action) || "^=".equals(action)
                    || "is not".equals(action) || "not like".equals(action) )
                return (" and " + name + " is not null ");
            else
                return "";
        }
        // 刪除前後空格
        value = value.trim();
        
        // 如果動作是"like",則使用多個 instr 來代替模糊查詢;效率更高
        if ( "like".equals(action) || "not like".equals(action) )
        {
            // 如果 value 值為空的話, 不用加上
            if ( "".equals(value) )
                return "";
            
            // 傳回值
            String returnSQL = "";
            // not like 時
            String notLike = ("not like".equals(action) ? "<=0" : "");
            
            // 按空格分隔
            String[] values = value.toUpperCase().replaceAll("\\s+", " ").split(" ");
            for ( int i = 0; i < values.length; i++ )
            {
                // 如果有參數列表
                if ( valueList != null )
                {
                    // 為參數列表加上 value 值
                    valueList.add(values[i]);
                    // 傳回的字串結果，例如： and instr(UPPER(name), ?)
                    // 由於本項目使用MySQL,他本身已經忽略大小寫,不必徒增UPPER()
                    returnSQL += " and instr(" + name + ", ?)";
                }
                else
                {
                    // 傳回的字串結果，例如： and instr(UPPER(name), ?)
                    returnSQL += " and instr(" + name + ", '" + StringUtil.toSqlStr(values[i]) + "')";
                }
                returnSQL += notLike;
            }
            return returnSQL;
        }
        
        // 如果有參數列表
        if ( valueList != null )
        {
            // 為參數列表加上 value 值
            valueList.add(value);
            // 傳回的字串結果，例如： and col = value
            return (" and " + name + " " + action + " ? ");
        }
        // 沒有參數列表，則傳回查詢SQL條件
        value = StringUtil.toSqlStr(value);
        return (" and " + name + " " + action + " '" + value + "' ");
    }
    
    
    /**
     * 增加SQL查詢條件，和對應的值 <br />
     * 動作為 like 時，使用模糊查詢，忽略大小寫，空格轉變成匹配任意內容; “%”、“_”將被轉義 <br />
     * @param name 需要增加的SQL的欄位名稱
     * @param action 需要增加的SQL的對應動作，如 like, =, >= 等
     * @param value 對應的值
     * @param valueList 參數列表
     * @param canNull 空值參數是否加入查詢,為true時""和null都將加入查詢條件,否則不加入查詢條件;預設為false
     * @return SQL條件
     * @example sql = "select * from tableName a where 1=1 "; <br />
     *          sql += DbUtil.addSqlCondition("a.aporder", "like", request.getParameter("aporder"), valueList, false);  <br />
     *     結果: sql = "select * from tableName a where 1=1 and a.aporder like ? "; <br />
     *           valueList.add("%value%");
     */
    public static String addLikeSql(String name, String action, String value, List<Object> valueList,
            boolean canNull)
    {
        // 防呆 和 去除前後空格
        name = (name == null) ? "" : name.trim();
        // 動作,轉成小寫並且刪除中間的空格
        action = (action == null) ? "" : action.trim().toLowerCase().replaceAll("\\s+", " ");
        // 如果 name,action 值為空的話, 不用加上; value為空是有可能的
        if ( "".equals(name) || "".equals(action) )
            return "";
        // 參數為空時，不加入查詢條件
        if ( !canNull && (value == null || "".equals(value)) )
            return "";
        
        // value為null值時
        if ( value == null )
        {
            // 等於 null
            if ( "like".equals(action) || "=".equals(action) || "is".equals(action) )
                return (" and " + name + " is null ");
            // 不等於null
            else if ( "!=".equals(action) || "<>".equals(action) || "^=".equals(action)
                    || "is not".equals(action) || "not like".equals(action) )
                return (" and " + name + " is not null ");
            else
                return "";
        }
        // 刪除前後空格
        value = value.trim();
        
        // 如果動作是"like",則使用多個 instr 來代替模糊查詢;效率更高
        if ( "like".equals(action) || "not like".equals(action) )
        {
            // 如果 value 值為空的話, 不用加上
            if ( "".equals(value) )
                return "";
            
            // 傳回值
            String returnSQL = "";
            // not like 時
            String notLike = ("not like".equals(action) ? "<=0" : "");
            
            // 按空格分隔
            String values = value.toUpperCase().replaceAll("\\s+", "%");
            // 如果有參數列表
            if ( valueList != null )
            {
                // 為參數列表加上 value 值
                valueList.add(values + "%");
                // 傳回的字串結果，例如： and instr(UPPER(name), ?)
                // 由於本項目使用MySQL,他本身已經忽略大小寫,不必徒增UPPER()
                //returnSQL += " and instr(" + name + ", ?)";
                returnSQL += " and " + name + " like ? ";
            }
            else
            {
                // 傳回的字串結果，例如： and instr(UPPER(name), ?)
                //returnSQL += " and instr(" + name + ", '" + StringUtil.toSqlStr(values[i]) + "')";
                returnSQL += " and " + name + " like '" + StringUtil.toSqlStr(values) + "%'";
            }
            returnSQL += notLike;
            return returnSQL;
        }
        
        // 如果有參數列表
        if ( valueList != null )
        {
            // 為參數列表加上 value 值
            valueList.add(value);
            // 傳回的字串結果，例如： and col = value
            return (" and " + name + " " + action + " ? ");
        }
        // 沒有參數列表，則傳回查詢SQL條件
        value = StringUtil.toSqlStr(value);
        return (" and " + name + " " + action + " '" + value + "' ");
    }
    

    /**
     * 增加查詢SQL，和對應的值； <br />
     * 如果參數值為空，就不加上去 動作為 like 時，使用模糊查詢，忽略大小寫，空格轉變成匹配任意內容; “%”、“_”將被轉義 <br />
     * @param name 需要增加的SQL的欄位名稱
     * @param action 需要增加的SQL的對應動作，如 like, =, >= 等
     * @param value 對應的值
     * @param valueList 參數列表
     * @param canNull 空值參數是否加入查詢,為true時""和null都將加入查詢條件,否則不加入查詢條件;預設為false
     * @return SQL條件
     * @example sql = "select * from tableName a where 1=1 ";  <br />
     *          sql += DbUtil.addSqlCondition("a.aporder", "like", request.getParameter("aporder"), valueList); <br />
     *     結果: sql = "select * from tableName a where 1=1 and a.aporder like ? "; <br />
     *          valueList.add("%value%");
     */
    public static String addSqlCondition(String name, String action, String value, List<Object> valueList)
    {
        return addSqlCondition(name, action, value, null, false);
    }
    
    
    /**
     * 增加查詢SQL，和對應的值； <br />
     * 如果參數值為空，就不加上去 動作為 like 時，使用模糊查詢，忽略大小寫，空格轉變成匹配任意內容; “%”、“_”將被轉義 <br />
     * @param name 需要增加的SQL的欄位名稱
     * @param action 需要增加的SQL的對應動作，如 like, =, >= 等
     * @param value 對應的值
     * @param valueList 參數列表
     * @param canNull 空值參數是否加入查詢,為true時""和null都將加入查詢條件,否則不加入查詢條件;預設為false
     * @return SQL條件
     * @example sql = "select * from tableName a where 1=1 ";  <br />
     *          sql += DbUtil.addSqlCondition("a.aporder", "like", request.getParameter("aporder"), valueList); <br />
     *     結果: sql = "select * from tableName a where 1=1 and a.aporder like ? "; <br />
     *          valueList.add("%value%");
     */
    public static String addLikeSql(String name, String action, String value, List<Object> valueList)
    {
        return addLikeSql(name, action, value, null, false);
    }
    

    /**
     * 增加查詢SQL，和對應的值； <br />
     * 如果值為空的，就不加上去 動作為 like 時，使用模糊查詢，忽略大小寫，空格轉變成匹配任意內容 <br />
     * 如果含有特殊符號，請在條件的最後面加上“escape'\'”
     * @param name 需要增加的SQL的欄位名稱
     * @param action 需要增加的SQL的對應動作，如 like, =, >= 等
     * @param value 對應的值
     * @return SQL條件
     * @example sql = "select * from tableName a where 1=1 "; <br />
     *          sql += DbUtil.addSqlCondition("a.aporder", "like", request.getParameter("aporder")); <br />
     *     結果: sql = "select * from tableName a where 1=1 and a.aporder like '%value%'"; <br />
     */
    public static String addSqlCondition(String name, String action, String value)
    {
        return addSqlCondition(name, action, value, null);
    }
    
    
    /**
     * 新增資料
     * @param conn 資料庫連結
     * @param table 表名稱
     * @param data 資料集
     * @return int 操作訊息(>0：為主鍵,否則失敗)
     * @example table:album,data:{id:1,name:2};result:1
     */
    public static int insert(Connection conn, String table, HashMap<String, Object> data) throws SQLException
    {
    	//取得SQL和參數列表
    	Object[] datas = getInsertSQL(table, data);
    	String sql = (String)datas[0];
        //參數列表
        List<Object> valueList = (List)datas[1];
		//新增
        return execute(conn, sql, valueList);
    }
    
    
    /**
     * 新增資料 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param table 表名稱
     * @param data 資料集
     * @return int 操作訊息(>0：為主鍵,否則失敗)
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     * @example table:album, data:{id:1,name:2}; result:1
     */
    public static int insert(String table, HashMap<String, Object> data)
    {
        Connection conn = null;
        try
        {
            // 取得資料庫連結
            conn = getConn();
            return insert(conn, table, data);
        }
        // 發生資料庫例外時,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.insert -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    
    
    /**
     * 取得新增的SQL 和 參數列表
     * @param table 表名稱
     * @param datalist 資料集
     * @return Object[]{String, ArrayList} 第一個是SQL,第二個是參數列表
     */
    private static Object[] getInsertSQL(String table, HashMap<String, Object> data)
    {
        String sql1 = "insert into " + table + "(", sql2 = ")values(";
        //為空
        if ( data == null || data.isEmpty() || data.size() == 0 )
        {
            throw new RuntimeException("DbUtil.getInsertSQL -> data is Empty!");
        }
        //為空
        if ( table == null || table.length() == 0 )
        {
            throw new RuntimeException("DbUtil.getInsertSQL -> table is Empty!");
        }
        //參數列表
        List<Object> valueList = new ArrayList<Object>();
        //增加參數
        for ( Iterator<Map.Entry<String, Object>> iter = data.entrySet().iterator(); iter.hasNext(); )
        {
            Map.Entry<String, Object> entry = iter.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            //設定系統預設時間,需與其它程式約定這種寫法
            if ( "default_type_datetime".equals(StringUtil.toString(value).toLowerCase()) )
            {
            	sql1 += key + ",";
                sql2 += "now(),";
                continue;
            }
            sql1 += key + ",";
            sql2 += "?,";
            valueList.add(value);
        }
        //刪除SQL最後的逗號
        sql1 = sql1.substring(0, sql1.length() - 1);
        sql2 = sql2.substring(0, sql2.length() - 1);
        String sql = sql1 + sql2 + ")";
        //傳回值
        return new Object[]{sql, valueList};
    }
    
    
    /**
     * 批次新增資料; 其中一筆新增失敗時,還原處理
     * @param conn 資料庫連結
     * @param table 表名稱
     * @param datalist 資料集
     * @return int 操作訊息,新增多少筆
     */
    public static int insert(Connection conn, String table, ArrayList<HashMap<String,Object>> datalist)
        throws SQLException
    {
        //記錄插入多少筆資料
        int recode = 0;
        //參數列表為空
        if ( datalist == null || datalist.isEmpty() || datalist.size() == 0 )
        {
            throw new RuntimeException("DbUtil.insert -> data is Empty!");
        }

        String sql = "";
        //用字串類型,而不是Boolean,防止取的時候發生例外
        String autoCommit = "";
        //批次更新,如果多次用同一個SQL,建立PreparedStatement會更高效率
        PreparedStatement pstmt = null;
        try
        {
            // 儲存原本的交易送出模式
            autoCommit = ("" + conn.getAutoCommit()).toLowerCase();
            if ( "true".equals(autoCommit) )
            {
                //設定不可 以自動送出
                conn.setAutoCommit(false);
            }
            
            // 逐行新增
            for ( int i = 0; i < datalist.size(); i++ )
            {
                //取得SQL和參數列表
                Object[] datas = getInsertSQL(table, datalist.get(i));
                String this_sql = (String)datas[0];
                List<Object> valueList = (List)datas[1];
                //當與上一次的SQL相同時,使用上次的PreparedStatement,以提高批次操作的效率
                //不相同時,重新建立PreparedStatement
                if ( false == sql.equals(this_sql) )
                {
                    close(pstmt);
                    sql = this_sql;
                    pstmt = conn.prepareStatement(sql);
                }
                // 傳遞參數
                putSQLParams(pstmt, valueList);
                // 執行,並記錄插入數量
                int efferRows = pstmt.executeUpdate();
                // 如果新增失敗
                if ( efferRows < 1 )
                {
                    throw new SQLException("新增失敗!!");
                }
                recode += efferRows;
            }
            
            // 如果原本是自動送出模式,送出交易; 否則讓外層來送出
            if ( "true".equals(autoCommit) )
            {
                conn.commit();
            }
        }
        //拋出例外
        catch ( SQLException e )
        {
            rollback(conn);
            //輸出例外
            System.out.println("DbUtil.insert -> SQLException:" + e);
            throw e;
        }
        finally
        {
            //關閉 PreparedStatement
            close(pstmt);
            // 還原原本的交易送出模式
            if ( "true".equals(autoCommit) )
            {
                conn.setAutoCommit(true);
            }
        }
        //操作訊息
        return recode;
    }
    
    
    /**
     * 更新資料
     * @param conn 資料庫連結
     * @param table 表名稱
     * @param keyCondition 主鍵條件; 沒有則更新整個表格
     * @param data 資料集
     * @return int 操作訊息(更新的筆數)
     * @example table:album,keyCondition:oid=102,data:{id:1,name:2};result:1
     */
    public static int update(Connection conn, String table, String keyCondition, HashMap<String, Object> data) throws SQLException
    {
        //更新結果
        int result = 0;
        String sql = "update " + table + " set ";
        //table為空
        if ( table == null || table.length() == 0 )
        {
            throw new SQLException("table is Empty!");
        }
        //資料集為空
        if ( data == null || data.isEmpty() || data.size() == 0 )
        {
            throw new SQLException("data is Empty!");
        }
        //參數列表
        List<Object> valueList = new ArrayList<Object>();
        //增加參數
        for ( Iterator<Map.Entry<String, Object>> iter = data.entrySet().iterator(); iter.hasNext(); )
        {
            Map.Entry<String, Object> entry = iter.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            //設定系統預設時間,需與其它程式約定這種寫法
            if ( "default_type_datetime".equals(StringUtil.toString(value).toLowerCase()) )
            {
                sql += key + "=now(),";
                continue;
            }
            sql += key + "=?,";
            valueList.add(value);
        }
        //更新
        if ( valueList.size() > 0 )
        {
            sql = sql.substring(0, sql.length() - 1);
            // 有主鍵條件時,加上
            if ( keyCondition != null && !"".equals(keyCondition) )
            {
                sql += " where " + keyCondition;
            }
            return execute(conn, sql, valueList);
        }
        //操作訊息
        return result;
    }
    
    
    /**
     * 更新資料 <br />
     * 建議當僅有一次資料庫操作時使用,需多次操作的建議建立 Connection
     * @param table 表名稱
     * @param keyCondition 主鍵條件; 沒有則更新整個表格
     * @param data 資料集
     * @return int 操作訊息(更新的筆數)
     * @throws RuntimeException 資料庫操作例外,可以不 catch
     * @example table:album,keyCondition:oid=102,data:{id:1,name:2};result:1
     */
    public static int update(String table, String keyCondition, HashMap<String, Object> data)
    {
        Connection conn = null;
        try
        {
            // 取得資料庫連結
            conn = getConn();
            return update(conn, table, keyCondition, data);
        }
        // 發生資料庫例外時,可以不捕獲
        catch ( SQLException e )
        {
            throw new RuntimeException("DbUtil.update -> SQLException: " + e.toString());
        }
        finally
        {
            close(conn);
        }
    }
    
}