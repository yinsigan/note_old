﻿<%--

/**
 * <P> Title: Holemar                                            </P>
 * <P> Description: 資料庫操作頁面                               </P>
 * <P> Copyright: Copyright (c) 2010/01/13                       </P>
 * <P> Company:Everunion Tech. Ltd.                              </P>
 * @author <A href='daillow@gmail.com'>Holer W. L. Feng</A>
 * @version 0.1
 */

--%><%@ page
    language = "java"
    pageEncoding = "UTF-8"
    contentType  = "text/html; charset=UTF-8"
    import = "java.util.*"
    import = "java.sql.*"
    import = "java.io.*"
    import = "com.everunion.util.DbUtil"

%><%

    // 源SQL
    String sourceSQL = getValue(request, "SQL");
    // SQL的執行類型
    ExcutType sqlType = getSqlType(sourceSQL);
    // 是否使用分頁
    String directSearch = getValue(request,"directSearch", "true");
    // 實際執行SQL
    String sql = toSQL(sourceSQL);

    // 操作影響結果數
    int rows = 0;
    // 結果集
    List rslist = null;
    // 分頁條
    String topHTML = "";
    String bottomHTML = "";

    // 資料庫連結訊息
    Connection conn = null;
/*
    // MySQL
    String username = "root";
    String password="root";
    String driverName = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://127.0.0.1:3306/culture?characterEncoding=UTF-8&amp;characterSetResults=UTF-8";

    //SQL Server
    username = "sa";
    password = "test";
    driverName = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
    url = "jdbc:microsoft:sqlserver://192.168.0.55:1433;databaseName=lianxi";

    // Access
    driverName = "sun.jdbc.odbc.JdbcOdbcDriver";
    url = "jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=D:\\database.mdb";
*/

try
{
    if ( sourceSQL != null && !"".equals(sourceSQL) )
    {
        // 開啟資料庫連結
        conn = DbUtil.getConn();
        //conn = getConn(driverName, url, username, password);

        // 直接查詢
        if ( "false".equals(directSearch) || ExcutType.directSearch == sqlType )
        {
            rslist = getData(conn, sql);
        }
        // 可分頁查詢
        else if ( ExcutType.Select == sqlType )
        {
            // 使用分頁
            PageUtil PU = new PageUtil(conn, request, sql);
            // 預設每頁顯示100筆
            PU.setPageSize(100);
            topHTML = PU.genPageHtml();
            bottomHTML = PU.genPageHtml();
            sql = PU.getPageSql();
            rslist = getData(conn, sql);
        }
        // 執行資料庫操作
        else
        {
            rows = execute(conn, sql);
        }
    }
}
//例外
catch ( Exception e )
{
    out.println(request.getRequestURI() + " 出錯:" + e.toString());
}
//關閉
finally
{
    close(conn);
}

%><html>
<head>
<title>資料庫測試</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body>
<form name="listPage" id="listPage" action="#" method="post">
    <div align="center">
        <%-- // 查詢或執行語句輸入 --%>
        <textArea name="SQL" id="SQL" cols="95" rows="5"><%= toTextarea(sourceSQL) %></textArea><br />
        <input type="submit" value="執行" />
        <input type="button" value="清空" onclick="var field=document.getElementById('SQL'); field.value=''; field.focus();"/>
        <input type="hidden" value="true" name="directSearch" />
        <input type="button" value="直接查詢" onclick="var form=this.form; form.directSearch.value='false'; 
        form.submit();"/>
    </div>

<%

    // ********************* 迴圈程式 *************************** //

    //列印出 查詢SQL 和操作SQL
    int listSize = ( rslist == null ? 0 : rslist.size());
    if ( !"".equals(sourceSQL) )
    {
        // 直接查詢
        if ( "false".equals(directSearch) || ExcutType.directSearch == sqlType )
        {
            out.println("查詢資料 " + listSize + " 筆 <br>");
        }
        else if ( ExcutType.Select == sqlType || ExcutType.directSearch == sqlType )
        {
            out.println("查詢資料 " + listSize + " 筆 <br>" + toHtml(sql));
        }
        // 執行資料庫操作
        else
        {
            out.println("操作影响 " + rows + " 筆 <br>");
        }
    }

    // 資料表格
    if ( listSize > 0 )
    {
        // 上分頁
        out.println(topHTML);
        out.println("<br /><table border='1' align='center'><tbody>");
        out.println("<tr>");
        //標題欄
        for ( Iterator iter = ((Map)rslist.get(0)).keySet().iterator(); iter.hasNext(); )
        {
            Object key = iter.next();
            out.println("<th title='" + toHtml(key) + "'>" + toHtml(key) + "</th>");
        }
        out.println("</tr>");
    }
    //各行資料
    for ( int i = 0; i < listSize; i++ )
    {
        out.println("<tr>");
        //各欄位資料
        for ( Iterator iter = ((Map)rslist.get(i)).entrySet().iterator(); iter.hasNext(); )
        {
            Map.Entry entry = (Map.Entry) iter.next();
            String value = (String) entry.getValue();
            String titleValue = ( "".equals(value) ? "&nbsp;" : toHtml(value) );
            String showValue = ( value == null ? "<font color='red'>null</font>" : titleValue );
            // 為""時,加上空格; 為空時,紅色顯示
            out.println( "<td title='" + titleValue + "'>" + showValue + "</td>" );
        }
        out.println("</tr>");
    }
    // 資料表格結束
    if ( listSize > 0 )
    {
        out.println("</tbody></table><br />");
        // 下分頁
        out.println(bottomHTML);
    }

%></form>
</body>
</html><%!

    /**
     * 執行類型4種: 直接查詢,分頁查詢,資料庫操作,未知類型
     */
    enum ExcutType
    {
        directSearch, Select, Excute, None;
    }


    /**
     * 判斷SQL的執行類型
     * @param sourceSQL 需要執行的SQL
     * @return 枚舉類的對應類型
     */
    public static ExcutType getSqlType(String sourceSQL)
    {
        // 為空時
        if ( sourceSQL == null || "".equals(sourceSQL) )
        {
            return ExcutType.None;
        }
        
        // 檢查SQL
        String sql = sourceSQL.trim().toLowerCase().replaceAll("\r|\n", " ").replaceAll("\\s+", " ");
        // 分頁查詢
        if ( sql.startsWith("select ") )
        {
            return ExcutType.Select;
        }
        // 直接查詢的類型
        String[] selectKeys = new String[]{"show", "desc", "describe"};
        for ( int i = 0; i < selectKeys.length; i++ )
        {
            if ( sql.startsWith(selectKeys[i] + " ") )
            {
                return ExcutType.directSearch;
            }
        }
        // 操作的類型,由於類型太多，不再檢查
        return ExcutType.Excute;
    }


    /**
     * 將需字串轉換成 可執行的SQL字串
     * @param sour 需要進行轉換的物件
     * @return 轉換後的字串
     */
    public static String toSQL(String sour)
    {
        // 為空時
        if ( sour == null || "".equals(sour) )
        {
            return "";
        }
        // 執行的SQL
        String sql = sour.trim().replaceAll(";+$", "");
        // 避免換行沒有加空格的錯誤
        sql = sql.replaceAll("\n", " \n");
        // 刪除行後面的註釋,以“--”和“#”為行註釋符號
        sql = sql.replaceAll("--.*", "").replaceAll("#.*", "");
        return sql;
    }


    /**
     * 取得資料庫類型
     * @param conn 資料庫連結
     * @return 資料庫類型
     */
    public String getDbType(Connection conn)
    {
        // 預設值
        String init = "mysql";
        if ( conn != null )
        {
            // 資料庫驅動名
            String driverName = conn.toString().toLowerCase();
            // MySQL 資料庫
            if ( driverName.indexOf("mysql") >= 0 )
            {
                return "mysql";
            }
            // Oracle 資料庫
            if ( driverName.indexOf("oracle") >= 0 )
            {
                return "oracle";
            }
            // SQL Server 資料庫
            if ( driverName.indexOf("sqlserver") >= 0 )
            {
                return "sqlserver";
            }
            // Access 資料庫
            if ( driverName.indexOf("access") >= 0 )
            {
                return "access";
            }
        }
        // 傳回預設值
        return init;
    }


    /**
     * 取得資料庫連結
     * @param driverName 資料庫驅動名
     * @param url 資料庫連結地址
     * @param username 資料庫登錄名
     * @param password 資料庫登錄密碼
     * @return Connection 資料庫連結
     * @throws ClassNotFoundException 資料庫驅動找不到例外
     * @throws SQLException 資料庫操作例外
     */
    public static Connection getConn(String driverName, String url, String username, String password)
        throws ClassNotFoundException, SQLException
    {
        Connection conn = null;
        try
        {
            // 加載驅動
            Class.forName(driverName);
            // 取得連結
            conn = DriverManager.getConnection(url, username, password);
        }
        // 拋出 資料庫驅動找不到例外
        catch ( ClassNotFoundException e )
        {
            System.out.println("DbUtil.getConn -> ClassNotFoundException:" + e.toString());
            throw e;
        }
        // 拋出 資料庫操作例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.getConn -> SQLException:" + e.toString());
            throw e;
        }
        return conn;
    }
    

    /**
     * 關閉資料庫連結
     * @param conn 資料庫連結
     */
    public static void close(Connection conn)
    {
        try
        {
            if ( conn != null )
            {
                // 如果不是自動送出模式,交易送出
                if ( false == conn.getAutoCommit() )
                {
                   conn.commit();
                }
                // 關閉資料庫連結
                conn.close();
                conn = null;
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.close -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉資料庫操作連結
     * @param statement 資料庫Statement
     */
    public static void close(Statement statement)
    {
        try
        {
            if ( statement != null )
            {
                // 關閉資源
                statement.close();
                statement = null;
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.close -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉結果集連結
     * @param resultSet 結果集連結
     */
    public static void close(ResultSet resultSet)
    {
        try
        {
            if ( resultSet != null )
            {
                // 關閉資源
                resultSet.close();
                resultSet = null;
            }
        }
        // 捕獲例外
        catch ( SQLException e )
        {
            System.out.println("DbUtil.close -> SQLException:" + e.toString());
        }
    }
    

    /**
     * 關閉連結
     * @param conn 資料庫連結
     * @param stmt 資料庫操作連結
     * @param resultSet 結果集連結
     */
    public static void close(Connection conn, Statement stmt, ResultSet resultSet)
    {
        close(resultSet);
        close(stmt);
        close(conn);
    }
    

    /**
     * 取得資料
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @param valueList 查詢的SQL的參數資料集
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws SQLException 資料庫操作例外
     */
    public static ArrayList<HashMap<String, String>> getData(Connection conn, String sql, List<Object> valueList)
            throws SQLException
    {
        ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        // 資料庫資源
        ResultSet rs = null;
        PreparedStatement pstmt = null;
        try
        {
            // 操作資料庫
            pstmt = conn.prepareStatement(sql);
            // 如果需要傳參數
            putSQLParams(pstmt, valueList);
            rs = pstmt.executeQuery();
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int col = rsMetaData.getColumnCount();
            // 加入資料
            while ( rs.next() )
            {
                HashMap<String, String> map = new LinkedHashMap<String, String>();
                for ( int i = 1; i <= col; i++ )
                {
                    map.put(rsMetaData.getColumnLabel(i).toLowerCase(), rs.getString(i));
                }
                list.add(map);
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.getData -> SQLException: " + e.toString());
            System.out.println("DbUtil.getData -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
        return list;
    }
    

    /**
     * 取得資料
     * @param conn 資料庫連結
     * @param sql 查詢的SQL
     * @return ArrayList 資料集,查不到資料時傳回size為0的ArrayList
     * @throws SQLException 資料庫操作例外
     */
    public static ArrayList<HashMap<String, String>> getData(Connection conn, String sql) throws SQLException
    {
        return getData(conn, sql, null);
    }
    

    /**
     * 執行資料庫操作SQL，如 INSERT、UPDATE 或 DELETE 等
     * @param conn 資料庫連結
     * @param sql 操作資料庫的SQL
     * @param valueList 操作資料庫的SQL的參數資料集
     * @return int 新增、修改或刪除 等所影響的筆數(如果是新增,且主鍵是自動增長的,傳回主鍵)
     * @throws SQLException 資料庫操作例外
     */
    public static int execute(Connection conn, String sql, List<Object> valueList) throws SQLException
    {
        // 資料庫資源
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try
        {
            // 取得資料庫 Statement
            pstmt = conn.prepareStatement(sql);
            // 傳遞參數
            putSQLParams(pstmt, valueList);
            int affect = pstmt.executeUpdate();
            
            return affect;
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.execute -> SQLException: " + e.toString());
            System.out.println("DbUtil.execute -> sql: " + sql);
            throw e;
        }
        // 關閉連結
        finally
        {
            close(rs);
            close(pstmt);
        }
    }
    

    /**
     * 執行無傳回值的 SQL，如 INSERT、UPDATE 或 DELETE 等
     * @param conn 資料庫連結
     * @param sql 操作資料庫的SQL
     * @return int 新增、修改或刪除 等所影響的筆數
     * @throws SQLException 資料庫操作例外
     */
    public static int execute(Connection conn, String sql) throws SQLException
    {
        return execute(conn, sql, null);
    }
    

    /**
     * 傳遞參數到 PreparedStatement, 僅供本類內部使用 (註:此方法有待改善,目前僅字串且不為空情況下測試過)
     * @param pstmt PreparedStatement
     * @param valueList 參數列表
     * @return PreparedStatement連結
     * @throws SQLException 資料庫操作例外
     */
    public static PreparedStatement putSQLParams(PreparedStatement pstmt, List<Object> valueList) throws SQLException
    {
        try
        {
            // 查詢的參數資料集的筆數
            int valueListSize = (valueList == null) ? 0 : valueList.size();
            for ( int i = 0; i < valueListSize; i++ )
            {
                // 取得參數
                Object obj = valueList.get(i);
                // 根據參數類型來賦值
                if ( obj == null )
                    pstmt.setNull(i + 1, java.sql.Types.NULL);
                else if ( obj instanceof String )
                    pstmt.setString(i + 1, "" + valueList.get(i));
                else if ( obj instanceof Integer )
                    pstmt.setInt(i + 1, Integer.parseInt("" + obj));
                else if ( obj instanceof Long )
                    pstmt.setLong(i + 1, Long.parseLong("" + obj));
                else if ( obj instanceof Double )
                    pstmt.setDouble(i + 1, Double.parseDouble("" + obj));
                else if ( obj instanceof java.util.Date )
                    pstmt.setDate(i + 1, new java.sql.Date(((java.util.Date)obj).getTime()));
                else if ( obj instanceof java.sql.Blob )
                    pstmt.setBlob(i + 1, (java.sql.Blob)obj);
                else
                    pstmt.setObject(i + 1, obj);
            }
        }
        catch ( SQLException e )
        {
            // 輸出例外
            System.out.println("DbUtil.putSQLParams -> SQLException: " + e.toString());
            throw e;
        }
        return pstmt;
    }


    /**
     * 轉成字串
     * @param sour 待轉資料
     * @param init 預設值
     * @param length 截取字串長度,超過此長度者,後面顯示“...”;不希望截取可填入0
     * @return String 字串
     */
    public static String toString(Object sour, String init, int length)
    {
        // 初始化
        String dest = null;
        try
        {
            dest = (String)sour;
            // 為空
            if ( dest == null || "".equals(dest) )
            {
                dest = init;
            }
        }
        // 拋出例外
        catch ( Exception e )
        {
            dest = init;
        }
        // 需要截取時(由於超出部分顯示“...”，所以必須3位以上才截取)
        if ( length > 0 && dest != null && dest.length() > length && dest.length() > 3 )
        {
            dest = dest.substring(0, length - 3) + "...";
        }
        return dest;
    }
    

    /**
     * 轉成字串
     * @param sour 待轉資料
     * @param init 預設值
     * @return String 字串
     */
    public static String toString(Object sour, String init)
    {
        return toString(sour, init, 0);
    }
    

    /**
     * 轉成字串
     * @param sour 待轉資料
     * @return String 字串
     */
    public static String toString(Object sour)
    {
        return toString(sour, "");
    }
    

    /**
     * 取得物件裏的值
     * @param sour 儲存著所需內容的物件，可以是 Map, HttpServletRequest, HttpSession, ResultSet 類別
     * @param name 對應的值的名稱，如果這個值為null，則認為 sour 本身是值
     * @param init 預設值，如果沒有取得對應值時，則傳回此值
     * @param length 截取字串長度,超過此長度者,後面顯示“...”;不希望截取可填入0
     * @return String 物件裏的值的字串
     */
    public static String getValue(Object sour, String name, String init, int length)
    {
        // 如果變數 sour 為空
        if ( sour == null )
        {
            return toString(init, init, length);
        }
        // 如果變數 name 為空，則認為 sour 本身是值
        if ( name == null )
        {
            return toString(sour, init, length);
        }
        
        // 根據 sour 的不同類型來取值
        try
        {
            // 如果 sour 是 String 類別的物件
            if ( sour instanceof String )
            {
                return toString(sour, init, length);
            }
            // 如果 sour 是 Map 類別的物件
            if ( sour instanceof Map )
            {
                Map<String, String> map = (Map)sour;
                // 如果此映射包含指定鍵的映射關系
                if ( map.containsKey(name) )
                {
                    return toString(map.get(name), init, length);
                }
                // 查詢資料庫時，鍵都會自動轉成小寫
                if ( map.containsKey(name.toLowerCase()) )
                {
                    return toString(map.get(name.toLowerCase()), init, length);
                }
                // 如果此映射不包含指定鍵的映射關系，則忽略大小寫來取值
                for ( Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator(); iter.hasNext(); )
                {
                    Map.Entry<String, String> entry = iter.next();
                    // 如果鍵忽略大小寫時與 name 相同
                    if ( name.equalsIgnoreCase(toString(entry.getKey())) )
                    {
                        return toString(entry.getValue(), init, length);
                    }
                }
            }
            // 如果 sour 是 HttpServletRequest 類別的物件
            if ( sour instanceof HttpServletRequest )
            {
                // Parameter裡面有,則取Parameter的
                if ( ((HttpServletRequest)sour).getParameterMap().containsKey(name) )
                {
                    return toString(((HttpServletRequest)sour).getParameter(name), init, length);
                }
                // 如果 Parameter 不包含對應的名稱; 取Attribute的
                return toString(((HttpServletRequest)sour).getAttribute(name), init, length);
            }
            // 如果 sour 是 HttpSession 類別的物件
            if ( sour instanceof HttpSession )
            {
                return toString(((HttpSession)sour).getAttribute(name), init, length);
            }
            // 如果 sour 是 ResultSet 類別的物件
            if ( sour instanceof ResultSet )
            {
                return toString(((ResultSet)sour).getString(name), init, length);
            }
            // 如果 sour 不是上述類別的物件，則認為它自身是值。
            return toString(sour, init, length);
        }
        // 取值時出現例外
        catch ( Exception e )
        {
            return toString(init, init, length);
        }
    }
    

    /**
     * 取得物件裏的值
     * @param sour 儲存著所需內容的物件，可以是 Map, HttpServletRequest, HttpSession, ResultSet 類別
     * @param name 對應的值的名稱，如果這個值為空，則認為 sour 本身是值
     * @param init 預設值，如果沒有取得對應值時，則傳回此值
     * @return String 物件裏的值的字串
     */
    public static String getValue(Object sour, String name, String init)
    {
        return getValue(sour, name, init, 0);
    }
    

    /**
     * 取得物件裏的值的字串
     * @param sour 儲存著所需內容的物件，可以是 Map, HttpServletRequest, HttpSession, ResultSet 類別
     * @param name 對應的值的名稱，如果這個值為null，則認為 sour 本身是值
     * @return String 物件裏的值的字串。取不到值，或者值為 null 則傳回""
     */
    public static String getValue(Object sour, String name)
    {
        return getValue(sour, name, "");
    }


    /**
     * 將需顯示的字串轉換成 textarea 顯示的字串
     * @param sour 需要進行轉換的物件
     * @return 轉換後的字串
     */
    public static String toTextarea(String sour)
    {
        // 為空時
        if ( sour == null || "".equals(sour) )
        {
            return "";
        }
        
        // 以下逐一轉換
        sour = sour.replaceAll("&", "&amp;");
        sour = sour.replaceAll("<", "&lt;");
        sour = sour.replaceAll(">", "&gt;");
        return sour;
    }
    

    /**
     * 將需顯示的字串轉換成HTML格式的字串
     * @param sour 需要進行轉換的物件
     * @return 轉換後的字串
     */
    public static String toHtml(String sour)
    {
        // 為空時
        if ( sour == null || "".equals(sour) )
        {
            return "";
        }
        
        // 以下逐一轉換
        sour = sour.replaceAll("&", "&amp;");
        sour = sour.replaceAll(" ", "&nbsp;");
        sour = sour.replaceAll("%", "&#37;");
        sour = sour.replaceAll("<", "&lt;");
        sour = sour.replaceAll(">", "&gt;");
        sour = sour.replaceAll("\n", "\n<br/>");
        sour = sour.replaceAll("\"", "&quot;");
        sour = sour.replaceAll("'", "&#39;");
        sour = sour.replaceAll("[+]", "&#43;");
        return sour;
    }
    

    /**
     * 將需顯示的物件轉換成HTML格式的字串
     * @param sour 需要進行轉換的物件
     * @return 轉換後的字串
     */
    public static String toHtml(Object sour)
    {
        // 為空時
        if ( sour == null || "".equals(sour) )
        {
            return "";
        }
        return toHtml(toString(sour));
    }

%><%!

/**
 * 分頁元件 <br />
 * 注意: <br />
 * 1.上下分頁模板必須放在form表單元件中,否則會有JS錯誤; <br />
 * 2.占用的名稱全部使用“_PU_”開頭,為避免名稱衡突,請勿使用這個開頭的名稱 <br />
 * 3.SQL Server版本的需設置主鍵為第一列 (如:select Id, name from userTable where ...),其中Id為主鍵. <br />
 *   多主鍵,可採用下面方法(select a.Id+a.name as keyId from userTable a where ... )). <br />
 * 4.使用AJAX查詢時, 請在 genPageHtml(check, beforeSubmit)方法的beforeSubmit裡面的最後加上return,避免submit <br />
 * 5.排序列,可使用  onClick="_PU_.sort('排序列');" <br />
 * 6.設定頁數 和 設定每頁顯示多少筆,必須在取分頁條HTML和取查詢SQL之前執行 <br />
 * @author <A HREF='daillow@gmail.com'>Holer</A>
 * @version 0.1
 */
class PageUtil
{
    // Connection
    private Connection conn = null;
    // sql
    private String sql = "";
    // 分頁SQL
    private String pageSql = "";
    // countSQL
    private String countSql = "";
    // 記錄總數
    private int rowCount = 0;
    // 一頁顯示的記錄數，預設為10
    private int pageSize = 10;
    // 總頁數
    private int pageCount = 0;
    // 待顯示頁碼
    private int pageNo = 0;
    // 記錄是否為空
    private boolean pageSizeIsNull;
    // 從request中獲取要排序的欄位名稱, 如item_id或item_ID desc
    private String sortFields = "";
    // 控制ORDER by asc, order by desc用
    private String sortAscOrDesc = "";
    // 資料庫類型
    private String DbType = "mysql";
    // 查詢的參數
    private List<Object> valueList = null;
    // 是否準備好
    private boolean isPrepare = false;
    // 輸出分頁條的次數
    private int outTimes = 0;
    
    /**
     * 從哪筆開始
     */
    public int StartNo = 0;
    
    /**
     * 按鈕及輸入框html,可直接在此修改
     */
    // 第一頁
    private String button_first = "<INPUT NAME='_PU_button' TYPE='button' VALUE='|<' CLASS='_PU_btn' onClick='_PU_.setPageNo(1, true);' ${disabled} />";
    // 上一頁
    private String button_pre = "<INPUT NAME='_PU_button' TYPE='button' VALUE='<' CLASS='_PU_btn' onClick='_PU_.setPageNo(${thisPageNo}, true);' ${disabled} />";
    // 下一頁
    private String button_next = "<INPUT NAME='_PU_button' TYPE='button' VALUE='>' CLASS='_PU_btn' onClick='_PU_.setPageNo(${thisPageNo}, true);' ${disabled} />";
    // 最後一頁
    private String button_last = "<INPUT NAME='_PU_button' TYPE='button' VALUE='>|' CLASS='_PU_btn' onClick='_PU_.setPageNo(${pageCount}, true);' ${disabled} />";
    // go按鈕
    private String button_go = "<INPUT NAME='_PU_button' TYPE='button' VALUE='GO' CLASS='_PU_btn' onClick='_PU_.submit();' ${disabled} />";
    // 每頁多少筆的輸入框
    private String text_size = "<INPUT NAME='_PU_text_size' TYPE='text' CLASS='_PU_text' SIZE='2' MAXLENGTH='5' VALUE='${pageSize}' ${disabled} onChange='_PU_.textSize_change(this);' onkeydown='return _PU_.input(event);' />";
    // 跳轉到第幾頁的輸入框
    private String text_pageNo = "<INPUT NAME='_PU_text_pageNo' TYPE='text' CLASS='_PU_text' SIZE='2' MAXLENGTH='5' VALUE='${pageNo}' ${disabled} onChange='_PU_.textPageNo_change(this);' onkeydown='return _PU_.input(event);' />";
    
    
    /**
     * 構造方法
     * @param DBconn Connection
     * @param request HttpServletRequest
     * @param sql 分頁傳入的SQL
     * @param valueList SQL的參數列表
     * @param DbType 資料庫類型
     */
    public PageUtil(Connection DBconn, HttpServletRequest request, String sql, List<Object> valueList, String DbType)
    {
        // 數據庫連結參數
        this.conn = DBconn;
        
        // 取得待顯示頁碼
        this.pageNo = request.getParameter("_PU_pageNo") == null ? 1 : Integer.parseInt(request
                .getParameter("_PU_pageNo"));
        // 設定和取得頁大小
        this.pageSize = request.getParameter("_PU_pageSize") == null ? pageSize : Integer.parseInt(request
                .getParameter("_PU_pageSize"));
        // 是否為null
        this.pageSizeIsNull = request.getParameter("_PU_pageSize") == null;
        
        // 要排序的欄位名稱
        this.sortFields = request.getParameter("_PU_sortFields") == null ? "" : request
                .getParameter("_PU_sortFields");
        // 控制ORDER by asc, order by desc用
        this.sortAscOrDesc = request.getParameter("_PU_sortAscOrDesc") == null ? "" : request
                .getParameter("_PU_sortAscOrDesc");
        
        // 資料庫類型
        setDbType(DbType);
        
        // 設定查詢SQL
        this.sql = (sql == null || "".equals(sql.trim())) ? "" : sql;
        this.valueList = valueList;
    }
    
    
    /**
     * 構造方法
     * @param DBconn Connection
     * @param request HttpServletRequest
     * @param sql 分頁傳入的SQL
     * @param valueList 分頁用的SQL的參數列表
     */
    public PageUtil(Connection DBconn, HttpServletRequest request, String sql, List<Object> valueList)
    {
        this(DBconn, request, sql, valueList, null);
    }
    
    
    /**
     * 構造方法
     * @param DBconn Connection
     * @param request HttpServletRequest
     * @param sql 分頁傳入的SQL
     */
    public PageUtil(Connection DBconn, HttpServletRequest request, String sql)
    {
        this(DBconn, request, sql, null, null);
    }
    
    
    /**
     * 取得分頁的SQL
     * @return 分頁的SQL
     */
    public String getSQL()
    {
        return this.sql;
    }
    
    
    /**
     * 取得資料庫類型
     * @return 資料庫類型
     */
    public String getDbType()
    {
        return this.DbType;
    }
    
    
    /**
     * 設定資料庫類型
     * @param DbType 資料庫類型
     */
    public void setDbType(String DbType)
    {
        // 如果已經準備好,則不可以再改變資料庫類型
        checkPrepare();
        // 沒有參數時,由資料庫連接來取
        if ( DbType == null || "".equals(DbType.trim()) )
        {
            String driverName = this.conn.toString().toLowerCase();
            //MySQL 資料庫
            if ( driverName.indexOf("mysql") >= 0 )
            {
                this.DbType = "mysql";
            }
            //Oracle 資料庫
            else if ( driverName.indexOf("oracle") >= 0 )
            {
                this.DbType = "oracle";
            }
            //SQL Server 資料庫 跟 Access 資料庫, 同樣的分頁SQL
            else if ( driverName.indexOf("sqlserver") >= 0 || driverName.indexOf("access") >= 0 )
            {
                this.DbType = "sqlserver";
            }
        }
        // 有參數傳入,則使用參數的
        else
        {
            this.DbType = DbType.toLowerCase();
        }
    }
    

    /**
     * 設定查詢總計多少筆資料的SQL
     * @exception 當select里有distinct的時,可能會出錯
     */
    private void setCountSql()
    {
        String sql = this.sql.trim().toLowerCase();
        // 如果只有一個 from; 直接修改SQL
        if ( sql.indexOf(" from ") == sql.lastIndexOf(" from ") && sql.indexOf(" distinct ") < 0 )
        {
            this.countSql = "SELECT COUNT(1) " + this.sql.substring(sql.indexOf(" from "));
        }
        else
        {
            this.countSql = "SELECT COUNT(1) FROM (" + this.sql + ") _PU_A";
        }
    }
    
    
    /**
     * 取得SQL
     * @return 已經產的SQL
     */
    public String getCountSql()
    {
        return this.countSql;
    }
    
    
    /**
     * 取得分頁SQL
     * @return 分頁SQL
     */
    public String getPageSql()
    {
        // 準備好
        setPrepare();
        return this.pageSql;
    }
    
    
    /**
     * 處理select command裡有distinct和group by command以及排序
     */
    private void setPageSql()
    {
        // 傳入的SQL
        String strSql = this.sql.trim();
        // 設定頁碼(防呆)
        this.pageNo = (this.pageNo <= 0 ? 1 : this.pageNo);
        
        // 表示需要排序
        if ( !"".equals(this.sortFields) )
        {
            // 排序欄位和排序方向
            String order = this.sortFields + " " + this.sortAscOrDesc;
            
            // Mysql
            if ( "mysql".equals(this.DbType) )
            {
                int startRow = this.pageSize * (this.pageNo - 1);
                String tem_sql = strSql.toLowerCase().replaceAll("\\s+", " ");
                // 已經有 limit
                if ( tem_sql.indexOf(" limit ") > 0 )
                {
                    // 此句效率很低,非迫不得已則不使用
                    this.pageSql = " SELECT _PU_.* from (" + strSql + " ) _PU_ order by " + order + " limit "
                        + startRow + "," + this.pageSize;
                }
                // 已經有 order by
                else if ( tem_sql.indexOf(" order by ") > 0 )
                {
                    //  order by 後面沒有括號(即order by 不是子查詢),直接在後面補上排序
                    if ( tem_sql.lastIndexOf(")") < tem_sql.lastIndexOf(" order by ") )
                    {
                        this.pageSql = strSql + ", " + order + " limit " + startRow + "," + this.pageSize;
                    }
                    // order by 在子查詢裡面
                    else
                    {
                        this.pageSql = " SELECT _PU_.* from (" + strSql + " ) _PU_ order by "
                            + order + " limit " + startRow + "," + this.pageSize;
                    }
                }
                // 沒有 order by 和 limit,則直接在後面加上, 這樣效率更高
                else
                {
                    this.pageSql = strSql + " order by " + order + " limit " + startRow + "," + this.pageSize;
                }
            }
            // Sql Server2000
            else if ( "sqlserver".equals(this.DbType) )
            {
                // 顯示行數,最後一頁時不是 pageSize
                int showRows = (this.pageNo < this.pageCount ? this.pageSize : this.rowCount - (this.pageNo-1) * this.pageSize);
                // 查無資料時, top 0 會報錯
                showRows = (showRows > 0 ? showRows : 1);
                this.pageSql = "SELECT * FROM( SELECT TOP " + showRows + " * FROM (" 
                    + " SELECT TOP " + (this.pageSize * this.pageNo) + " * FROM ("
                    + strSql + ") AS _PU_A ORDER BY 1 ASC "
                    + ") AS _PU_B ORDER BY 1 DESC )  AS _PU_C ORDER BY " + order;
            }
            // Oracle
            else if ( "oracle".equals(this.DbType) )
            {
                // 已經有 order by 
                if ( strSql.toLowerCase().indexOf(" order ") > 0 )
                {
                    // 將 strSql ==> select a.* from ( strSql ) a ORDER BY 1,2
                    strSql = "select _PU_A.* from (" + strSql + ") _PU_A order by " + order;
                }
                // 沒有 order by,直接在後面加上, 這樣效率會更高
                else
                {
                    strSql += " order by " + order;
                }
                int startRow = (this.pageNo - 1) * this.pageSize + 1;
                int endRow = this.pageNo * this.pageSize;
                // 需要排序sql
                this.pageSql = "SELECT * FROM ( SELECT ROWNUM as _PU_ROWNUM, _PU_B.* from ("
                    + strSql + ") _PU_B where _PU_ROWNUM <= " + endRow 
                    + " ) WHERE _PU_ROWNUM >= " + startRow;
            }
        }
        // 表示不需要排序
        else
        {
            // Mysql
            if ( "mysql".equals(this.DbType) )
            {
                int startRow = this.pageSize * (this.pageNo - 1);
                // 已經有 limit
                if ( strSql.toLowerCase().indexOf(" limit ") > 0 )
                {
                    this.pageSql = "SELECT _PU_.* from (" + strSql + " ) _PU_ limit "
                        + startRow + "," + this.pageSize;
                }
                // 沒有 limit,則直接在後面加上, 這樣效率更高
                else
                {
                    this.pageSql = strSql + " limit " + startRow + "," + this.pageSize;
                }
            }
            // Sql Server 2000
            else if ( "sqlserver".equals(this.DbType) )
            {
                // 顯示行數,最後一頁時不是 pageSize
                int showRows = (this.pageNo < this.pageCount ? this.pageSize : this.rowCount - (this.pageNo-1) * this.pageSize);
                // 查無資料時, top 0 會報錯
                showRows = (showRows > 0 ? showRows : 1);
                this.pageSql = "SELECT * FROM( SELECT TOP " + showRows + " * FROM (" 
                    + " SELECT TOP " + (this.pageSize * this.pageNo) + " * FROM ("
                    + strSql + ") AS _PU_A ORDER BY 1 ASC ) AS _PU_B ORDER BY 1 DESC ) as _PU_C ORDER BY 1 ASC";
            }
            // Oracle
            else if ( "oracle".equals(this.DbType) )
            {
                int startRow = (this.pageNo - 1) * this.pageSize + 1;
                int endRow = this.pageNo * this.pageSize;
                this.pageSql = "SELECT * FROM ( SELECT ROWNUM as _PU_ROWNUM, _PU_A.* from ("
                    + strSql + ") _PU_A WHERE _PU_ROWNUM <= " + endRow + " ) WHERE _PU_ROWNUM >= " + startRow;
            }
        }
    }
    
    
    /**
     * 取得每頁產生的記錄數
     * @return 每頁產生的記錄數
     */
    public int getPageSize()
    {
        return this.pageSize;
    }
    
    
    /**
     * 設定一頁顯示多少筆資料
     * @param i 一頁顯示多少筆資料
     */
    public void setPageSize(int i)
    {
        // 如果已經準備工作,則不可以再改變一頁顯示多少筆資料
        checkPrepare();
        // 如果沒有傳入顯示多少筆資料的參數
        if ( this.pageSizeIsNull )
        {
            this.pageSize = i;
        }
    }
    
    
    /**
     * 取得頁數
     * @return 頁數
     */
    public int getPageCount()
    {
        return this.pageCount;
    }
    
    
    /**
     * 取得第幾頁
     * @return 第幾頁
     */
    public int getPageNo()
    {
        return this.pageNo;
    }
    
    
    /**
     * 設定頁數
     * @param i 頁數
     */
    public void setPageNo(int pageNo)
    {
        // 如果已經準備工作,則不可以再改變頁數
        checkPrepare();
        this.pageNo = pageNo;
    }
    
    
    /**
     * 設定頁數為最大頁
     */
    public void setPageNoMax()
    {
        setPrepare();
        this.pageNo = this.pageCount;
    }
    
    
    /**
     * 取得顯示幾行
     * @return 顯示幾行
     */
    public int getRowCount()
    {
        return this.rowCount;
    }
    
    
    /**
     * 取得目前頁基准行號
     * @return 顯示幾行
     */
    public int getBaseRowNo()
    {
        return getPageSize() * (getPageNo() - 1);
    }
    
    
    /**
     * 檢查工作是否已經準備好
     * @throws RuntimeException 已經準備好則拋出例外,可以不捕獲的例外
     */
    private void checkPrepare()
    {
        // 如果已經準備好,則不可以再改變
        if ( this.isPrepare )
        {
            System.out.println("分頁SQL已經使用,不可再修改; 若要修改,請在genPageHtml(), getPageSql() 方法之前");
            throw new RuntimeException();
        }
    }
    
    
    /**
     * 產生分頁的準備工作
     * @return 成功傳回true,失敗傳回false
     */
    private void setPrepare()
    {
        // 如果已經準備好,不再執行
        if ( this.isPrepare )
            return;
        
        setCountSql();
        try
        {
            // 產生SQL表達式
            PreparedStatement pstmt = this.conn.prepareStatement(this.countSql);
            ResultSet rs = null;
            // 查詢的參數資料集的筆數
            int valueListSize = (this.valueList == null) ? 0 : this.valueList.size();
            // 如果需要傳參數
            for ( int i = 0; i < valueListSize; i++ )
            {
                // 取得參數
                Object obj = this.valueList.get(i);
                // 根據參數類型來賦值
                if ( obj == null )
                    pstmt.setNull(i + 1, java.sql.Types.NULL);
                else if ( obj instanceof String )
                    pstmt.setString(i + 1, "" + valueList.get(i));
                else if ( obj instanceof Integer )
                    pstmt.setInt(i + 1, Integer.parseInt("" + obj));
                else if ( obj instanceof Long )
                    pstmt.setLong(i + 1, Long.parseLong("" + obj));
                else if ( obj instanceof Double )
                    pstmt.setDouble(i + 1, Double.parseDouble("" + obj));
                else if ( obj instanceof java.util.Date )
                    pstmt.setDate(i + 1, new java.sql.Date(((java.util.Date)obj).getTime()));
                else if ( obj instanceof java.sql.Blob )
                    pstmt.setBlob(i + 1, (java.sql.Blob)obj);
                else
                    pstmt.setObject(i + 1, obj);
            }
            rs = pstmt.executeQuery();
            if ( rs.next() )
            {
                // 記錄的總數
                this.rowCount = rs.getInt(1);
                this.pageCount = (this.rowCount + this.pageSize - 1) / this.pageSize;
            }
            
            // 沒有資料時重設pageNo為0
            this.pageNo = this.pageNo > this.pageCount ? this.pageCount : this.pageNo;
            this.pageNo = this.pageNo < 1 ? 1 : this.pageNo;
            this.pageNo = this.rowCount <= 0 ? 0 : this.pageNo;
            // 開始序號
            this.StartNo = (this.pageNo - 1) * this.pageSize;
            setPageSql();
            
            // 關閉資源
            rs.close();
            pstmt.close();
            this.isPrepare = true;
        }
        // 例外
        catch ( Exception e )
        {
            // 第幾頁
            this.pageNo = 0;
            // 共幾頁
            this.pageCount = 0;
            // 顯示幾頁
            this.rowCount = 0;
            e.printStackTrace();
            this.isPrepare = false;
        }
    }
    
    
    /**
     * 產生一個分頁導航條的HTML,不區分上下分頁條
     * @return 傳回分頁導航條的HTML內容
     */
    public String genPageHtml()
    {
        return genPageHtml("", "");
    }
    
    
    /**
     * 產生一個分頁導航條的HTML,不區分上下分頁條
     * @param beforeSubmit 處理送出前的Java Script,建議是送出的(如果使用 AJAX, 請在最後加上return,避免送出)
     * @return 傳回分頁導航條的HTML內容
     */
    public String genPageHtml(String beforeSubmit)
    {
        return genPageHtml("", beforeSubmit);
    }
    
    
    /**
     * 產生一個分頁導航條的HTML, 不區分上下分頁條
     * @param check 處理送出前的Java Script,建議是表單檢查的
     * @param beforeSubmit 處理送出前的Java Script,建議是送出的(如果使用 AJAX, 請在最後加上return,避免送出)
     * @return 傳回分頁導航條的HTML內容
     */
    public String genPageHtml(String check, String beforeSubmit)
    {
        // 準備工作
        setPrepare();
        
        // 導航條的html
        StringBuffer sList = new StringBuffer();
        sList.append("<DIV ID='_PU_pageControl" + this.outTimes + "'> \r\n");
        sList.append("<TABLE width='100%' cellspacing='0' cellpadding='0' border='0' class='_PU_table' id='_PU_table'> \r\n");
        sList.append("<TBODY><TR ALIGN='center'> \r\n");
        
        // 第一欄位(內容為: 每頁多少筆)
        sList.append("<TD align='left'>&nbsp;${EverPage}");
        // 輸入框:每頁多少筆
        sList.append(this.text_size.replace("${pageSize}", this.pageSize + "")
                .replace("${disabled}", ((this.pageCount <= 0) ? "DISABLED" : "")));
        sList.append("${ROW}&nbsp;</TD> \r\n");
        
        // 第二欄位(第1~10筆/共74筆)
        sList.append("<TD align='center' ID='_PU_TDRowCount" + this.outTimes + "'>&nbsp;${ORDER}<em>");
        sList.append(this.pageNo > 0 ? (this.rowCount > 0 ? ((this.pageNo - 1) * this.pageSize + 1) : 0) : this.pageNo);
        sList.append("</em>~<em>");
        sList.append((this.pageNo * this.pageSize > this.rowCount) ? this.rowCount : (this.pageNo * this.pageSize));
        sList.append("</em>${ROW}/${TOTAL}<em>");
        sList.append(this.rowCount);
        sList.append("</em>${ROW}&nbsp;</TD> \r\n");
        
        // 第三欄位(按鈕,及第幾頁輸入框)
        sList.append("<TD align='right'> \r\n");
        // 按鈕:第一頁
        sList.append(this.button_first.replace("${disabled}", ((this.pageNo == 1) ? "DISABLED" : "")));
        sList.append("&nbsp; \r\n");
        // 按鈕:上一頁
        sList.append(this.button_pre.replace("${disabled}", ((this.pageNo <= 1) ? "DISABLED" : ""))
                .replace("${thisPageNo}", (this.pageNo - 1) + ""));
        sList.append("&nbsp; \r\n");
        // 輸入框:第幾頁
        sList.append("&nbsp;${ORDER}");
        sList.append(this.text_pageNo.replace("${pageNo}", this.pageNo + "")
                .replace("${disabled}", ((this.pageCount <= 0) ? "DISABLED" : "")));
        // 共多少頁
        sList.append("${PAGE}/${TOTAL}<em>");
        sList.append(this.pageCount > 0 ? this.pageCount : 1);
        sList.append("</em>${PAGE}&nbsp;");
        // 按鈕:下一頁
        sList.append(this.button_next.replace("${disabled}", ((this.pageNo >= this.pageCount) ? "DISABLED" : ""))
                .replace("${thisPageNo}", (this.pageNo + 1) + ""));
        sList.append("&nbsp; \r\n");
        // 按鈕:最後一頁
        sList.append(this.button_last.replace("${disabled}", ((this.pageNo >= this.pageCount) ? "DISABLED" : ""))
                .replace("${pageCount}", "" + this.pageCount));
        sList.append("&nbsp; \r\n");
        // 按鈕:送出(go)
        sList.append(this.button_go.replace("${disabled}", ((this.pageCount <= 0) ? "DISABLED" : "")));
        sList.append("</TR></TBODY></TABLE></DIV>");
        
        
        // 如果還沒有這段的時候輸出,有則不輸出
        if ( this.outTimes == 0 )
        {
            // 儲存訊息的隱藏域
            sList.append("<INPUT TYPE='hidden' NAME='_PU_sortFields' ID='_PU_sortFields' VALUE='" + this.sortFields + "'/> \r\n ");
            sList.append("<INPUT TYPE='hidden' NAME='_PU_sortAscOrDesc' ID='_PU_sortAscOrDesc' VALUE='" + this.sortAscOrDesc + "'/> \r\n ");
            sList.append("<INPUT TYPE='hidden' NAME='_PU_pageNo' ID='_PU_pageNo' VALUE='" + this.pageNo + "'/> \r\n ");
            sList.append("<INPUT TYPE='hidden' NAME='_PU_pageSize' ID='_PU_pageSize' VALUE='" + this.pageSize + "'/> \r\n ");
            
            // js 函數
            sList.append(" <SCRIPT type='text/javascript' language='JavaScript'> \r\n");
            // js 分頁公用類,所有的js函數都包括在這類別裡面,以免佔用過多關鍵字
            sList.append(" var _PU_ = new Object(); \r\n");
            
            /**
             * 送出
             * @param pageNo 送出的頁碼
             * @param pageSize 每頁顯示多少筆
             */
            sList.append(" _PU_.submit = function () { \r\n");
            // js驗證
            check = (check == null || check.trim().length() == 0) ? "" : check + "; \r\n";
            // 先執行檢查的 js
            sList.append(check);
            // 讓按鈕不可點選
            sList.append("   var btnName = window.document.getElementsByName('_PU_button'); \r\n");
            sList.append("   for ( var i=0; i < btnName.length; i++ ) { btnName[i].disabled=true; } \r\n");
            // 讓每頁多少筆輸入框不可再輸入
            sList.append("   var text_sizeName = window.document.getElementsByName('_PU_text_size'); \r\n");
            sList.append("   for ( var i=0; i < text_sizeName.length; i++ ) { text_sizeName[i].disabled=true; } \r\n");
            // 讓第幾頁輸入框不可再輸入
            sList.append("   var text_pageNoName = window.document.getElementsByName('_PU_text_pageNo'); \r\n");
            sList.append("   for ( var i=0; i < text_pageNoName.length; i++ ) { text_pageNoName[i].disabled=true; } \r\n");
            // 執行送出前的動作(也可以是ajax送出)
            sList.append((beforeSubmit == null || beforeSubmit.trim().length() == 0) ? "" : beforeSubmit + "; \r\n");
            // 送出
            sList.append("   window.document.getElementById('_PU_pageSize').form.submit(); \r\n");
            sList.append("}; \r\n");
            
            /**
             * 設置頁碼
             * @param pageNo 跳轉到的頁碼
             * @param isSubmit 是否送出,為true時設定頁碼後送出,否則只設定頁碼不送出
             */
            sList.append(" _PU_.setPageNo = function (pageNo, isSubmit) { \r\n");
            // 設定頁碼
            sList.append("    window.document.getElementById('_PU_pageNo').value = parseInt(pageNo); \r\n");
            sList.append("    if ( true === isSubmit ) { this.submit(); } \r\n");
            sList.append(" }; \r\n");
            
            /**
             * 設定每頁顯示多少筆
             * @param pageSize 每頁顯示多少筆
             * @param isSubmit 是否送出,為true時設定顯示數後送出,否則只設定不送出
             */
            sList.append(" _PU_.setPageSize = function (pageSize, isSubmit) { \r\n");
            sList.append("    if ( 0 < parseInt(pageSize) ) { window.document.getElementById('_PU_pageSize').value=parseInt(pageNo); } \r\n");
            // 設定跳轉到第幾頁
            sList.append("    window.document.getElementById('_PU_pageSize').value = parseInt(pageSize); \r\n");
            sList.append("    if ( true === isSubmit ) { this.submit(); } \r\n");
            sList.append(" }; \r\n");
            
            /**
             * 過濾按鍵,只允許輸入數字
             * @param event 事件 (為兼容 IE 和 FireFox)
             * @example <input type="text" onkeydown="return _PU_.input(event);"/>
             */
            sList.append(" _PU_.input = function (event) { \r\n");
            // 兼容 IE 和 FireFox
            sList.append("   event = event || window.event; \r\n");
            // 不讓按下 Shift
            sList.append("   if ( event.shiftKey === true ) { return false; } \r\n");
            // 按下的鍵的編碼
            sList.append("   var code = event.charCode || event.keyCode; \r\n");
            // 輸入數字
            sList.append("   if ( (code >= 48 && code <= 57) || (code >= 96 && code <= 105) || ");
            // 輸入刪除按鍵,左右按鍵
            sList.append("     code === 8  || code === 46 || code === 39 || code === 37  ) { return true; } \r\n");
            // Enter 鍵,先執行 onchange 事件,再送出
            sList.append("   if ( code === 13 ) { \r\n");
            // 取得事件源
            sList.append("      var source = event.target || event.srcElement; \r\n");
            // 事件源的名稱
            sList.append("      var name = source.getAttribute('name'); \r\n");
            // 執行每頁多少筆的輸入框的 onchange 事件
            sList.append("      if ( '_PU_text_size' === name ) { this.textSize_change(source); } \r\n");
            // 執行跳轉到第幾頁的輸入框的 onchange 事件
            sList.append("      else if ( '_PU_text_pageNo' === name ) { this.textPageNo_change(source); } \r\n");
            // 送出
            sList.append("      this.submit(); \r\n");
            sList.append("   } \r\n");
            // 其它按鍵,不讓輸入
            sList.append("   return false; \r\n");
            sList.append(" }; \r\n");
            
            /**
             * 每頁多少筆的輸入框的 onchange 事件
             * @param inputer 輸入框物件
             */
            sList.append(" _PU_.textSize_change = function(inputer) { \r\n");
            // 設值,輸入不正確時預設為原本的值
            sList.append("   inputer.value = (parseInt(inputer.value) || " + this.pageSize + " ); \r\n");
            // 設值,總體的值
            sList.append("   window.document.getElementById('_PU_pageSize').value = inputer.value; \r\n");
            // 設值,其它的每頁多少筆的輸入框的值
            sList.append("   var text_size = window.document.getElementsByName('_PU_text_size'); \r\n");
            sList.append("   for ( var i = 0; i < text_size.length; i++ ) { text_size[i].value = inputer.value; } \r\n");
            sList.append(" }; \r\n");
            
            /**
             * 跳轉到第幾頁的輸入框的 onchange 事件
             * @param inputer 輸入框物件
             */
            sList.append(" _PU_.textPageNo_change = function(inputer) { \r\n");
            // 設值,輸入不正確時預設為原本的值
            sList.append("   inputer.value = (parseInt(inputer.value) || " + this.pageNo + " ); \r\n");
            // 設值,總體的值
            sList.append("   window.document.getElementById('_PU_pageNo').value = inputer.value; \r\n");
            // 設值,其它的每頁多少筆的輸入框的值
            sList.append("   var text_pageNo = window.document.getElementsByName('_PU_text_pageNo'); \r\n");
            sList.append("   for ( var i = 0; i < text_pageNo.length; i++ ) { text_pageNo[i].value = inputer.value; } \r\n");
            sList.append(" }; \r\n");
            
            /**
             * 調節寬度, 寬度不夠時隱藏 “第1~6筆/共6筆” 這個欄位
             * @param outTime 第幾個分頁導航條
             */
            sList.append(" _PU_.autoWidth = function(outTime) { \r\n");
            sList.append("   if ( window.document.getElementById('_PU_pageControl' + outTime).offsetHeight > 30 ) { \r\n");
            sList.append("      window.document.getElementById('_PU_TDRowCount' + outTime).style.display = 'none'; \r\n");
            sList.append("   } \r\n");
            sList.append(" }; \r\n");
            
            /**
             * 排序方法(供頁面引用)
             * @param filed 排序的欄位
             */
            sList.append(" _PU_.sort = function(filed) { \r\n");
            sList.append("   if ( !filed ) { return; } \r\n");
            // 排序欄位
            sList.append("   var sortElement = window.document.getElementById('_PU_sortFields'); \r\n");
            sList.append("   var preSortValue = sortElement.value; \r\n");
            sList.append("   sortElement.value = filed; \r\n");
            // 排序方向
            sList.append("   var ascDescElement = window.document.getElementById('_PU_sortAscOrDesc'); \r\n");
            sList.append("   var pre_order = ascDescElement.value.toLowerCase(); \r\n");
            sList.append("   if ( preSortValue === sortElement.value ) { ascDescElement.value = ( 'asc' === pre_order ) ? 'desc' : 'asc'; } \r\n");
            sList.append("   else { ascDescElement.value = 'asc' } \r\n");
            // 送出
            sList.append("   this.submit(); \r\n");
            sList.append(" }; \r\n");
            
            sList.append(" </SCRIPT> \r\n");
        }

        // 寬度不夠時隱藏 “第1~6筆/共6筆” 這個欄位
        sList.append(" <SCRIPT type='text/javascript' language='JavaScript'> \r\n");
        sList.append("   _PU_.autoWidth(" + this.outTimes + "); \r\n");
        sList.append(" </SCRIPT> \r\n");

        this.outTimes++;
        return strEncode(sList.toString());
    }
    
    
    /**
     * 取代固定的變量
     * @param strSRC 要取代的字串
     * @return 取代後的字串
     */
    private String strEncode(String strSRC)
    {
        strSRC = strSRC.replace("${ORDER}", "\u7B2C"); // 第
        strSRC = strSRC.replace("${PAGE}", "\u9801"); // 頁
        strSRC = strSRC.replace("${TOTAL}", "\u5171"); // 共
        strSRC = strSRC.replace("${ROW}", "\u7B46"); // 筆
        strSRC = strSRC.replace("${EverPage}", "\u6BCF\u9801"); // 每頁
        // strSRC = strSRC.replace("${GOTO}", "\u8DF3\u5230"); //跳到
        return strSRC;
    }
    
}

%>