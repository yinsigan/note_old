﻿
1. 修改文件的修改日期
   复制文件，在文件名后面加上个“+,,”
   copy LeapFTP.exe+,,


2. 让“.bat”文件的文件名作为运行程序的参数

    // ************* example 2 start ****************
    @ echo off

    :: 计算目录名长度，长度为：%num%
    set str=%cd%
    :next1
    if not "%str%"=="" (
    :: 算术运算，使num的值自增1，相当于num++或者++num语句
    set /a num+=1
    :: 截取字符串，每次截短1
    set "str=%str:~1%"
    :: 跳转到next1标签: 这里利用goto和标签，构成循环结构
    goto next1
    )
    echo 目录的长度为：%num%

    :run
    :: 记录下文件名
    set args=%0
    set /a num+=2

    :: 字符串截取的需要，需要传递参数进行截取
    setlocal enabledelayedexpansion
    :: 截取出文件名:去除目录名和后缀名
    set args=!args:~%num%,-5!

    :: 运行 LeapFTP.exe 程序，并传递参数进去，参数可以多个，用空格隔开即可
    start LeapFTP.exe %args%
    // ************* example 2 end ****************



3. 用“.bat”文件的文件名作为运行程序的名称和参数
   并且，如果有转码工具的话，转换成简体来运行
   很类似上面一例
    // ************* example 3 start ****************
    @ echo off

    :: 计算目录名长度，长度为：%num%
    set str=%cd%
    :next1
    if not "%str%"=="" (
    :: 算术运算，使num的值自增1，相当于num++或者++num语句
    set /a num+=1
    :: 截取字符串，每次截短1
    set "str=%str:~1%"
    :: 跳转到next1标签: 这里利用goto和标签，构成循环结构
    goto next1
    )

    :run
    :: 记录下文件名
    set args=%0
    set /a num+=2

    :: 字符串截取的需要，需要传递参数进行截取
    setlocal enabledelayedexpansion
    :: 截取出文件名:去除目录名和后缀名
    set args=!args:~%num%,-5!

    :: 让本文件的文件名作为启动的程序名的参数
    :: start exe by chinese_simple
    if exist %SystemRoot%\AppPatch\AppLoc.exe (start %SystemRoot%\AppPatch\AppLoc.exe %args% /L0804) else (start %args%)

    // ************* example 3 end ****************


4.复制更新文件到工作目录“toWork”
    @echo off

    :: 配置信息
    set fromDir=D:\cvsclient\everunion\pili
    set toDir=D:\vs\pili
    set EXCLUDE=uncopy_toWork.txt
    set keepfile=toWork.txt

    :: 删除log等文件
    echo delete log ...
    del /s /q /f "%toDir%\logs\*"
    rmdir /q /s "%toDir%\logs"

    :: 创建/清空历史存档
    echo. >%keepfile%
    :: 复制更新文件1
    echo copy to Work ... aspx ... >>%keepfile%
    XCOPY "%fromDir%\src\aspx" "%toDir%\" /E /R /Y /D /C /EXCLUDE:%EXCLUDE% >>%keepfile%

    :: 复制更新文件2
    echo. >>%keepfile%
    echo copy to Work ... c# ... >>%keepfile%
    XCOPY "%fromDir%\src\c#" "%toDir%\classlib\" /E /R /Y /D /C /EXCLUDE:%EXCLUDE% >>%keepfile%

    :: 读取历史存档
    for /f "delims=" %%a in (%keepfile%) do echo. %%a

    echo.
    pause

5.文件夹同步(源目录已经删除的，也会同步删除)
    :: 文件夹同步
    :: 需设置来源文件夹,自动同步到目标文件夹;来源文件夹删除的,目标文件夹也相应删除
    :: 目标文件可指定位置,不指定的话默认是本文件所在文件夹
    @echo off

    :: 0. 配置
    :: 来源文件夹
    set fromDR=E:\本机\hh
    :: 目标文件夹
    set toDR=%cd%\hh


    :: 1. 更新文件

    :: 默认复制到此批处理所在文件夹
    if "%toDR%" == "" set toDR=%cd%

    :: 开始复制
    echo 更新文件...
    XCOPY "%fromDR%" "%toDR%\" /S /R /Y /D /C /H /K
    echo.


    :: 2. 删除来源文件夹已经删除的文件

    :: 复制自身
    if "%toDR%" == "%cd%"  ( XCOPY /R /Y /D /C /H /K %0 "%fromDR%\" )

    :: 计算目标文件夹名长度,长度为：%num1%
    set str=%toDR%
    set /a num1=0
    :next1
    if not "%str%"=="" (
        :: 算术运算, 使num的值自增1, 相当于 num1++ 语句
        set /a num1+=1
        :: 截取字符串, 每次截短1
        set "str=%str:~1%"
        :: 跳转到next1标签: 这里利用goto和标签, 构成循环结构
        goto next1
    )
    :: echo 目标文件夹的长度为：%num1%

    :: 盘符会被自动去掉
    set /a num1-=2


    :: 字符串截取的需要, 需要传递参数进行截取
    setlocal enabledelayedexpansion

    echo 删除源文件夹不存在的文件夹...
    for /R "%toDR%" /D %%a in (*) do (
        set "str=!%%a!"
        set "str=!str:~%num1%!"
        :: 显示出要删除的文件夹
        if not exist "%fromDR%!str!" echo %%a
        if not exist "%fromDR%!str!" rmdir /q /s "%%a"
    )
    echo.

    echo 删除源文件夹不存在的文件...
    for /R "%toDR%" %%b in (*) do (
        set "str=!%%b!"
        set "str=!str:~%num1%!"
        :: 显示出要删除的文件
        if not exist "%fromDR%!str!" echo %%b
        if not exist "%fromDR%!str!" del /q /f "%%b"
    )
    echo.


    :: 3. 删除临时保存到来源文件夹的自身
    if not "%toDR%" == "%cd%"  goto end

    set "str=!%0!"
    set /a num1+=1
    set "str=!str:~%num1%,-1!"

    del /q /f "%fromDR%\%str%"

    :end
    pause


6. 输出所有开机自动启动的项目(xp可以，但win7不行):
    @echo off
    :: 考虑到程序并非都安装在系统盘下，所以还要用!str:~-1!来截取盘符
    :: 如果路径中含有N个中文字符的话，此路径的最后N个字符将不显示(一个中文字符占两个字符位)
    :: code by jm 2006-7-27
    setlocal enabledelayedexpansion
    echo.
    echo 开机自启动的程序有：
    echo.
    for /f "skip=4 tokens=1* delims=:" %%i in ('reg query HKLM\Software\Microsoft\Windows\CurrentVersion\Run') do (
        set str=%%i
        set var=%%j
        set "var=!var:"=!"
        if not "!var:~-1!"=="=" echo !str:~-2!:!var!
    )
    pause

7. 文件内容替换
   注意，替换时空行及“!”会丢失，需要在源文件的“!”前加上“^”来保留它。
    :: 把dd.txt中的abc替换成123
    @echo off

    set f="dd.txt"
    set src=abc
    set dst=123

    setlocal enabledelayedexpansion
    for /f "usebackq delims=" %%a in (%f%) do (
        :: 重新编写这个文件
        if not defined flag cd.>%f%&set flag=1
        :: 替换
        set v=%%a
        set v=!v:%src%=%dst%!
        echo.!v!>>%f%
    )
    endlocal
    pause

