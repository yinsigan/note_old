﻿
前言：
    jQuery 是一套面向对象的简洁轻量级的 JavaScript Library。
    jQuery 让你用更精简少量的代码来轻松达到跨浏览器 DOM 操作、事件处理、设计页面元素动态效果、AJAX 互动等。
    jQuery 跟 UI 相关的 plugins 已经做过了一些整合，目前独立发布为 jQuery UI (http://ui.jquery.com/)
    jQuery 的函数大多具有批处理的功能，光是这点就可以让你的程序更简洁了。



1.介绍：
    钱符号 $ 是 jQuery 的函数名称， $("div") 就是用 jQuery 来选取文件内所有的 <div> 元素。
    $("div").addClass("special"); // 让你帮文件上所有的 <div> 元素都加入 class = "special"
    钱符号其实是 jQuery 的缩写， $("div") 跟 jQuery("div") 是一样的。也可以自己设定另外一个缩写。
    jQuery 所支持的 CSS Selector(选择器) 包含了 CSS1、CSS2 以及 CSS3，此外透过 plugin 还可支持常用的 XPath 语法

2.选择对象：
  1) 基本：
     $("*"):       选取所有的元素集合。多用于结合上下文来搜索。
     $("element"): 根据 元素名 选取元素集合。如：$("div")
     $("#id"):     根据 id 选取一个元素。如果 id 中包含特殊字符，可以用两个斜杠转义。若有id相同者，选第一个
     $(".class"):  根据 class 选取元素集合。一个元素可以有多个 class,只要有一个符合就能被选取到。
     $("Selector1, Selector2, SelectorN"): 将每一个选择器匹配到的元素合并后一起返回。可以指定任意多个选择器，并将匹配到的元素合并到一个结果集内。
                    如：$("div,span,p.myClass")，可选取到 <div>, <p class="myClass">, <span> 元素
     $("element#id"):    选取指定 id 的 element 元素。如：$("div#idName")
     $("element.class"): 选取指定 class 的 element 元素。如：$("div.className")
  2) 层级：
     $("ancestor descendant"): 在给定的祖先元素下选取所有的后代元素(包括子元素的子元素)。如：$("div.className p")
     $("parent > child"):      在给定的父元素下选取所有的一级子元素(不包括子元素的子元素)。如：$("form > input")
     $("prev + next"):         选取所有紧接在 prev 元素后的第一个 next 元素(不是父元素下的子元素)
     $("prev ~ siblings"):     选取 prev 元素之后的所有同辈 siblings 元素
  3) 基本选择器：
     $(":first"): 选取第一个元素。如：$("tr:first")
     $(":last"):  选取最后一个元素。
     $(":not(Selector)"): 去除所有与给定选择器匹配的元素。支持复杂选择器。如：$("input:not(:checked)")
     $(":even"):  选取所有索引值为偶数的元素，从 0 开始计数。如：$("tr:even")，选取表格的1、3、5...行
     $(":odd"):   选取所有索引值为奇数的元素，从 0 开始计数。如：$("tr:odd")，选取表格的2、4、6...行
     $(":eq(index)"): 选取一个给定索引值的元素，从 0 开始计数。如：$("tr:eq(1)")，选取表格的第2行
     $(":gt(index)"): 选取所有大于给定索引值的元素，从 0 开始计数。如：$("tr:gt(0)")，选取表格的第2至结束行
     $(":lt(index)"): 选取所有小于给定索引值的元素，从 0 开始计数。如：$("tr:lt(2)")，选取表格的第1~2行
     $(":header"): 选取 h1 ~ h6 的标题元素
     $(":animated"): 选取所有正在执行动画效果的元素

     $("div:has(a)"): 选取至少有包住一个 <a> 的 <div> 元素，如:<div><a>...</a></div> 中的 <div>
  6) 属性：
     $("[attribute]"): 根据给定的属性选取元素。如:$("div[id]")，选取含有属性为 id 的<div>元素
     $("element[attribute]"): 选取含有属性的元素，如: $("a[href]") 选取 <a href="#">Amazon</a>
     $("element[attribute='value']"): 选取含有属性为某个值的元素，如: $("a[href='#']") 选取 <a href="#">Amazon</a>
     $("element[attribute!='value']"): 等价于“:not([attr=value])”, 选取不含有属性为某个值的元素
     $("element[attribute^='value']"): 选取给定的属性是以某些值开始的元素
     $("element[attribute$='value']"): 选取给定的属性是以某些值结尾的元素
     $("element[attribute*='value']"): 选取给定的属性是包含某些值的元素


3.选择含有特殊字符的元素:
    如：
      <span id="foo:bar"></span>
      <span id="foo[bar]"></span>
      <span id="foo.bar"></span>
    jQuery 代码分别为:
      $("#foo\\:bar");
      $("#foo\\[bar\\]");
      $("#foo\\.bar");



4.常用方法：
    使用 jQuery 来选取元素，其中大部份的语法都是可以让你快速地一次选取多个元素。
    透过 jQuery 内建的函数，你可以：
    1.对 DOM 进行操作，例如对文件节点的新增或修改
    2.添加事件处理
    3.做一些基本的视觉效果，例如隐藏、显示、下拉显示、淡出淡入等等
    4.使用 AJAX 传送窗体内容或取得远程文件

  // 以下执行后的内容，皆为示意结果
  1)在元素节点下追加一段内容
    $("p").append("<b>Hello</b>");
    执行前：<p>I would like to say: </p>
    执行后：<p>I would like to say: <b>Hello</b></p>

  2)css()
    // 選取 id 為 body 的元素，並且修改兩個 css 屬性。
    $("#body").css({ border: "1px solid green", height: "40px" });
    执行前: <div id="body"> ... </div>
    执行后: <div id="body" style="border: 1px solid green; height: 40px"> ... </div>

  3)提交前的判断
    // $(document).ready()事件，在载入就绪时执行。在 body 标签的 onload 事件之前执行。
    // $("form").submit() 相当于给所有的<form>标签加上 onsubmit 的内容
    <html>
      <head>
        <script type="text/javascript" language="javascript" src="../jquery.js"></script>
        <script type="text/javascript" language="javascript">
        $(document).ready(function()
        {
            $("form").submit(function()
            {
                if ( $("input#username").val() === "" )
                {
                    $("span").show();
                    return false;
                }
            });
        });
        </script>
      </head>
      <body>
        <form action="jj.html">
          <label for="username">請輸入大名</label>
          <input type="text" id="username" name="username" />
          <span style="display:none"><font color="red">這個欄位必填喔</font></span><br />
          <input type="submit" value="test" /><br />
        </form>
      </body>
    </html>
	// $(document).ready(function(){}) 可简写为: jQuery(function(){})

  4)获取最新的 jquery.js 文件：
    http://code.jquery.com/jquery-latest.js

  5)发送Ajax
	function getDep()
	{
		var pSource = form1.pSource.value;
		if ( "" === pSource || pSource == null )
		{
			return;
		}
		//送出Ajax
		jQuery.ajax({
			//POST 或者 GET
			type:"POST",
			//網址
			url: "ajax_changeYL.jsp",
			//发送的数据
			data: "pSource=" + pSource + "&id=change",
			//同步请求
			async: false,
			//錯誤时
			error:function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus);},
			//成功时,即回调函数
			success:function(data, textStatus)
			{
				// data 可能是 xmlDoc, jsonObj, html, text, 等等...
				$("#queryResult").html(data); //相当于 $("#queryResult").innerHTML = data;
			}
		});
	}

	6) 将 form 序列化
	   $("form").serialize();
	   这会将 form 的所有内容序列化,结果如: single=单选的值&multiple=多选的值&check=check2&radio=radio1












// http://jsgears.com/thread-63-1-1.html
