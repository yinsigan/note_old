1、熟悉PHP 5.3.1 几个新版本的区别

        VC9   是专门为IIS定制的脚本支持最新的微软组件从而提高效率
        VC6   是为了其他WEB服务软件提供的脚本，如  Apache

    在VC9和VC6当中又分为： Non Thread Safe 和  Thread Safe

先从字面意思上理解，Thread Safe是线程安全，执行时会进行线程（Thread）
安全检查，以防止有新要求就启动新线程的CGI执行方式而耗尽系统资源。
Non Thread Safe是非线程安全，在执行时不进行线程（Thread）安全检查。

2、Apache PHP Mysql 之间的关系

3、熟悉Apache PHP 重要的几个配置文件

Apache配置文件 httpd.conf

　　LoadModule php5_module C:/WAMP/PHP5.3.1/php5apache2_2.dll

　　PHPIniDir "C:/WAMP/PHP5.3.1"

　　AddType application/x-httpd-php .php .phtml

PHP配置文件 PHP.ini

　　php.ini-development 、 php.ini-production   修改成 php.ini 文件即可

　　extension_dir，设置为extension_dir = "C:/WAMP/PHP5.3.1/ext"

4、实际操作在windows下搭配WAMP环境

Apache 2.2.14  安装版

PHP 5.3.1  VC6 x86 Thread Safe 解压版Zip

Mysql 5.0.x 安装或解压版都可以，只要能正常启动

独立PPT参考文件下载 http://bbs.php100.com/read-htm-tid-19863.html

