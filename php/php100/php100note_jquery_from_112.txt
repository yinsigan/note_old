双击编辑异步更新

    操作流程 

        数据库查询列表 -> 增加技巧标记 -> 双击更改当前位置状态为input框体并获得焦点 ->焦点离开抓取新内容修改当前位置,并执行post异步提交

        1、将原始内容放入input
        2、抓取必要标记：编号，字段，值

    $(select).html(content)
    $(select).attr(attribute)
    $(selector).parents(selector)
    $(selector).live(event,data,function)

-------------------------------------------------------------------------------------------
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-cn" lang="gbk">
<head>
<title>PHP100视频教程 jquery 双击编辑</title>
<style type="text/css">
table	{ border:0;border-collapse:collapse;}
td		{ font:normal 12px/17px Arial;padding:2px;width:100px;}
th		{ font:bold 12px/17px Arial;text-align:left;padding:4px;border-bottom:1px solid #333;}
.dan	{ background:#FC0}  
.ed	{ background:#669;color:#fff;}
</style>
<script src="jquery-1.5.1.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
            $("tbody>tr:even").addClass("dan");
            $("tbody>tr>td").dblclick(function(){
                var inval = $(this).html();
                var infd = $(this).attr("fd");
                var inid = $(this).parents().attr("id");
                $(this).html("<input id='edit"+infd+inid+"' name ='' value= '"+inval+"' />");
                //选中文字
                $("#edit"+infd+inid).focus().live("blur",function(){
                    var editval = $(this).val();
                    //当离开时去掉input框
                    $(this).parents("td").html(editval);
                    //传值
                    $.post("post.php",{id:inid,fd:infd,val:editval});
                });

            });
    });
</script>
</head>
<body>
	<table>
		<thead>
			<tr><th>标题</th><th>时间</th><th>地点</th></tr>
		</thead>
		<tbody>

			<tr id="1"> 
				<td fd="t">PHP100视频1</td><td fd="d">2011</td><td fd="a">上海</td></tr>

			<tr  id="2"> 
				<td fd="t">PHP100视频2</td><td fd="d">2012</td><td fd="a">杭州</td></tr>
			<tr  id="3"> 
				<td fd="t">PHP100视频3</td><td fd="d">2011</td><td fd="a">济南</td></tr>
			<tr  id="4"> 
				<td fd="t">PHP100视频4</td><td fd="d">2011</td><td fd="a">北京</td></tr>
		</tbody>
	</table>
</body>
</html>

--------------------
post.php

<?php
if(!empty($_POST['id'])){
	$id=$_POST['id'];
	$fd=$_POST['fd'];
	$val=$_POST['val'];
   $sql = "update tables set `$fd`='$val' where `id`='$id'";
   echo $sql;
}

?>
