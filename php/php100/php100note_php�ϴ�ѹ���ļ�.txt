1、 PHP上传并解压的原理
2、 PHP执行系统命令的几类函数及区别
     system() 输出并返回最后一行shell结果。 
    exec() 不输出结果，返回最后一行shell结果 
    passthru() 只调用命令，把运行结果原样地输出 
    new com() 系统预定义com类，根据需要任意选择内置方法

相同点：都可以获得命令执行的状态码


3、使用PHP 预定义的Com组件加载Shell 



   $obj=new com("wscript.shell");
   $obj->run(“所要执行的命令内容”);

    备注：

    Rar解压命令：  winrar      x      被解压文件     加压的位置
   

4、实例操作PHP上传解压案例

获取当前绝对路径： getcwd()  
上传移动文件函数： move_uploaded_file（）

<?php
	//取得当前php文件路径
	$dir = getcwd();
   if(isset($_POST["sub"])){
     $tname=$_FILES["upfiles"]["tmp_name"];
     $nname=$_FILES["upfiles"]["name"];
     move_uploaded_file($tname,$nname);  
     //com组件可以执行dos命令
     $obj = new com("wscript.shell");  
     //执行run命令
     $obj->run("winrar x $dir\\".$nname." ".$dir , 1,true);       
     unlink($nname); 
   }
?>
 <form action="" method="POST" enctype="multipart/form-data">

 选择上传文件 <input type="file" name="upfiles"/>
 <input type="submit" name='sub' value='提交并解压' >

</form>

