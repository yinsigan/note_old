thinkphp学习

    <?php
        //定义ThinkPHP框架路径(相对于入口文件)
        define('THINK_PATH','./ThinkPHP/');

        //定义项目名称和路径
        define('APP_PATH','./home/');

        define('APP_NAME','home');
        
        //编译目录
        //define('RUNTIME_PATH','./admin/temp');

        //是否去除空白和注释
        //define('STRIP_RUNTIME_SPACE',false);

        //不缓存
        //define('NO_CACHE_RUNTIME',True);


        //加载框架入口文件
        require THINK_PATH.'ThinkPHP.php';

        //实例化一个网站应用实例
        App::run();

    ?>

--------------------------------------------

ThinkPHP_2.1_core\home\Lib\Action\IndexAction.class.php

public function del(){
    echo "A";
} 
//可以定义一个私有方法,然后在一个类中的public方法调用
//浏览器访问方法
http://localhost/ThinkPHP_2.1_core/index.php/index/del

//PATHINFO模式 在index.php后定义一些东西
    //m是模块(控制器) a是动作(class中的function)
    http://localhost/ThinkPHP_2.1_core/index.php?m=user&a=index
    http://localhost/ThinkPHP_2.1_core/index.php/user/index

//URL兼容模式
//  http://localhost/ThinkPHP_2.1_core/index.php?s=/user/g
//  'URL_MODEL'=>3
//  http://localhost/ThinkPHP_2.1_core/index.php?s=/user/index

// 如果使用普通模式,必须要在前面加上/m/模块/a/动作(方法)

/*  保存在g.php(documentroot)
 *  <?php
    function dump($data){
        echo '<pre>';
        var_dump($data);
        echo '<pre>';


    }
    dump($_SERVER);
    ?>
 * */

//URL访问模式 conf目录下
// 0 叫做普通模式 http://localhost/appName/index.php?m=moduleName&a=actionName&id=1
// 1 叫做pathinfo模式 http://bbs.lampbrother.net/index.php/模块/动作
// 2 rewrite(重写)伪静态 
//
// 当服务器上面不支持pathinfo模式的时候,但是你又在之前的路径访问格式上面,全部用的是pathinfo格式
// 它会提示你路径不正确你可以用兼容模式来处理GET
// 3 叫做兼容模式 http://bbs.lampbrothre.net/index.php?s=/user/g

/*
 *URL_PATHINFO_MODEL=>1普通模式
 *
 *http://localhost/ThinkPHP_2.1_core/index.php/user/index/username/aa echo $_GET["username"]
 *  'APP_DEBUG'=>true *
 *
 */

//调另一个控制器
//$this->display("Index:index");
//跨皮肤
// $this->display("skin@user:sg");
// $this->display("./home/Tpl/default/User/index.html");
//
echo __ROOT__;//代表网站的根目录地址
echo __APP__;//代当前项目的入口文件地址
echo __URL__;//代表当前模块地址
echo __ACTION__;代表当前操作地址
echo __SELF__;//代表当前的URL地址
echo __CURRENT__;//当前模块的模版目录

ACTION_NAME //当前操作名称
APP_PATH //代表当前项目目录
APP_NAME //代表当前项目名称
APP_TEMP_PATH //代表当前项目名称
APP_PUBLIC_PATH //代表项目公共文件目录
CAHCE_PATH //项目模版缓存目录
COMMON_PATH //项目公共文件目录
DATA_PATH //项目数据文件目录
IS_APACHE // 是否属于 Apache (2.1版开始已取消)
IS_CGI //是否属于 CGI模式 
IS_IIS //是否属于 IIS  (2.1版开始已取消)
IS_WIN //是否属于Windows 环境 

<form method="post" action="__URL__/add">
    <input id="" name="username" type="text" />
    <input id="" name="" type="submit" value="提交"/>
</form>

//模版替换
TMPL_PARSE_STRING  => array(

     '__PUBLIC__' => '/Common',  // 更改默认的__PUBLIC__ 替换规则

     '__JS__' => '/Public/JS/',  // 增加新的JS类库路径替换规则

)


//debug模式
 *  'APP_DEBUG'=>true *
 *  SHOW_RUN_TIME=>true //显示运行时间
 *  SHOW_ADV_TIME=>true //显示详细的运行时间
 *  SHOW_DB_TIMES=>true //显示数据库的操作次数
 *  SHOW_CACHE_TIMES=>true //显示缓存操作次数
 *  SHOW_USE_MEM=>true //显示内存开销
 *  TEMP_TRACE_FILE=>APP_PATH.'/Public/trace.php'; //或Conf/trace.php return array{};
 *//或$this->trace("aa","b");
 * dump();
 * debug_start('run'); //代码  debug_end('run');
 * halt("aa") //输出信息并中断
 *
 *//模型调试(就是显示数据库操作类当中的执行的sql语句)
 * getLastSql()//获得最后一次执行sql语句
 * LOG_RECORD=>true //开启日志记录
 * LOG_RECORD_LEVEL=>array('EMERG','ALERT');
 * Log::write($message,$level,$type,$file); //日志信息 日志级别 日志类别 位置
 
 ------------------数据库操作--------------------------

---------conf------------
 'DB_TYPE'=>'mysql',
 'DB_HOST'=>'localhost',
 'DB_USER'=>'root',
 'DB_NAME'=>'videodemo',
 'DB_PWD'=>'liwenkaihaha',
 'DB_PORT'=>'3306'
 'DB_PREFIX'=>'think_'
 
 <?php
    $arr1 = array(
        'URL_MODEL'=>2;
    ); 

    $arr2 = include './config.inc.php';
    return array_merge($arr1,$arr2);

 ?>

//手动第一种
$dsn = 'mysql://username:password@localhost:3306/DBNAME';
$db = new Db($dsn);

//手动第二种
$dsn = array(
    'dbms' => 'mysql',
    'username'=>'google',
    'password'=>'google',
    'hostname'=>'8.8.8.8', //集群加逗号分开
    'hostport'=>'3309',
    'database'=>'googledns',
);

$db = new Db($dsn);

//高并发主从数据库
'DB_TYPE'=>'mysql'

//读写分离
'DB_RW_SEPARATE'=>'true' //第一台写,其他读

//读取配置文件参数
C('DB_HOST')

-------------实例化模型----------------------------

$User = new Model('User'); //第一种方法 传表名
//表名 think_user_message 对应 UserMessage 或 user_message

//实例化一个表返回数组
public function del(){
    $User = new Model('user');  //与$User = M('user');等价
    $list = $User->select();
    dump($list);
}

-----第二种方式------

//跨模型操作
public function del(){
    $user = M('user','CommonModel'); //CommonModel是一个模型 //等价于 new CommonModel('user')
    $list = $user->select();
    dump($list);
    $user->abd(); //CommonModel模型中的方法
}

-----第三种方式------

//实例化一个自定义模型
$user = new UserModel(); //模型名与表名相同 $user = D('user');

-----第四种方式------

//实例化一个空模型
    $user = new Model();
    $list = $user->query('select * from think_user');
    dump($list);

    //D方法可以自动检测模型类，不存在时,它会抛出一个异常。同时对于已实例化的模型,不会去重复实例化。默认的D方法只能支持调用当前项目下面的模型
    //D('admin','user')
    //D('admin.user');

----------------create方法与令牌------------------------

//真实表名称,避免表前缀冲突
protected $trueTableName = 'cms_user';

//UserMessageModel.class.php

    protected $dbName = 'user'; //跨库

    protected $fields = array(
        0=>'id',
        1=>'username',
        2=>'password',
        '_pk'=>'id',
        '_autoinc'=>ture,
    );

/**
 * 获得表单中的数据 自动令牌
 */
function add(){
    $user = new Model('user');

    if($user->create()){
        $this->success('create成功')
    }else{
        $this->error('create失败');
    }
}

/**
 *  令牌开启
 */
TOKEN_ON=>true


-------------------------
public function add(){
    $user = new Model('user');
    if($a=$user->create()){
        echo $user->username; //显示用户名
        dump($a);
        $this->success("实例化成功");
    }else{
        $this->error("错误");
    }
    
    /*
    if(!$user->autoCheckToken($_POST)){
        //令牌验证失败的代码就可以写到此处
    }
    */
}

public function add(){
    $user->password=md5($user->password);
    $user = new Model('user');
    if($vo=$user->create()){
        if($user->add()){
            $this->success("Aa");
        }else{
            $this->error("aa");
        }
    }
} //会添加数据

<form action="__URL__/add" method="post">
    <input id="" name="username" type="text" />
    <input id="" name="password" type="password" />
    <input type="submit" value="提交" />
    // {__NOTOKEN__} //关闭令牌
</form> //会增加一串字符串 令牌
-------------------------
$user = M('user');
//find() 只查询一条记录
//select() 读取所有信息
//findAll() 与select()方法相同
//$list = $user->where('id>3')->select();
$list = $user->where('id>1')->limit('2')->select();
$list = $user->where('id>1')->order('id desc')->select();
$list = $user->select(array('where'=>'id>1','limit'=>'3','order'=>'id desc'));
$list = $user->limit('2')->order('id desc')->where('id > 1')->field('id,username')->select();

//跨表查询
$list = $user->table('think_user user,think_user_message m')->where('user.id=m.id')->select();
//field()域选择
$list = $user->field('user.id as uid,m.id as mid')->table('think_user user,think_user_message m')->where('user.id=m.id')->select();
$list = $user->field(array('user.id'=>'uid','m.id'=>'mid'))->table('think_user user,think_user_message m')->where('user.id=m.id')->select();

//order()
$list = $user->limit('2')->order(array('id'=>'desc','username'))->where('id>1')->field('id,username')->select();

//getField
$list = $user->where('id=4')->getField('password,username');

//save()更新数据
$data['password'] = 'aaaaa';
$list=$user->where('id=4')->save($data);

    $data['password'] = 'bbbb';
    $data['id'] = '4';
    $list=$user->save($data);
    //$list=$user->data($data)->save($data);

//$list=$user->data($data)->add();

$user = new Model('user');
$_GET["password"] = md5($_GET['password']);
    if($list = $user->add($_GET)){
        $this->success("添加成功");
    }else{
        $this->error("失败");
}

//删除记录
$list=$user->delete(6); //删除id=6那条记录
$list=$user->where('id>3')->order('id desc')->limit('i')->delete();

//setInc 增加ID值
//setDec 减少ID值

setField 更新数据

-------------查询----------------------

$user = M('User');
$data["username"] = 'test';
$list = $user->where($data)->select();
dump($list);
        
$con = new stdClass();
$con->username='test';
$list=$user->where($con)->select();
dump($list);

$data['username'] = array('eq','test');
$list=$user->where($data)->select();

//$data['id'] = array('neq','3');
//$data['username'] = array('like','a%');
//$data['id'] = array('between',array(2,5));

ThinkPHP支持对某个字段的区间查询，例如：
$map['id'] = array(array('gt',1),array('lt',10)) ;
得到的查询条件是： (`id` > 1) AND (`id` < 10)
$map['id'] = array(array('gt',3),array('lt',10), 'or') ;
得到的查询条件是： (`id` > 3) OR (`id` < 10)
$map['id']  = array(array('neq',6),array('gt',3),'and'); 
$User = M("User"); // 实例化User对象
 获取用户数：
$userCount = $User->count(); 
 获取用户的最大积分：
$maxScore = $User->max('score');
 获取积分大于0的用户的最小积分：
$minScore = $User->where('score>0')->min('score');
获取用户的平均积分：
$avgScore = $User->avg('score');
统计用户的总成绩：
$sumScore = $User->sum('score');


------------------无限制分类--------------------

1 	新闻 	    0 	0
2 	中国新闻 	1 	0-1
3 	美国新闻 	1 	0-1
4 	北京新闻 	2 	0-1-2
5 	华盛顿新闻 	3 	0-1-3

