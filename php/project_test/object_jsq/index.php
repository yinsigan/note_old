<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title>图形计算(使用面向对象技术开发)</title>
</head>
<body>
<center>
    <h1>图形(周长&面积)计算器</h1>
    <a href="index.php?action=rect">矩形</a> ||
    <a href="index.php?action=triangle">三角形</a> ||
    <a href="index.php?action=circle">圆形</a>
    <hr />
</center>
<?php
    error_reporting(E_ALL & ~E_NOTICE);
    function __autoload($obj) {
        include './' . $obj . '.class.php';
    }
    echo new Form();
    if( isset($_POST["sub"]) ) {
        echo new Result();
    }
?>
	
</body>
</html>
