<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
</head>
<?php
$num1 = true;
$num2 = true;
$numa = true;
$numb = true;
$message = "";
if(isset($_GET["sub"])){
    if($_GET["num1"] == ""){
        $num1 = false;
        $message = "第一个值不能为空";

    }
    if(!is_numeric($_GET["num1"])){
        $numa = false;
        $message .= "第一个数不是数字";
    }
    if($_GET["num2"] == ""){
        $num2 = false;
        $message .= " 第二个值不能为空";
    }
    if(!is_numeric($_GET["num2"])){
        $numb = false;
        $message .= "第二个数不是数字";
    }
    if($num1 && $num2 && $numa && $numb){
        $sum = 0;
        switch($_GET["ysf"]){
            case "+":
                $sum = $_GET["num1"] + $_GET["num2"];
                break;
            case "-":
                $sum = $_GET["num1"] - $_GET["num2"];
                break;
            case "*":
                $sum = $_GET["num1"] * $_GET["num2"];
                break;
            case "/":
                $sum = $_GET["num1"] / $_GET["num2"];
                break;
            case "%":
                $sum = $_GET["num1"] % $_GET["num2"];
                break;
        }
    }
}

   
?>
<body>
    <table align="center" border="1" width="500">
        <caption> <h1>计算器</h1> </caption>
        <form action="jsq.php">
        <tr>
        	<td>
            <input id="" size="5" name="num1" type="text" value="<?php if(isset($_GET["num1"])) echo $_GET["num1"]?>" />
        	</td>
        	<td>
                <select id="" name="ysf">
                    <option value="+" <?php if(isset($_GET["ysf"]) && $_GET["ysf"] == "+") echo "selected"?>>+</option>
                    <option value="-" <?php if(isset($_GET["ysf"]) && $_GET["ysf"] == "-") echo "selected"?>>-</option>
                    <option value="*" <?php if(isset($_GET["ysf"]) && $_GET["ysf"] == "*") echo "checked"?>>*</option>
                    <option value="/" <?php if(isset($_GET["ysf"]) && $_GET["ysf"] == "/") echo "checked"?>>/</option>
                    <option value="%" <?php if(isset($_GET["ysf"]) && $_GET["ysf"] == "%") echo "checked"?>>>%</option>
                </select>
        	</td>
        	<td>
            <input id="" name="num2" size="5" type="text" value="<?php if(isset($_GET["num2"])) echo $_GET["num2"]?>"/>
        	</td>
        	<td>
        		<input id="" name="sub" type="submit" value="计算" />
        	</td>
        </tr>
        <?php
            if(isset($_GET["sub"])){
                echo '<tr><td colspan="5">';
                if($num1 && $num2 && $numa && $numb){
                    echo "结果:" . $_GET["num1"] . " " . $_GET["ysf"] . " " . $_GET["num2"] . " = " . $sum;
                }else{
                    echo $message;
                }
                echo '</td></tr>';
            }
        ?>
        
        </form>
    </table>
</body>
</html>
