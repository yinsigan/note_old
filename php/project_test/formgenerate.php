<?php
$options = array("010"=>"北京","020"=>"上海","024"=>"沈阳","0411"=>"大连");
$default = "024";
$html = generate_radio_group("city_id", $options, $default);
echo $html;
function generate_radio_group($name, $options, $default="") {
    $name = htmlentities($name);
    $html = "";
    foreach ($options as $value => $label) {
        $value = htmlentities($value);
        $html .= "<input type=\"RADIO\" ";
        if ($value == $default) {
            $html .= "CHECKED ";
        }
        $html .= "NAME=\"$name\" VALUE=\"$value\">";
        $html .= $label . "<br />";
    }
    return $html;
}

function generate_checkboxes($name, $options, $default=array()) {
    if (!is_array($default)) {
        $default = array();
    }
    $html = "";
    foreach ($options as $value => $label) {
        $html .= "<input type=checkbox ";
        if (in_array($value, $default)) {
            $html .= "checked ";
        }
        $html .= "name=\"{$name}[]\" value =\"$value\">";
        $html .= $label . "<br />";
    }
    return $html;
}
$interests = array("音乐"=>"音乐", "电影"=>"电影", "互联网"=>"互联网", "旅游"=>"旅游");
$html = generate_checkboxes("interests", $options, $interests);
echo $html;


$options = array('1'=>"请选择",'news'=>"新闻",'events'=>'事件','publications'=>'稿件');
$default = "news";
$html = generater_muilti_option("select", $options, $default);
echo $html;

function generater_muilti_option($name, $options, $default) {
    echo '<select name="'.$name.'[]" id="'.$name.'[]" multiple="multiple">';
    foreach ($options as $value => $option) {
        echo '<option value="' . htmlspecialchars($value) . '"';
        if ($default == $value) {
            echo ' selected';
        }
        echo '>' . htmlspecialchars($option) . '</option>';
    }
    echo '</select>';
}
?>
<form action="test.php" method=post>
<?php echo $html;?>
<input type=submit value="继续">
</form>
