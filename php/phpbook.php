func_num_args() == argument.length;
func_get_arg($i) == argument[i];
func_get_args();

函数是否存在function_exists
create_function == new Function()

字符串序列化serialize

wordwrap()函数用于指定从某列字符进行强制换行

字符串操作函数

    字符串分割 explode == String.split()

    str_word_count()函数 统计一段文字中有多少个英语单词
    arrary_count_values()对文本中单词的出现次数进行统计

    strstr stristr子串查找
    strpos == String.indexOf()
    mb_strpos

    str_repeat
    substr_count查找子串出现的次数

    str_replace == String.replace()
    
    与实体引用有关的参数
    htmlspecialchars htmlentities 

    清除HTML标签strip_tags()

时间日期函数

    checkdate 检查日期是否正确
    date

    date_default_timezone_get();
    date_default_timezone_set();

header("refresh:3;url=http://www.php.net");

const 前面不可以有任何访问控制修饰符
abstract function func(); 最少是protected
class A {
    const SS = "ssss";
}
echo A::SS; //可以直接访问哦
接口中的方法访问控制符只能是public

final不能修饰成员属性

正则表达式优先级
\
() (?:) []
* + ? {}
^ $ \b
|

模式修正符号 i u
s 忽略\n 让.可以匹配

echo preg_replace("/([a-zA-Z]+\(\))/e", '"<font color=\"red\">"'.'. strtoupper("${1}") .'. '"</font>"' , $str);

文件类型:block, char, dir, fifo, file, link, unknown

