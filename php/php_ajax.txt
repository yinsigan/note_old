--------------------------------------------------------------------------------------------------------
method                              description
--------------------------------------------------------------------------------------------------------
abort()                             Cancels the current request //取消当前请求
getAllResponseHeaders()             Returns all HTTP headers as a String type variable //返回http头信息
getResponseHeader()                 Returns the value of the HTTP header specified in the method //返回方法中指定的头信息
open                                Specifies the different attributes necessary to make a connection to
                                    the server; allows you to make selections such as GET or POST (more
                                    on that later), whether to connect asynchronously, and which URL
                                    to connect to
setRequestHeader()                  Adds a label/value pair to the header when sent //当发送信息时设置头部信息
send()                              Sends the current request //发送请求

//进一步看这些方法

open ("method","URL","async","username","pswd") //open方法的使用
/*
An important note is that this method may
only be invoked after the open() method has been used, and must be used before the
send function is called.
此方法必须先于send方法后于open方法使用
*/
setRequestHeader("label","value") //


--------------------------------------------------------------------------------------------------------
Property                            Description
--------------------------------------------------------------------------------------------------------
onreadystatechange                  Used as an evnet handler for events that trigger upon state changes
readyState                          Contains the current state of the object(0:uninitialized,1:loading,2:loaded,3:interactive,4:complete) //请求状态
responseText                        Returns the response in string format //应答信息
responseXML                         Returns the response in proper XML format //XML应答信息
status                              Returns the status of the request in numerical format(regular page errors are returned,such as the number 404,which refers to a not found error) //返回服务器应答状态,数字格式
statusText                          Returns the status of the request,but in string format(e,g.,a 404 error would return the stirng Not Found) //返回服务器应答状态,字符串格式

    //附表:Common HTTP Response COdes
    --------------------------------------------------------------------------------------------------------
    code                            description
    --------------------------------------------------------------------------------------------------------
    200 OK                          This response code is returned if the document of file in question is found and served correctly. //成功
    303 Not Modified                This response code is returned if a browser has indicated that it has a local,cached copy,and the server's copy has not changed from this cached copy.
    401 Unauthorized                This response code is generated if the request in question requires authorization to access the requested document.
    403 Forbidden                   This response code is returned if the requested document does not have proper permissions to be accessed by the requestor.
    404 Not Found                   This response code is sent back if the file that is attempting to be accessed counld not found(e.g.,if it doesn't esist).
    500 Internal Server Error       This code will be returned if the server that is being contacted has a problem.
    503 Service Unavailable         This response code is generated if the server is too overwhelmed to handle the request.

----------------------------------------------------------------------------
//测试浏览器的xmlhttprequest对象
//IE有个新旧两个ActiveXObject对象

<script type="text/javascript">
        //Create a boolean variable to check for a valid Internet Exlorer instance.
    var xmlhttp = false;

    //Check if we are using IE.
    try{
        //If the Javascript version is greater than 5.
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        alert("You are using Microsoft Internet Explorer.");
    }catch(e){
        //If not,then use the older active x object.
        try{
            //If we are using Internet Explorer.
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            alert("You are using Microsoft Internet Explorer");
        }catch(E){
            //Else we must be using a non-IE browser.
            xmlhttp = false;
        }
    }
    
    //If we are using a non-IE browser,create a JavaScript instance of the Object.
    if(!xmlhttp && typeof XMLHttpRequest != 'undefined'){
        alert("You are not using Microsoft Internet Explorer");
    }
</script>

    //下面的方法创建xmlhttprequest更简单
    var xmlhttp;
    //If, the activexobject is available, we must be using IE.
    if (window.ActiveXObject){
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } else {
    //Else, we can use the native Javascript handler.
    xmlhttp = new XMLHttpRequest();
    }
        
----------------------------------------------------------------------------
Sending a Request to the Server
----------------------------------------------------------------------------
//发送信息最好用post,获得信息就用get

//简单的使用ajax函数

    function makerequest(serverPage, objID) {
        var obj = document.getElementById(objID);

        xmlhttp.open("GET", serverPage);

        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                obj.innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.send(null);
    }

