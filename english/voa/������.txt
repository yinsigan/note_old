A nickname is a shortened form of a person's name
昵称是一个人名字的一个简称

A nickname can also be a descriptive name for a person,place or thing.
它也可以是人、地点或事物的描述性的名称

America's 50 states have some of the most historically interesting nicknames.
美国的五十个州都有些极具历史意义的有趣的昵称

Alabama is known as the Heart of Dixie because it is in the very middle of a group of states in the Deep South.
阿拉巴马州被称为"Heart of Dixie",因为它地处美国最南部各州的正中心

Dixie itself is a nickname for the American South.
"Dixie"本身就是美国南部的一个昵称

It started when Louisiana printed notes with the French word for "ten" on them.
起初是路易斯安那州在票据上印上了法语中表示"10"的单词

"Deece," or D-L-X,led to 'Dixie'
由Deece(即D-I-X)演变成了Dixie这个词

Way up north,Alaska is called the Last Frontier for understandable reasons.
北边的阿拉斯加州由于众所周知的原因被称为"最后的边疆"

Near the Arctic Circle,it was the final part of the nation to be explored and settled.
它靠近北极圈,是美国拓疆的最后一块国土

Arizona is the Grand Canyon State because of the famous winding canyon carved by the Colorado River.
亚利桑那州被称为"大峡谷之州",是因为这里有着著名的由科罗拉多河雕琢而成的蜿蜒的峡谷

The southern state of Arkansas is the Land of Opportunity.
位于南部的阿肯色州被称为"机会之乡"

The state legislature chose this nickname.
该州的立法机关确立了这个昵称

Arkansas is rich in natural resources and has become a favorite place for older people to retire.
阿肯色州的自然资源丰富,现已成为老人们退休后最喜欢走的地方

In a popular Spanish book,a fictional island called "California" was filled with gold.
在一部流行的西班亚语的书中，一座名为"加利福尼亚"的虚构的岛屿上黄金遍地

Sure enough,plenty of it was discovered in the real California,in 1848.
1848年，现实中的加利福尼亚州也被发现储有大量的黄金

This started a gold rush unlike any other in American history in the Golden State.
从此,"金色之州"加利福尼亚开始了美国历史上从未有过的淘金热。

You would think Colorado would be known as the Rocky Mountain State.
你可能会认为科罗拉多州会被称为"洛基山之州"

But its nickname is the Centennial State.
但其实它的昵称是"百年之州"

That is because it became a state in 1876,exactly 100 years after the nation declared its independence.
因为1876年科罗拉多成为美国一州的时候,刚好是美国宣布独立一百周年

Connecticut is called the Nutmeg State after a spice.
康乃狄格州国为一种香料被称为"肉豆蔻州"

Connecticut Yankees,as people in this northeast state are called,are know to be smart in business.
身处美国东北部的康涅狄格被称为"康涅狄格佬",因其在经商方面精明而得名。

So smart that it was said they could sell wooden,meaning false,nutmegs to strangers.
据说他们精明到可以身陌生人成功地兜售质量很差的肉豆蔻

Little Delaware is called the First State because it was the first state -- the first to approve the new United States Connecticut.
小小的特拉华州被称为"第一州",因为它是第一个通过美国新宪法的州.

The southern state of Florida likes to tell about its sunny days and fine beaches.
位于南部的佛罗里达州以其明媚的阳光和美妙的沙滩而著称

Florida's neighbor to the north grows some of the sweetest fruit in America.
邻近佛罗里达州北部的佐治亚州种值了一些美国最甜的水果。

So Georgia is the Peach State.
因此佐治亚州被称为"桃子之州"

Hawaii,for out in the Pacific Ocean,is the Aloha State.
远在太平洋的夏威夷别名是"Aloha州"

That is the friendly greeting that means both "hello" and "goodbye" in the native Hawaiian language.
在夏威夷的土地语中,“Aloha” 是一句友好的问候语，有着"你好"和"再见"的双重意思

So,aloha for now.
那么,到这儿就"aloha"吧!

Next week we will tell you about the nicknames of more American states.
下周我们将继续讲述美国更多州的昵称
